package com.example.sociales.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.example.sociales.Activities.Reportes;
import com.example.sociales.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class AdapterOficinas extends RecyclerView.Adapter<AdapterOficinas.ProductViewHolder> {
    private Context mCtx;
    private List<ModelOficinas> items;

    public AdapterOficinas(Context mCtx, List<ModelOficinas> items) {
        this.mCtx = mCtx;
        this.items = items;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_afiliacion_item, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {
        AdapterCoordinadores adapterCoordinador;
        GridLayoutManager gridLayoutManager;
        final ModelOficinas product = items.get(position);
        final String nombre= product.getNombre();
        final String totales= "Total coordinadores: " + product.getTotal();
        holder.tvNombreOficina.setText(nombre);
        holder.tvTotalCoordinadoresPorOficina.setText(totales);


        /**
         * AQUI MANDAMOS EL PARAMETRO PARA EL ADAPTADOR DE COORDINADOIRES
         */
        adapterCoordinador = new AdapterCoordinadores(mCtx, product.getJsonCoordinador());
        holder.recyclerViewCoordinadores.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(mCtx, 1);
        holder.recyclerViewCoordinadores.setLayoutManager(gridLayoutManager);
        holder.recyclerViewCoordinadores.setAdapter(adapterCoordinador);


        holder.btDetalles.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                holder.progressBar.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        if(holder.recyclerViewCoordinadores.getVisibility()==View.GONE)
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.recyclerViewCoordinadores.setVisibility(View.VISIBLE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_arriba);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetalles.setImageDrawable(img);
                            holder.progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.recyclerViewCoordinadores.setVisibility(View.GONE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_abajo);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetalles.setImageDrawable(img);
                            holder.progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);


            }
        });

        holder.rootLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                holder.progressBar.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        if(holder.recyclerViewCoordinadores.getVisibility()==View.GONE)
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.recyclerViewCoordinadores.setVisibility(View.VISIBLE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_arriba);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetalles.setImageDrawable(img);
                            holder.progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.recyclerViewCoordinadores.setVisibility(View.GONE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_abajo);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetalles.setImageDrawable(img);
                            holder.progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);


            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerViewCoordinadores;
        TextView tvNombreOficina, tvTotalCoordinadoresPorOficina;
        ImageView btDetalles;
        LinearLayout rootLayout;
        ProgressBar progressBar;

        //android.widget.SearchView etBuscarNombre;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            recyclerViewCoordinadores= itemView.findViewById(R.id.recyclerViewCoordinadores);
            tvNombreOficina = itemView.findViewById(R.id.tvNombreOficina);
            tvTotalCoordinadoresPorOficina = itemView.findViewById(R.id.tvTotalCoordinadoresPorOficina);
            btDetalles = itemView.findViewById(R.id.btDetalles);
            rootLayout = itemView.findViewById(R.id.rootLayout);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }


}
