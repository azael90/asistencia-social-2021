package com.example.sociales.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.example.sociales.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdapterCoordinadores extends RecyclerView.Adapter<AdapterCoordinadores.ProductViewHolder> {
    private Context mCtx;
    private JSONArray arrayCoordinadores;

    public AdapterCoordinadores(Context mCtx, JSONArray arrayCoordinadores) {
        this.mCtx = mCtx;
        this.arrayCoordinadores = arrayCoordinadores;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_coordinadores_list, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {
        AdapterAfiliaciones adapterAfiliaciones;
        GridLayoutManager gridLayoutManager;

        try {
            List<JSONObject> jsonValues = new ArrayList<JSONObject>();

            for (int i = 0; i <= arrayCoordinadores.length()-1; i++) {
                jsonValues.add(arrayCoordinadores.getJSONObject(i));
            }
            for (int i=0; i<=jsonValues.size()-1;i++)
            {
                holder.tvNombreCoordinador.setText(jsonValues.get(position).getString("nombre"));
                holder.tvTotalesPromotores.setText("Total de promotores: " + jsonValues.get(position).getString("total"));
                /**
                 * AQUI MANDAMOS EL PARAMETRO PARA EL ADAPTADOR DE PROMOTORES
                 */
                adapterAfiliaciones = new AdapterAfiliaciones(mCtx, jsonValues.get(position).getJSONArray("promotores"));
                holder.rvListaDeAfiliaciones.setHasFixedSize(true);
                gridLayoutManager = new GridLayoutManager(mCtx, 1);
                holder.rvListaDeAfiliaciones.setLayoutManager(gridLayoutManager);
                holder.rvListaDeAfiliaciones.setAdapter(adapterAfiliaciones);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }





        holder.btDetallesCoordinadores.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                holder.progressBarCoordinadores.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        if(holder.layoutDetalles.getVisibility()==View.GONE)
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.layoutDetalles.setVisibility(View.VISIBLE);
                            holder.tvOcultar.setVisibility(View.VISIBLE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_arriba);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetallesCoordinadores.setImageDrawable(img);
                            holder.progressBarCoordinadores.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.layoutDetalles.setVisibility(View.GONE);
                            holder.tvOcultar.setVisibility(View.GONE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_abajo);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetallesCoordinadores.setImageDrawable(img);
                            holder.progressBarCoordinadores.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);


            }
        });



        holder.rootlayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                holder.progressBarCoordinadores.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        if(holder.layoutDetalles.getVisibility()==View.GONE)
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.layoutDetalles.setVisibility(View.VISIBLE);
                            holder.tvOcultar.setVisibility(View.VISIBLE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_arriba);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetallesCoordinadores.setImageDrawable(img);
                            holder.progressBarCoordinadores.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                            holder.layoutDetalles.setVisibility(View.GONE);
                            holder.tvOcultar.setVisibility(View.GONE);
                            Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_abajo);
                            img.setBounds(0, 0, 60, 60);
                            holder.btDetallesCoordinadores.setImageDrawable(img);
                            holder.progressBarCoordinadores.setVisibility(View.INVISIBLE);
                        }
                    }
                }, 300);


            }
        });


        holder.tvOcultar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //TransitionManager.beginDelayedTransition(holder.rootLayout, new AutoTransition());
                holder.layoutDetalles.setVisibility(View.GONE);
                holder.tvOcultar.setVisibility(View.GONE);
                Drawable img = mCtx.getResources().getDrawable(R.drawable.flecha_abajo);
                img.setBounds(0, 0, 60, 60);
                holder.btDetallesCoordinadores.setImageDrawable(img);
            }
        });



    }

    @Override
    public int getItemCount() {
        return arrayCoordinadores.length();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView rvListaDeAfiliaciones;
        TextView tvNombreCoordinador, tvTotalesPromotores, tvOcultar;
        ImageView btDetallesCoordinadores;
        LinearLayout layoutDetalles;
        LinearLayout rootlayout;
        ProgressBar progressBarCoordinadores;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            rvListaDeAfiliaciones= itemView.findViewById(R.id.rvListaDeAfiliaciones);
            tvNombreCoordinador = itemView.findViewById(R.id.tvNombreCoordinador);
            tvTotalesPromotores = itemView.findViewById(R.id.tvTotalPromotores);
            layoutDetalles = itemView.findViewById(R.id.layoutDetalles);
            rootlayout = itemView.findViewById(R.id.rootlayout);
            tvOcultar = itemView.findViewById(R.id.tvOcultar);
            btDetallesCoordinadores = itemView.findViewById(R.id.btDetallesCoordinadores);
            progressBarCoordinadores = itemView.findViewById(R.id.progressBarCoordinadores);
        }
    }


}
