package com.example.sociales.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chinodev.androidneomorphframelayout.NeomorphFrameLayout;
import com.example.sociales.DatabaseLocal.Archivo;
import com.example.sociales.DatabaseLocal.ArchivosDescargados;
import com.example.sociales.Extras.MyCallBack;
import com.example.sociales.Extras.MyCallBackFromDownloads;
import com.example.sociales.R;

import java.util.List;

public class AdapertPDFDescargados extends RecyclerView.Adapter<AdapertPDFDescargados.ProductViewHolder> {
    private Context mCtx;
    private MyCallBackFromDownloads myCallBackFromDownloads;
    private List<ArchivosDescargados> productList;

    public AdapertPDFDescargados(Context mCtx, List<ArchivosDescargados> productList, MyCallBackFromDownloads myCallBackFromDownloads) {
        this.mCtx = mCtx;
        this.productList = productList;
        this.myCallBackFromDownloads = myCallBackFromDownloads;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_pdf_list_download, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {
        final ArchivosDescargados product = productList.get(position);
        final String nombre= product.getNombreArchivo();
        holder.tvNombrePDF.setText(nombre);
        holder.tvFechaDescarga.setText(product.getFecha());


        holder.btVerDocumento.setTag(position);
        holder.btVerDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCallBackFromDownloads.onClickOpenDocument(position, product.getNombreArchivo(), product.getFecha());
            }
        });

        holder.btVerDocumentoNeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCallBackFromDownloads.onClickOpenDocument(position, product.getNombreArchivo(), product.getFecha());
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        NeomorphFrameLayout btVerDocumentoNeo;
        TextView tvNombrePDF, tvFechaDescarga;
        ImageView btVerDocumento;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvNombrePDF= itemView.findViewById(R.id.tvNombrePDF);
            btVerDocumento = itemView.findViewById(R.id.btVerDocumento);
            tvFechaDescarga = itemView.findViewById(R.id.tvFechaDescarga);
            btVerDocumentoNeo = itemView.findViewById(R.id.btVerDocumentoNeo);

        }
    }


}
