package com.example.sociales.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.chinodev.androidneomorphframelayout.NeomorphFrameLayout;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.Solicitud_Cliente;
import com.example.sociales.Extras.CallbackSolicitud;
import com.example.sociales.R;
import com.google.android.gms.vision.text.Line;

import java.util.List;


public class AdapterContract extends RecyclerView.Adapter<AdapterContract.ProductViewHolder>
{
    private Context mCtx;
    private List<ModelContractDetails> productList;
    private List<ModelPaymentPerContract> productListPaymentsPerContract;
    AdapterPaymentIntoContract adapterPaymentIntoContract;
    GridLayoutManager gridLayoutManager;

    public AdapterContract(Context mCtx, List<ModelContractDetails> productList, List<ModelPaymentPerContract> productListPaymentsPerContract)
    {
        this.mCtx = mCtx;
        this.productList = productList;
        this.productListPaymentsPerContract = productListPaymentsPerContract;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =inflater.inflate(R.layout.contract_view, null );
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {
        final ModelContractDetails product= productList.get(position);
        holder.tvContrato.setText("No. Contrato: " + product.getContrato());
        holder.tvSolicitud.setText(product.getSolicitud());
        holder.tvNombre.setText(product.getNombre());
        holder.tvNombrePlan.setText(product.getNombrePlan());
        holder.tvCostoPaquete.setText(product.getCosto());
        holder.tvDomicilio.setText(product.getDomicilio());
        holder.tvSaldoContrato.setText(product.getSaldo());
        holder.tvFormaPago.setText(product.getFormaPago());
        holder.tvMontoPago.setText(product.getMontoPago());
        holder.tvMontoAbonado.setText(product.getMontoAbonado());
        holder.tvDetalleServicio.setText(product.getDetalleServicio());
        holder.tvEstatus.setText(product.getStatus());
        holder.tvFechaContrato.setText(product.getFechaContrato());

        try {
            if (productListPaymentsPerContract.size() == 0) {
                holder.layoutDetalles.setVisibility(View.GONE);
                holder.tvSinCobros.setVisibility(View.VISIBLE);
            } else {
                holder.layoutDetalles.setVisibility(View.VISIBLE);
                holder.tvSinCobros.setVisibility(View.GONE);
                switch (productListPaymentsPerContract.size()) {
                    case 1:
                        holder.view2.getLayoutParams().height = 300;
                        break;
                    case 2:
                        holder.view2.getLayoutParams().height = 450;
                        break;
                    case 3:
                        holder.view2.getLayoutParams().height = 600;
                        break;
                    case 4:
                        holder.view2.getLayoutParams().height = 700;
                        break;
                    case 5:
                        holder.view2.getLayoutParams().height = 850;
                        break;
                }
            }

            adapterPaymentIntoContract = new AdapterPaymentIntoContract(mCtx, productListPaymentsPerContract);
            holder.rvPayments.setHasFixedSize(true);
            gridLayoutManager = new GridLayoutManager(mCtx, 1);
            holder.rvPayments.setLayoutManager(gridLayoutManager);
            holder.rvPayments.setAdapter(adapterPaymentIntoContract);

        }catch (Throwable e)
        {
            Log.e("ADAPTER_CONTRACT", "error en adapter contract: " + e.getMessage());
        }

    }

    @Override
    public int getItemCount()
    {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvContrato,tvNombre, tvSolicitud, tvNombrePlan, tvCostoPaquete, tvDomicilio, tvSaldoContrato, tvFormaPago, tvMontoPago, tvMontoAbonado, tvDetalleServicio, tvEstatus, tvSinCobros, tvFechaContrato;
        RecyclerView rvPayments;
        ConstraintLayout layoutDetalles;
        View view2;
        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvContrato=itemView.findViewById(R.id.tvContrato);
            tvNombre=itemView.findViewById(R.id.tvNombre);
            tvSolicitud=itemView.findViewById(R.id.tvSolicitud);
            tvNombrePlan=itemView.findViewById(R.id.tvNombrePlan);
            layoutDetalles=itemView.findViewById(R.id.layoutDetalles);
            tvCostoPaquete=itemView.findViewById(R.id.tvCostoPaquete);
            tvDomicilio=itemView.findViewById(R.id.tvDomicilio);
            tvSaldoContrato=itemView.findViewById(R.id.tvSaldoContrato);
            tvFormaPago=itemView.findViewById(R.id.tvFormaPago);
            tvMontoPago=itemView.findViewById(R.id.tvMontoPago);
            tvMontoAbonado=itemView.findViewById(R.id.tvMontoAbonado);
            tvDetalleServicio=itemView.findViewById(R.id.tvDetalleServicio);
            tvEstatus=itemView.findViewById(R.id.tvStatus);
            rvPayments=itemView.findViewById(R.id.rvPayments);
            view2=itemView.findViewById(R.id.view2);
            tvSinCobros=itemView.findViewById(R.id.tvSinCobros);
            tvFechaContrato=itemView.findViewById(R.id.tvFechaContrato);
        }
    }

}
