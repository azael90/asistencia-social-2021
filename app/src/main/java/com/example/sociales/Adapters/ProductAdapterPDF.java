package com.example.sociales.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chinodev.androidneomorphframelayout.NeomorphFrameLayout;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.Extras.MyCallBack;
import com.example.sociales.R;

import java.util.List;

public class ProductAdapterPDF extends RecyclerView.Adapter<ProductAdapterPDF.ProductViewHolder> {
    private Context mCtx;
    private MyCallBack callback;
    private List<ModelPDF> productList;
    private static String APP_DIRECTORY = "NOMINAS_PDF/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "Recibos_Nomina";
    private static String nombrePDF="";

    public ProductAdapterPDF(Context mCtx, List<ModelPDF> productList, MyCallBack callback) {
        this.mCtx = mCtx;
        this.productList = productList;
        this.callback = callback;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_pdf_list, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {
        final ModelPDF product = productList.get(position);
        final String nombre= product.getNombre() + ".pdf";
        holder.tvNombrePDF.setText(nombre);

        if(Control.verificarSiTenemosUnArchivoConElMismoNombre(nombre))
            holder.btDescargar.setImageResource(R.drawable.check);
        else
            holder.btDescargar.setImageResource(R.drawable.download_icon);



        holder.btDescargar.setTag(position);
        holder.btDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickDownload(position, product.getNombre(), product.url);
            }
        });

        holder.btDescargarNeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickDownload(position, product.getNombre(), product.url);
            }
        });


        /*holder.btDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                nombrePDF = product.getNombre();
                descargarPDF(product.getUrl(), nombre);
                Log.v("DESCARGANDO PDF", ": " + product.getNombre());
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        NeomorphFrameLayout btDescargarNeo;
        TextView tvNombrePDF;
        ImageView btDescargar;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvNombrePDF= itemView.findViewById(R.id.tvNombrePDF);
            btDescargar = itemView.findViewById(R.id.btDescargar);
            btDescargarNeo = itemView.findViewById(R.id.btDescargarNeo);

        }
    }


    /*public void descargarPDF(String urlPDFPorPosicion, String nombrePDF)
    {
        //String urlParaDescargar = "http://webpersonal.uma.es/~ECASILARI/Research/Documentos_y_programas/05manual_es.pdf";
        ProgressDialog progressDialog = new ProgressDialog(mCtx);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("Descargando");
        progressDialog.setMessage("Su documento " + nombrePDF + " estará listo en un momento...");
        new descargarPDFAsyncTask(progressDialog).execute(urlPDFPorPosicion);

    }*/


    /*static class descargarPDFAsyncTask extends AsyncTask<String, Integer, String>
    {
        ProgressDialog progressDialog;

        descargarPDFAsyncTask(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... urlPDF) {
            String urlParaDescargar = urlPDF[0];

            HttpURLConnection conexion = null;
            InputStream input = null;
            OutputStream output = null;

            File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
            boolean isDirectoryCreated = file.exists();

            if(!isDirectoryCreated)
                isDirectoryCreated = file.mkdirs();

            if(isDirectoryCreated){
                try {

                    URL url = new URL(urlParaDescargar);
                    conexion = (HttpURLConnection) url.openConnection();
                    conexion.connect();

                    if(conexion.getResponseCode() != HttpURLConnection.HTTP_OK)
                    {
                        return "Conexión no se realizó correctamente";
                    }

                    input = conexion.getInputStream();
                    String rutaDelFicheroGuardado = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator +"/"+nombrePDF+".pdf";

                    File newFile = new File(rutaDelFicheroGuardado);
                    try
                    {
                        newFile.createNewFile();
                    }
                    catch (IOException e)
                    {
                        Log.e("Error: ",  e.getMessage());
                    }

                    output = new FileOutputStream(rutaDelFicheroGuardado);

                    int tamanoFichero = conexion.getContentLength();


                    byte[] data = new byte[1024];
                    int total =0;
                    int count;

                    while((count = input.read(data)) != -1)
                    {
                        sleep(50);
                        output.write(data, 0, count);
                        total += count;
                        publishProgress((int) (total * 100 /tamanoFichero));
                    }

                }catch (MalformedURLException e)
                {
                    e.printStackTrace();
                    return "Error: " + e.getMessage();
                }catch (IOException e)
                {
                    e.printStackTrace();
                    return "Error: " + e.getMessage();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if(input != null) input.close();
                        if(output != null) output.close();
                        if(conexion != null) conexion.disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            //Este return corresponde al parametro mensaje de onPostExecute
            return "Se realizo la descarga correctamente";
        }

        @Override
        protected void onProgressUpdate(Integer... progreso) {
            super.onProgressUpdate(progreso);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(progreso[0]);
        }

        @Override
        protected void onPostExecute(String mensaje) {
            super.onPostExecute(mensaje);
            progressDialog.dismiss();
            Toast.makeText(mCtx, mensaje, Toast.LENGTH_SHORT).show();
        }
    }*/



}
