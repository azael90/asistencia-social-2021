package com.example.sociales.Adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class ProductAdapterImages extends RecyclerView.Adapter<ProductAdapterImages.ProductViewHolder> {
    private Context mCtx;
    private List<ProductImages> productList;
    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";
    //Dialog myDialog;
    BottomSheetDialog mBottomSheetDialog;
    public ProductAdapterImages(Context mCtx, List<ProductImages> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
        //this.bandera=bandera;

    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.patron_promociones_primer_plano, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        /*holder.imgPublicidad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // run scale animation and make it bigger
                    Animation anim = AnimationUtils.loadAnimation(mCtx, R.anim.scale_in_tv);
                    holder.imgPublicidad.startAnimation(anim);
                    anim.setFillAfter(true);
                } else {
                    // run scale animation and make it smaller
                    Animation anim = AnimationUtils.loadAnimation(mCtx, R.anim.scale_out_tv);
                    holder.imgPublicidad.startAnimation(anim);
                    anim.setFillAfter(true);
                }
            }
        });*/
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position)
    {
        final ProductImages product = productList.get(position);

        Picasso.with(mCtx).load(product.getUrl()).into(holder.imgPublicidad);
        Picasso.with(mCtx).setIndicatorsEnabled(true);

        //Glide.with(mCtx).load(product.getUrl()).into(holder.imgPublicidad);

        //Picasso.with(mCtx).load(product.getUrl()).into(holder.imgPublicidad);
        //Picasso.with(mCtx).setIndicatorsEnabled(true);

        holder.imgPublicidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPreview(product.getUrl());
                holder.imgPublicidad.requestFocus();
                holder.imgPublicidad.hasFocus();
            }
        });


    }

    public String getStringImagen(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }




    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        PhotoView imgPublicidad;
        public ProductViewHolder(View itemView)
        {
            super(itemView);
            imgPublicidad= itemView.findViewById(R.id.imgPublicidad);
        }
    }


    public void mostrarPreview(String url)
    {
        ImageView btSalir;
        PhotoView imageView;
        //myDialog= new Dialog(mCtx);
        //myDialog.setContentView(R.layout.dialog_custom_layout);
        mBottomSheetDialog = new BottomSheetDialog(mCtx);
        mBottomSheetDialog.setContentView(R.layout.dialog_custom_layout);



        imageView=(PhotoView) mBottomSheetDialog.findViewById(R.id.imageView);
        //Picasso.with(mCtx).load(url).into(imageView);
        //Picasso.with(mCtx).setIndicatorsEnabled(true);
        Glide.with(mCtx).load(url).into(imageView);

        mBottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mBottomSheetDialog.show();
    }

}
