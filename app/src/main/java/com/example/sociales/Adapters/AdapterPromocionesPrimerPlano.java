package com.example.sociales.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sociales.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterPromocionesPrimerPlano extends RecyclerView.Adapter<AdapterPromocionesPrimerPlano.ProductViewHolder>
{
    private Context mCtx;
    private List<ProductImages> productList;

    public AdapterPromocionesPrimerPlano(Context mCtx, List<ProductImages> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.patron_promociones_primer_plano, null);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPromocionesPrimerPlano.ProductViewHolder holder, int position) {
        final ProductImages product = productList.get(position);
        Picasso.with(mCtx).load(product.getUrl()).into(holder.imgPublicidad);
        Picasso.with(mCtx).setIndicatorsEnabled(true);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imgPublicidad;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            imgPublicidad= itemView.findViewById(R.id.imgPublicidad);
        }
    }
}
