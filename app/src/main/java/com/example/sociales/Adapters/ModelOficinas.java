package com.example.sociales.Adapters;

import org.json.JSONArray;

public class ModelOficinas
{
    String nombre="", total="";
    JSONArray jsonCoordinador;

    public ModelOficinas(String nombre, String total, JSONArray jsonCoordinador) {
        this.nombre = nombre;
        this.total = total;
        this.jsonCoordinador = jsonCoordinador;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    public JSONArray getJsonCoordinador() {
        return jsonCoordinador;
    }

    public void setJsonCoordinador(JSONArray jsonCoordinador) {
        this.jsonCoordinador = jsonCoordinador;
    }
}
