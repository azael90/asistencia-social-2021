package com.example.sociales.Adapters;

public class ModelPromocionesPrimerPlano
{
    String nombre, idStatusPromocion, imagen, fecha;

    public ModelPromocionesPrimerPlano(String nombre, String idStatusPromocion, String imagen, String fecha) {
        this.nombre = nombre;
        this.idStatusPromocion = idStatusPromocion;
        this.imagen = imagen;
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdStatusPromocion() {
        return idStatusPromocion;
    }

    public void setIdStatusPromocion(String idStatusPromocion) {
        this.idStatusPromocion = idStatusPromocion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
