package com.example.sociales.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sociales.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class AdapterAfiliaciones extends RecyclerView.Adapter<AdapterAfiliaciones.ProductViewHolder>
{
    private Context mCtx;
    private JSONArray jsonArrayPromotores;

    public AdapterAfiliaciones(Context mCtx, JSONArray jsonArrayPromotores)
    {
        this.mCtx = mCtx;
        this.jsonArrayPromotores = jsonArrayPromotores;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =inflater.inflate(R.layout.item_list_afliacion, null );
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {

        try {
            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i <= jsonArrayPromotores.length()-1; i++) {
                jsonValues.add(jsonArrayPromotores.getJSONObject(i));
            }

            for (int i=0; i<=jsonValues.size()-1;i++)
            {
                holder.tvCodigo.setText(jsonValues.get(position).getString("promotor_codigo"));
                holder.tvNombre.setText(jsonValues.get(position).getString("promotor_nombre"));
                holder.tvNumeroDeAfiliaciones.setText(jsonValues.get(position).getString("promotor_cantidad_afiliaciones"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }





    }

    @Override
    public int getItemCount()
    {
        return jsonArrayPromotores.length();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvCodigo, tvNombre, tvNumeroDeAfiliaciones;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvCodigo=itemView.findViewById(R.id.tvCodigo);
            tvNombre=itemView.findViewById(R.id.tvNombre);
            tvNumeroDeAfiliaciones=itemView.findViewById(R.id.tvNumeroDeAfiliaciones);
        }
    }


    /*public void filter(final String strSearch) {
        if(strSearch.length()==0)
        {
            items.clear();
            items.addAll(originalItems);
        }
        else
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                //items.clear();
                List<ModelAfiliacionesList> collect = originalItems.stream()
                        .filter(i-> i.getNombre().toLowerCase().contains(strSearch))
                        .collect(Collectors.toList());
                //items.clear();
                items.addAll(collect);
            }
            else
            {
                //items.clear();
                for(ModelAfiliacionesList i : originalItems)
                {
                    if(i.getNombre().toLowerCase().contains(strSearch))
                    {
                        items.add(i);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }*/
}
