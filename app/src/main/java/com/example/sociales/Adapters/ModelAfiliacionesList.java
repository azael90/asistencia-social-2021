package com.example.sociales.Adapters;

public class ModelAfiliacionesList {
    String nombre = "";

    public ModelAfiliacionesList(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
