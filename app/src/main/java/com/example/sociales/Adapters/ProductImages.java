package com.example.sociales.Adapters;

public class ProductImages
{
    String url="";
    String bitmap="";

    public ProductImages(String url, String bitmap)
    {
        this.url = url;
        this.bitmap = bitmap;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBitmap() {
        return bitmap;
    }
}

