package com.example.sociales.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sociales.R;

import java.util.List;


public class AdapterPaymentIntoContract extends RecyclerView.Adapter<AdapterPaymentIntoContract.ProductViewHolder>
{
    private Context mCtx;
    private List<ModelPaymentPerContract> productListPaymentsPerContract;

    public AdapterPaymentIntoContract(Context mCtx, List<ModelPaymentPerContract> productListPaymentsPerContract)
    {
        this.mCtx = mCtx;
        this.productListPaymentsPerContract = productListPaymentsPerContract;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =inflater.inflate(R.layout.adapter_payments_per_contract, null );
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {
        final ModelPaymentPerContract productPayments= productListPaymentsPerContract.get(position);
        holder.tvFolio.setText("Folio: " + productPayments.getFolio());
        holder.tvMonto.setText("Monto: " + productPayments.getMonto());
        String cadena = "Fecha de movimiento: "+ productPayments.getFecha() +" a las " + productPayments.getHora();
        holder.tvContenidoConFecha.setText(cadena);
        holder.tvRecuperador.setText("Recuperador: "+ productPayments.getCobrador());

    }

    @Override
    public int getItemCount()
    {
        return productListPaymentsPerContract.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvFolio, tvMonto, tvContenidoConFecha, tvRecuperador;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvFolio=itemView.findViewById(R.id.tvFolio);
            tvMonto=itemView.findViewById(R.id.tvMonto);
            tvContenidoConFecha=itemView.findViewById(R.id.tvContenidoConFecha);
            tvRecuperador=itemView.findViewById(R.id.tvRecuperador);
        }
    }

}
