package com.example.sociales.Adapters;

public class ModelContractDetails {

    String contrato="", nombre="", solicitud="", nombrePlan="", costo="", domicilio="", saldo="", formaPago="", montoPago="", montoAbonado="", detalleServicio="", status="", fechaContrato="";

    public ModelContractDetails(String contrato, String nombre, String solicitud, String nombrePlan, String costo,
                                String domicilio, String saldo, String formaPago, String montoPago,
                                String montoAbonado, String detalleServicio, String status, String fechaContrato) {
        this.contrato = contrato;
        this.nombre = nombre;
        this.solicitud = solicitud;
        this.nombrePlan = nombrePlan;
        this.costo = costo;
        this.domicilio = domicilio;
        this.saldo = saldo;
        this.formaPago = formaPago;
        this.montoPago = montoPago;
        this.montoAbonado = montoAbonado;
        this.detalleServicio = detalleServicio;
        this.status = status;
        this.fechaContrato = fechaContrato;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(String solicitud) {
        this.solicitud = solicitud;
    }

    public String getNombrePlan() {
        return nombrePlan;
    }

    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getMontoPago() {
        return montoPago;
    }

    public void setMontoPago(String montoPago) {
        this.montoPago = montoPago;
    }

    public String getMontoAbonado() {
        return montoAbonado;
    }

    public void setMontoAbonado(String montoAbonado) {
        this.montoAbonado = montoAbonado;
    }

    public String getDetalleServicio() {
        return detalleServicio;
    }

    public void setDetalleServicio(String detalleServicio) {
        this.detalleServicio = detalleServicio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(String fechaContrato) {
        this.fechaContrato = fechaContrato;
    }
}
