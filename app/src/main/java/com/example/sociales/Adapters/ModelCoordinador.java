package com.example.sociales.Adapters;

import org.json.JSONArray;

public class ModelCoordinador {
    String nombre = "", codigo="", total="";
    JSONArray arrayPromtores;

    public ModelCoordinador(String nombre, String codigo, String total, JSONArray arrayPromtores) {
        this.nombre = nombre;
        this.arrayPromtores = arrayPromtores;
        this.codigo = codigo;
        this.total = total;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public JSONArray getArrayPromtores() {
        return arrayPromtores;
    }

    public void setArrayPromtores(JSONArray arrayPromtores) {
        this.arrayPromtores = arrayPromtores;
    }
}
