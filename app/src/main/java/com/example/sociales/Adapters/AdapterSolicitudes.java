package com.example.sociales.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.chinodev.androidneomorphframelayout.NeomorphFrameLayout;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.Solicitud_Cliente;
import com.example.sociales.DatabaseLocal.SolicitudesNuevas;
import com.example.sociales.DatabaseLocal.Ultimas;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.CallbackSolicitud;
import com.example.sociales.Extras.MyCallBackFromDownloads;
import com.example.sociales.R;

import java.util.List;


public class AdapterSolicitudes extends RecyclerView.Adapter<AdapterSolicitudes.ProductViewHolder>
{
    private Context mCtx;
    private List<SolicitudesNuevas> productList;
    private CallbackSolicitud callbackSolicitud;

    public AdapterSolicitudes(Context mCtx, List<SolicitudesNuevas> productList, CallbackSolicitud callbackSolicitud)
    {
        this.mCtx = mCtx;
        this.productList = productList;
        this.callbackSolicitud = callbackSolicitud;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =inflater.inflate(R.layout.afiliaciones_lista, null );
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {
        final SolicitudesNuevas product= productList.get(position);
        String cadena = "Código:    "+product.getCodigoactivacion();
        holder.tvcodigo.setText(cadena);
        holder.tvSolicitud.setText(product.getCodigobarras());
        holder.tvFecha.setText("      "+ product.getFecha());
    }

    @Override
    public int getItemCount()
    {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvcodigo, tvSolicitud, tvFecha, tvStatus;
        //ConstraintLayout layoutDetalles;
        //ImageView btDetalles, imgFachada, imgComprobanteDom, imgIdentificacionC, imgContratillo, imgDepositoo;
        //NeomorphFrameLayout btGeneralSolciitud, btFachada, btComprobanteDomicilio, btIdentificacion, btContrato, btDeposito;
        //View circleView1, lineaView1, cirleView2, lineaView2;
        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvcodigo=itemView.findViewById(R.id.tvcodigo);
            tvSolicitud=itemView.findViewById(R.id.tvSolicitud);
            tvFecha=itemView.findViewById(R.id.tvFecha);
            //layoutDetalles=itemView.findViewById(R.id.layoutDetalles);
            //btDetalles=itemView.findViewById(R.id.btDetalles);
            //btGeneralSolciitud=itemView.findViewById(R.id.btGeneralSolciitud);
            tvStatus=itemView.findViewById(R.id.tvStatus);
            //imgFachada=itemView.findViewById(R.id.imgFachada);
            /*imgComprobanteDom=itemView.findViewById(R.id.imgComprobanteDom);
            imgIdentificacionC=itemView.findViewById(R.id.imgIdentificacionC);
            imgContratillo=itemView.findViewById(R.id.imgContratillo);
            imgDepositoo=itemView.findViewById(R.id.imgDepositoo);

            btFachada=itemView.findViewById(R.id.btFachada);
            btComprobanteDomicilio=itemView.findViewById(R.id.btComprobanteDomicilio);
            btIdentificacion=itemView.findViewById(R.id.btIdentificacion);
            btContrato=itemView.findViewById(R.id.btContrato);
            btDeposito=itemView.findViewById(R.id.btDeposito);


            circleView1=itemView.findViewById(R.id.view);
            lineaView1=itemView.findViewById(R.id.view2);
            cirleView2=itemView.findViewById(R.id.viwwew);
            lineaView2=itemView.findViewById(R.id.view2www);*/


        }
    }

}
