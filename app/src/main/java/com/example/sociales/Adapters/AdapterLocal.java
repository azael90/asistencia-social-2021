package com.example.sociales.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.example.sociales.Activities.PrincipalScreen;
import com.example.sociales.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

public class AdapterLocal  extends RecyclerView.Adapter<AdapterLocal.ProductViewHolder>
{
    private Context mCtx;
    private List<ProductImages> productList;
    //Dialog myDialog;
    BottomSheetDialog mBottomSheetDialog;

    public AdapterLocal(Context mCtx, List<ProductImages> productList)
    {
        this.mCtx = mCtx;
        this.productList = productList;
    }
    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =inflater.inflate(R.layout.load_images, null );
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {
        final ProductImages product= productList.get(position);
        byte[] decodedString = Base64.decode(product.getBitmap(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.imgPublicidad.setImageBitmap(decodedByte);
        //holder.imgPublicidad.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        holder.imgPublicidad.getLayoutParams().width = 450;
        holder.imgPublicidad.getLayoutParams().height = 450;
        holder.imgPublicidad.setScaleType(ImageView.ScaleType.CENTER_CROP);

        holder.imgPublicidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPreview(product.getBitmap());
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imgPublicidad;
        public ProductViewHolder(View itemView)
        {
            super(itemView);
            imgPublicidad=itemView.findViewById(R.id.imgPublicidad);


        }
    }



    public void mostrarPreview(String bitmap)
    {
        ImageView btSalir;
        PhotoView imageView;
        //myDialog= new Dialog(mCtx);

        mBottomSheetDialog = new BottomSheetDialog(mCtx);
        mBottomSheetDialog.setContentView(R.layout.dialog_custom_layout);


        //myDialog.setContentView(R.layout.dialog_custom_layout);
        imageView=(PhotoView) mBottomSheetDialog.findViewById(R.id.imageView);

        byte[] decodedString = Base64.decode(bitmap, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageView.setImageBitmap(decodedByte);
        //holder.imgPublicidad.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        imageView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        imageView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        /*btSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });*/

        mBottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mBottomSheetDialog.show();
    }
}
