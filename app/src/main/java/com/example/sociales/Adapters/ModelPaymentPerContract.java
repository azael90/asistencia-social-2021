package com.example.sociales.Adapters;

public class ModelPaymentPerContract {
    String folio= "", fecha="", hora="", monto="", cobrador="";

    public ModelPaymentPerContract(String folio, String fecha, String hora, String monto, String cobrador) {
        this.folio = folio;
        this.fecha = fecha;
        this.hora = hora;
        this.monto = monto;
        this.cobrador = cobrador;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getCobrador() {
        return cobrador;
    }

    public void setCobrador(String cobrador) {
        this.cobrador = cobrador;
    }
}
