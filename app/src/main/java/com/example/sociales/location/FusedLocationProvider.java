package com.example.sociales.location;
        
    import android.Manifest;
    import android.content.Context;
    import android.location.Location;
    import android.os.Looper;
    import android.util.Log;
    import androidx.annotation.NonNull;
    import com.example.sociales.DatabaseLocal.Control;
    import com.example.sociales.Extras.ApplicationResourcesProvider;
    import com.google.android.gms.location.FusedLocationProviderClient;
    import com.google.android.gms.location.LocationCallback;
    import com.google.android.gms.location.LocationRequest;
    import com.google.android.gms.location.LocationResult;
    import com.google.android.gms.location.LocationServices;
    import com.google.android.gms.location.LocationSettingsRequest;
    import com.google.android.gms.location.LocationSettingsResponse;
    import com.google.android.gms.location.SettingsClient;
    import com.google.android.gms.tasks.OnCompleteListener;
    import com.google.android.gms.tasks.OnFailureListener;
    import com.google.android.gms.tasks.OnSuccessListener;
    import com.google.android.gms.tasks.Task;
    import android.Manifest;
    import pl.tajchert.nammu.Nammu;
    import java.text.SimpleDateFormat;
    import java.util.Date;

    /**
    * Created by Jordan Alvarez on 09/08/2019.
    */
    public class FusedLocationProvider {

        private static final int UPDATE_INTERVAL = 60 * 1000 * 10;//60 * 1000 * 10 ; //-> 10 MIN - UPDATE WHEN IN PRODUCTION
        private static final int FASTEST_UPDATE_INTERVAL = 60 * 1000 * 9; // 60 * 1000 * 3; //60 * 1000 * 9 -> 10 MIN - UPDATE WHEN IN PRODUCTION
        private static final String TAG = "ASISTENCIA_SOCIAL";
        private FusedLocationProviderClient mFusedLocationProvider;
        private SettingsClient mSettingsClient;
        private LocationRequest mLocationRequest;
        private LocationSettingsRequest mLocationSettingsRequest;
        private LocationCallback mLocationCallback;
        private Location mCurrentLocation = null;
        private boolean mRequestLocationUpdates;
        private String date;
        private Location lastKnownLocation = null;

        public FusedLocationProvider(Context context) {
            mRequestLocationUpdates = false;
            mFusedLocationProvider = LocationServices.getFusedLocationProviderClient(context);
            mSettingsClient = LocationServices.getSettingsClient(context);

            createLocationRequest();
            createLocationCallback();
            buildLocationSettingsRequest();
        }

        private void createLocationRequest() {
            mLocationRequest = new LocationRequest();

            mLocationRequest.setInterval(UPDATE_INTERVAL);

            mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);

            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }

        private void createLocationCallback() {
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    Log.i(TAG, "Ubicación obtenida: " + locationResult.getLastLocation());

                    mCurrentLocation = locationResult.getLastLocation();

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    date = dateFormat.format(new Date());

                    Control.insertarUbicaciones(
                            null,
                            Control.getPromotorCodigo(),
                            String.valueOf(mCurrentLocation.getLatitude()),
                            String.valueOf(mCurrentLocation.getLongitude()),
                            date,
                            0);

                    Log.i(TAG, "Fecha de ubicación: " + date);
                }
            };
        }

        private void buildLocationSettingsRequest() {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
        }

        public void startLocationUpdates() {
            if (mRequestLocationUpdates)
                return;

            mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                    .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            Log.i(TAG, "Obteniendo ubicaciones");
                            Nammu.init(ApplicationResourcesProvider.getContext());
                            if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                                mFusedLocationProvider.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                                mRequestLocationUpdates = true;
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mRequestLocationUpdates = false;
                        }
                    });
        }

        public void stopLocationUpdates() {
            if (!mRequestLocationUpdates) {
                return;
            }
            mFusedLocationProvider.removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.i(TAG, "Detenido la obtencion de ubicaciones");
                            mRequestLocationUpdates = false;
                        }
                    });
        }

        public Location getGPSPosition() {
            return mCurrentLocation;
        }

        public Location getLastKnownLocation() {
            return mCurrentLocation;
        }

        public void setLastKnownLocation(double latitude, double longitude) {
            mCurrentLocation.setLatitude(latitude);
            mCurrentLocation.setLongitude(longitude);

        }


    }