package com.example.sociales.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.sociales.Adapters.AdapterCoordinadores;
import com.example.sociales.Adapters.AdapterCoordinadoresDos;
import com.example.sociales.Adapters.AdapterOficinas;
import com.example.sociales.Adapters.ModelCoordinador;
import com.example.sociales.Adapters.ModelOficinas;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.Oficinas;
import com.example.sociales.DatabaseLocal.Promotor;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.CallbackLoading;
import com.example.sociales.Extras.URL;
import com.example.sociales.Extras.VolleySingleton;
import com.example.sociales.R;
import com.google.android.gms.vision.text.Line;
import com.google.android.material.textfield.TextInputEditText;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class Reportes extends AppCompatActivity implements CallbackLoading
{
    public static String TAG="REPORTES";
    LinearLayout layoutCoordinador, layoutGerente, layoutAdmin;
    String codigoPromotor = "";
    TextInputEditText etFecha1, etFecha2, etCodigoEmpleado;
    ProgressBar progressBarFind;
    Button btBuscar;
    LinearLayout contenedorDeBusqueda, root_layout, contenedorDeBusquedaAdmin;
    AppCompatSpinner spOficinas;
    ImageView btBack;
    DatePickerDialog.OnDateSetListener mDateSetListener, mDateSetListener2,  mDateSetListenerAdmin, mDateSetListener2Admin;
    String nombreOficina = "";
    Dialog dialogError;
    boolean isAdmin = false;
    //*************************** COORDINADORES ****************************
    ProgressBar progressBarFindCoordinador;
    Button btBuscarCoordinador;
    TextInputEditText etFecha1Coordinador, etFecha2Coordinador;
    RecyclerView rvCoordinadores;
    LinearLayout frameNotFoundCoordinador;

    //**********************************************************************

    //*************************** COORDINADORES ****************************
    ProgressBar progressBarFindAdmin;
    TextInputEditText etCodigoAdmin;
    Button btBuscarAdmin;
    TextInputEditText etFecha1Admin, etFecha2Admin;
    RecyclerView rvAfiliacionesAdmin;
    LinearLayout frameNotFoundAdmin;
    TextView tvTotalGeneralAdmin;
    //**********************************************************************
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reportes);
        layoutCoordinador = (LinearLayout) findViewById(R.id.layoutCoordinador);
        layoutGerente = (LinearLayout)findViewById(R.id.layoutGerente);
        layoutAdmin = (LinearLayout)findViewById(R.id.layoutAdmin);

        btBack = (ImageView)findViewById(R.id.btBack);
        etFecha1 = (TextInputEditText) findViewById(R.id.etFecha1);
        etFecha2 = (TextInputEditText) findViewById(R.id.etFecha2);
        etCodigoEmpleado = (TextInputEditText) findViewById(R.id.etCodigoEmpleado);
        btBuscar = (Button) findViewById(R.id.btBuscar);
        contenedorDeBusqueda = (LinearLayout) findViewById(R.id.contenedorDeBusqueda);
        root_layout = (LinearLayout) findViewById(R.id.root_layout);
        spOficinas =(AppCompatSpinner) findViewById(R.id.spOficinas);
        dialogError = new Dialog(this);

        //*********Coordinador
        etFecha1Coordinador = (TextInputEditText) findViewById(R.id.etFecha1Coordinador);
        etFecha2Coordinador = (TextInputEditText) findViewById(R.id.etFecha2Coordinador);
        progressBarFindCoordinador = (ProgressBar) findViewById(R.id.progressBarFindCoordinador);
        btBuscarCoordinador = (Button) findViewById(R.id.btBuscarCoordinador);
        rvCoordinadores = (RecyclerView) findViewById(R.id.rvCoordinadores);
        frameNotFoundCoordinador = (LinearLayout) findViewById(R.id.frameNotFoundCoordinador);
        //*********
        //*********ADMIN
        etFecha1Admin = (TextInputEditText) findViewById(R.id.etFecha1Admin);
        etFecha2Admin = (TextInputEditText) findViewById(R.id.etFecha2Admin);
        progressBarFindAdmin = (ProgressBar) findViewById(R.id.progressBarFindAdmin);
        btBuscarAdmin = (Button) findViewById(R.id.btBuscarAdmin);
        rvAfiliacionesAdmin = (RecyclerView) findViewById(R.id.rvAfiliacionesAdmin);
        frameNotFoundAdmin = (LinearLayout) findViewById(R.id.frameNotFoundAdmin);
        etCodigoAdmin = (TextInputEditText) findViewById(R.id.etCodigoAdmin);
        tvTotalGeneralAdmin = (TextView) findViewById(R.id.tvTotalGeneralAdmin);
        contenedorDeBusquedaAdmin = (LinearLayout) findViewById(R.id.contenedorDeBusquedaAdmin);
        //*********

        final Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            if(extras.containsKey("codigoPromotor"))
                codigoPromotor = extras.getString("codigoPromotor");

            if(extras.containsKey("isAdmin"))
                isAdmin = extras.getBoolean("isAdmin");
        }
        else
            Log.v(TAG, "No se recuperaron datos de extras");


        /**
         * LISTA DE TIPO DE PROMOTORES
         * 1 = GERENTE
         * 2 = COORDINADOR
         * 3 = ASISTENTE
         * 2 = ADMIN
         */

        if(isAdmin){
            layoutAdmin.setVisibility(View.VISIBLE);
            layoutGerente.setVisibility(View.GONE);
            layoutCoordinador.setVisibility(View.GONE);
        }else{
            if(Control.getTipoDePromotor() == 1) {
                layoutCoordinador.setVisibility(View.GONE);
                layoutGerente.setVisibility(View.VISIBLE);
                layoutAdmin.setVisibility(View.GONE);
            }else if(Control.getTipoDePromotor() == 2) {
                layoutGerente.setVisibility(View.GONE);
                layoutCoordinador.setVisibility(View.VISIBLE);
                layoutAdmin.setVisibility(View.GONE);
            }
        }




        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        /*spOficinas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nombreOficina =  parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/


        btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!verificarInternet())
                    mostrarError("Parece que no tienes conexión a internet, verifica la conexión");
                else
                {
                    if(camposLlenos())
                    {
                        JSONObject json = new JSONObject();
                        try
                        {
                            json.put("codigo", codigoPromotor);
                            json.put("startDate", etFecha1.getText().toString());
                            json.put("endDate", etFecha2.getText().toString());

                            doRequest(json, root_layout);
                        }catch(JSONException ex) {
                            ex.printStackTrace();
                        }
                    }else
                        Toast.makeText(Reportes.this, "Por favor completa los campos", Toast.LENGTH_LONG).show();
                }
            }
        });

        btBuscarCoordinador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!verificarInternet())
                    mostrarError("Parece que no tienes conexión a internet, verifica la conexión");
                else
                {
                    if(camposLlenosCoordinador())
                    {
                        JSONObject json = new JSONObject();
                        try
                        {
                            json.put("codigo", codigoPromotor);
                            //json.put("codigo", "P3609");
                            json.put("startDate", etFecha1Coordinador.getText().toString());
                            //json.put("startDate", "2020-01-01");
                            json.put("endDate", etFecha2Coordinador.getText().toString());
                            //json.put("endDate", "2020-01-30");

                            doRequestCoordinador(json, root_layout);
                        }catch(JSONException ex) {
                            ex.printStackTrace();
                        }
                    }else
                        Toast.makeText(Reportes.this, "Por favor completa los campos", Toast.LENGTH_LONG).show();
                }
            }
        });

        btBuscarAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!verificarInternet())
                    mostrarError("Parece que no tienes conexión a internet, verifica la conexión");
                else
                {
                    if(camposLlenosAdmin())
                    {
                        JSONObject json = new JSONObject();
                        try
                        {
                            json.put("codigo", codigoPromotor);
                            json.put("startDate", etFecha1Admin.getText().toString());
                            json.put("endDate", etFecha2Admin.getText().toString());
                            json.put("codigoConsulta", etCodigoAdmin.getText().toString());
                            doRequestAdmin(json, root_layout);

                        }catch(JSONException ex) {
                            ex.printStackTrace();
                        }
                    }else
                        Toast.makeText(Reportes.this, "Por favor completa los campos", Toast.LENGTH_LONG).show();
                }
            }
        });



        etFecha1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar calendar= Calendar.getInstance();
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
                int mes=calendar.get(Calendar.MONTH);
                int ano=calendar.get(Calendar.YEAR);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    DatePickerDialog datePickerDialog= new DatePickerDialog(Reportes.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth)
                        {
                            month = month + 1;
                            String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                            String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                            String date = year + "-" + mes + "-" + dia;
                            etFecha1.setText(date);
                        }
                    },ano,mes,dia);
                    datePickerDialog.show();

                } else{
                    DatePickerDialog dialog = new DatePickerDialog(Reportes.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, ano,mes,dia);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }


            }
        });

        etFecha2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar calendar= Calendar.getInstance();
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
                int mes=calendar.get(Calendar.MONTH);
                int ano=calendar.get(Calendar.YEAR);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    DatePickerDialog datePickerDialog= new DatePickerDialog(Reportes.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth)
                        {
                            month = month + 1;
                            String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                            String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                            String date = year + "-" + mes + "-" + dia;
                            etFecha2.setText(date);
                        }
                    },ano,mes,dia);
                    datePickerDialog.show();

                } else{
                    DatePickerDialog dialog = new DatePickerDialog(Reportes.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener2, ano,mes,dia);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                String date = year + "-" + mes + "-" + dia;
                etFecha1.setText(date);
            }
        };


        mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                String date = year + "-" + mes + "-" + dia;
                etFecha2.setText(date);
            }
        };






        mDateSetListenerAdmin = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                String date = year + "-" + mes + "-" + dia;
                etFecha1Admin.setText(date);
            }
        };


        mDateSetListener2Admin = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                String date = year + "-" + mes + "-" + dia;
                etFecha2Admin.setText(date);
            }
        };










        //********
        etFecha1Coordinador.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar calendar= Calendar.getInstance();
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
                int mes=calendar.get(Calendar.MONTH);
                int ano=calendar.get(Calendar.YEAR);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    DatePickerDialog datePickerDialog= new DatePickerDialog(Reportes.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth)
                        {
                            month = month + 1;
                            String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                            String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                            String date = year + "-" + mes + "-" + dia;
                            etFecha1Coordinador.setText(date);
                        }
                    },ano,mes,dia);
                    datePickerDialog.show();

                } else{
                    DatePickerDialog dialog = new DatePickerDialog(Reportes.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, ano,mes,dia);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }


            }
        });

        etFecha2Coordinador.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar calendar= Calendar.getInstance();
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
                int mes=calendar.get(Calendar.MONTH);
                int ano=calendar.get(Calendar.YEAR);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    DatePickerDialog datePickerDialog= new DatePickerDialog(Reportes.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth)
                        {
                            month = month + 1;
                            String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                            String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                            String date = year + "-" + mes + "-" + dia;
                            etFecha2Coordinador.setText(date);
                        }
                    },ano,mes,dia);
                    datePickerDialog.show();

                } else{
                    DatePickerDialog dialog = new DatePickerDialog(Reportes.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener2, ano,mes,dia);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            }
        });





        //****** ADMIN**
        etFecha1Admin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar calendar= Calendar.getInstance();
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
                int mes=calendar.get(Calendar.MONTH);
                int ano=calendar.get(Calendar.YEAR);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    DatePickerDialog datePickerDialog= new DatePickerDialog(Reportes.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth)
                        {
                            month = month + 1;
                            String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                            String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                            String date = year + "-" + mes + "-" + dia;
                            etFecha1Admin.setText(date);
                        }
                    },ano,mes,dia);
                    datePickerDialog.show();

                } else{
                    DatePickerDialog dialog = new DatePickerDialog(Reportes.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListenerAdmin, ano,mes,dia);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }


            }
        });

        etFecha2Admin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar calendar= Calendar.getInstance();
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
                int mes=calendar.get(Calendar.MONTH);
                int ano=calendar.get(Calendar.YEAR);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    DatePickerDialog datePickerDialog= new DatePickerDialog(Reportes.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth)
                        {
                            month = month + 1;
                            String mes = String.valueOf(month).length()<=1 ? "0"+ String.valueOf(month) : String.valueOf(month);
                            String dia = String.valueOf(dayOfMonth).length()<=1 ? "0"+ String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                            String date = year + "-" + mes + "-" + dia;
                            etFecha2Admin.setText(date);
                        }
                    },ano,mes,dia);
                    datePickerDialog.show();

                } else{
                    DatePickerDialog dialog = new DatePickerDialog(Reportes.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener2Admin, ano,mes,dia);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            }
        });

    }

    public boolean verificarInternet()
    {
        ConnectivityManager con = (ConnectivityManager) Reportes.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /*private ArrayList<String> llenarSpinnerOficinas()
    {
        ArrayList<String> COLONIAS = new ArrayList<String>();
        List<Oficinas> oficinas = Oficinas.findWithQuery(Oficinas.class, "SELECT * FROM Oficinas");
        if(oficinas.size()>0)
        {
            for (int i = 0; i < oficinas.size(); i++)
            {
                try
                {
                    COLONIAS.add(oficinas.get(i).getNombre());
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
            Log.i("SIN REGISTROS", "SIN REGISTROS EN OFICINAS");
        return COLONIAS;
    }*/



    boolean camposLlenos()
    {
        return etFecha1.getText().toString().length()>0 && etFecha2.getText().toString().length()>0;
    }

    boolean camposLlenosCoordinador()
    {
        return etFecha1Coordinador.getText().toString().length()>0 && etFecha2Coordinador.getText().toString().length()>0;
    }

    boolean camposLlenosAdmin()
    {
        return etFecha1Admin.getText().toString().length()>0 && etFecha2Admin.getText().toString().length()>0 && etCodigoAdmin.getText().toString().length()>0;
    }

    public void doRequest(JSONObject jsonParams, View view)
    {
        final AdapterOficinas[] adapterCoordinador = new AdapterOficinas[1];
        List<ModelOficinas> productListOficina = new ArrayList<>();
        final GridLayoutManager[] gridLayoutManager = new GridLayoutManager[1];
        RecyclerView rvAfiliaciones = (RecyclerView) findViewById(R.id.rvAfiliaciones);
        ProgressBar progressBarFind = (ProgressBar) view.findViewById(R.id.progressBarFind);
        progressBarFind.setVisibility(View.VISIBLE);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_FIND_AFILIACIONES_GERENTE, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    LinearLayout frameNotFound = (LinearLayout) view.findViewById(R.id.frameNotFound);
                    TextView tvTotalGeneral = (TextView) view.findViewById(R.id.tvTotalGeneral);
                    frameNotFound.setVisibility(View.GONE);
                    int total = 0;

                    if (!response.has("error"))
                    {
                        frameNotFound.setVisibility(View.GONE);
                        productListOficina.clear();

                        JSONObject jsonOficinas = response.getJSONObject("result");

                        //for(int i=0; i <= jsonOficinas.length()-1; i++)
                        //{
                            for (Iterator<String> iter = jsonOficinas.keys(); iter.hasNext();)
                            {
                                JSONObject jsonPorOficina = new JSONObject();

                                String key = iter.next();
                                jsonPorOficina = jsonOficinas.getJSONObject(key);

                                ModelOficinas modelCoordinador = new ModelOficinas(
                                        jsonPorOficina.getString("nombre"),
                                        jsonPorOficina.getString("total"),
                                        jsonPorOficina.getJSONArray("coordinadores")
                                );
                                productListOficina.add(modelCoordinador);

                                total += Integer.parseInt(jsonPorOficina.getString("total"));
                            }

                        //}

                        adapterCoordinador[0] = new AdapterOficinas(Reportes.this, productListOficina);
                        rvAfiliaciones.setHasFixedSize(true);
                        gridLayoutManager[0] = new GridLayoutManager(getApplicationContext(), 1);
                        rvAfiliaciones.setLayoutManager(gridLayoutManager[0]);
                        rvAfiliaciones.setAdapter(adapterCoordinador[0]);
                        tvTotalGeneral.setText("Total general de afiliaciones: " + total);
                        tvTotalGeneral.setVisibility(View.VISIBLE);
                        contenedorDeBusqueda.setVisibility(View.VISIBLE);
                        progressBarFind.setVisibility(View.INVISIBLE);
                    }
                    else {
                        try {
                            adapterCoordinador[0].notifyDataSetChanged();
                            adapterCoordinador[0] = null;
                            productListOficina.clear();

                        }catch (Throwable e)
                        {
                            e.getMessage();
                        }
                        tvTotalGeneral.setVisibility(View.GONE);
                        contenedorDeBusqueda.setVisibility(View.GONE);
                        frameNotFound.setVisibility(View.VISIBLE);
                        progressBarFind.setVisibility(View.INVISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    public void onClickLoading(int position, boolean active)
    {
        ProgressBar progressBarFind = (ProgressBar) root_layout.findViewById(R.id.progressBarFind);
        if(active)
            progressBarFind.setVisibility(View.VISIBLE);
        else
            progressBarFind.setVisibility(View.INVISIBLE);
    }



    public void mostrarError(final String codeError) {
        final Button btNew;
        TextView tvCodeError;
        dialogError.setContentView(R.layout.login_incorrecto);
        dialogError.setCancelable(false);
        btNew = (Button) dialogError.findViewById(R.id.btNew);
        tvCodeError = (TextView) dialogError.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);
        if(codeError.equals("Parece que no tienes conexión a internet, verifica la conexión")) {
            btNew.setText("Aceptar");
        }

        btNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(btNew.getText().toString().equals("Aceptar"))
                {
                    dialogError.dismiss();
                }
            }
        });
        dialogError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogError.show();

    }





    //****Metodos de coordinador***
    public void doRequestCoordinador(JSONObject jsonParams, View view)
    {
        final AdapterCoordinadoresDos[] adapterCoordinador = new AdapterCoordinadoresDos[1];
        List<ModelCoordinador> modelListCoordinador = new ArrayList<>();

        final GridLayoutManager[] gridLayoutManager = new GridLayoutManager[1];

        progressBarFindCoordinador.setVisibility(View.VISIBLE);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_FIND_AFILIACIONES_COORDINADOR, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    frameNotFoundCoordinador.setVisibility(View.GONE);

                    if (!response.has("error"))
                    {
                        frameNotFoundCoordinador.setVisibility(View.GONE);
                        modelListCoordinador.clear();

                        JSONObject jsonCoordinador = response.getJSONObject("result");
                        for (Iterator<String> iter = jsonCoordinador.keys(); iter.hasNext();)
                        {
                            JSONObject jsonPorOficina = new JSONObject();

                            String key = iter.next();
                            jsonPorOficina = jsonCoordinador.getJSONObject(key);

                            ModelCoordinador modelCoordinador = new ModelCoordinador(
                                    jsonPorOficina.getString("nombre"),
                                    jsonPorOficina.getString("codigo"),
                                    jsonPorOficina.getString("total"),
                                    jsonPorOficina.getJSONArray("promotores")
                            );
                            modelListCoordinador.add(modelCoordinador);
                        }


                        adapterCoordinador[0] = new AdapterCoordinadoresDos(Reportes.this, modelListCoordinador);
                        rvCoordinadores.setHasFixedSize(true);
                        gridLayoutManager[0] = new GridLayoutManager(getApplicationContext(), 1);
                        rvCoordinadores.setLayoutManager(gridLayoutManager[0]);
                        rvCoordinadores.setAdapter(adapterCoordinador[0]);
                        rvCoordinadores.setVisibility(View.VISIBLE);
                        contenedorDeBusqueda.setVisibility(View.VISIBLE);
                        progressBarFindCoordinador.setVisibility(View.INVISIBLE);
                    }
                    else {
                        try {
                            adapterCoordinador[0].notifyDataSetChanged();
                            adapterCoordinador[0] = null;
                            modelListCoordinador.clear();
                            rvCoordinadores.setVisibility(View.GONE);
                        }catch (Throwable e)
                        {
                            e.getMessage();
                        }

                        //tvTotalGeneral.setVisibility(View.GONE);
                        //contenedorDeBusqueda.setVisibility(View.GONE);
                        frameNotFoundCoordinador.setVisibility(View.VISIBLE);
                        progressBarFindCoordinador.setVisibility(View.INVISIBLE);
                        rvCoordinadores.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public void doRequestAdmin(JSONObject jsonParams, View view)
    {
        final AdapterOficinas[] adapterCoordinador = new AdapterOficinas[1];
        List<ModelOficinas> productListOficina = new ArrayList<>();
        final GridLayoutManager[] gridLayoutManager = new GridLayoutManager[1];
        progressBarFindAdmin.setVisibility(View.VISIBLE);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_FIND_AFILIACIONES_GERENTE, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    frameNotFoundAdmin.setVisibility(View.GONE);
                    int total = 0;

                    if (!response.has("error"))
                    {
                        frameNotFoundAdmin.setVisibility(View.GONE);
                        productListOficina.clear();

                        JSONObject jsonOficinas = response.getJSONObject("result");


                        for (Iterator<String> iter = jsonOficinas.keys(); iter.hasNext();)
                        {
                            JSONObject jsonPorOficina = new JSONObject();

                            String key = iter.next();
                            jsonPorOficina = jsonOficinas.getJSONObject(key);

                            ModelOficinas modelCoordinador = new ModelOficinas(
                                    jsonPorOficina.getString("nombre"),
                                    jsonPorOficina.getString("total"),
                                    jsonPorOficina.getJSONArray("coordinadores")
                            );
                            productListOficina.add(modelCoordinador);

                            total += Integer.parseInt(jsonPorOficina.getString("total"));
                        }

                        //}

                        adapterCoordinador[0] = new AdapterOficinas(Reportes.this, productListOficina);
                        rvAfiliacionesAdmin.setHasFixedSize(true);
                        gridLayoutManager[0] = new GridLayoutManager(getApplicationContext(), 1);
                        rvAfiliacionesAdmin.setLayoutManager(gridLayoutManager[0]);
                        rvAfiliacionesAdmin.setAdapter(adapterCoordinador[0]);
                        tvTotalGeneralAdmin.setText("Total general de afiliaciones: " + total);
                        tvTotalGeneralAdmin.setVisibility(View.VISIBLE);
                        contenedorDeBusquedaAdmin.setVisibility(View.VISIBLE);
                        progressBarFindAdmin.setVisibility(View.INVISIBLE);
                    }
                    else {
                        try {
                            adapterCoordinador[0].notifyDataSetChanged();
                            adapterCoordinador[0] = null;
                            productListOficina.clear();

                        }catch (Throwable e)
                        {
                            e.getMessage();
                        }
                        tvTotalGeneralAdmin.setVisibility(View.GONE);
                        contenedorDeBusquedaAdmin.setVisibility(View.GONE);
                        frameNotFoundAdmin.setVisibility(View.VISIBLE);
                        progressBarFindAdmin.setVisibility(View.INVISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}