package com.example.sociales.Activities;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.sociales.Adapters.AdapertPDFDescargados;
import com.example.sociales.Adapters.AdapterContract;
import com.example.sociales.Adapters.AdapterLocal;
import com.example.sociales.Adapters.AdapterSolicitudes;
import com.example.sociales.Adapters.ModelContractDetails;
import com.example.sociales.Adapters.ModelPDF;
import com.example.sociales.Adapters.ModelPaymentPerContract;
import com.example.sociales.Adapters.ProductAdapterImages;
import com.example.sociales.Adapters.ProductAdapterPDF;
import com.example.sociales.Adapters.ProductImages;
import com.example.sociales.BuildConfig;
import com.example.sociales.DatabaseLocal.Archivo;
import com.example.sociales.DatabaseLocal.ArchivosDescargados;
import com.example.sociales.DatabaseLocal.Cliente;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.Domicilios;
import com.example.sociales.DatabaseLocal.EsquemaPago;
import com.example.sociales.DatabaseLocal.ExportImportDB;
import com.example.sociales.DatabaseLocal.Localidad_Colonia;
import com.example.sociales.DatabaseLocal.Promotor;
import com.example.sociales.DatabaseLocal.Solicitud;
import com.example.sociales.DatabaseLocal.Solicitud_Cliente;
import com.example.sociales.DatabaseLocal.SolicitudesNuevas;
import com.example.sociales.DatabaseLocal.Ubicacion;
import com.example.sociales.DatabaseLocal.publicidad;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.BaseActivity;
import com.example.sociales.Extras.CallbackSolicitud;
import com.example.sociales.Extras.MyCallBack;
import com.example.sociales.Extras.MyCallBackFromDownloads;
import com.example.sociales.Extras.Preferences;
import com.example.sociales.Extras.VolleySingleton;
import com.example.sociales.R;
import com.example.sociales.Service.Constants;
import com.example.sociales.Service.ForegroundService;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.internal.NavigationMenu;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.orm.SugarRecord;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.github.yavski.fabspeeddial.FabSpeedDial;

import static com.example.sociales.Extras.ApplicationResourcesProvider.getContext;
import static java.lang.Thread.sleep;

public class PrincipalScreen extends BaseActivity implements MyCallBack, MyCallBackFromDownloads, CallbackSolicitud {

    static String TAG="TOMAR_FOTOS_PRINCIPAL_CLASS";
    FrameLayout root_layout;
    //***************** Descargar PDF VARIABLES**************************************************************************
    RelativeLayout background;
    public String nombreDelArchivo="", urlDesdeAdapter="";
    private static String APP_DIRECTORY = "NOMINAS_PDF/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "Recibos_Nomina";
    RecyclerView rvPDF;
    ProductAdapterPDF adapterModelPDF;
    List<ModelPDF> productListModelPDF;
    PDFView pdfView;
    FrameLayout framePDF;
    boolean archivoAbierto = false, cancelar = false;
    LinearLayout layoutLoadPDF;
    LinearLayout frameSinRegistrosDePDF;
    TextView tvDescargados;
    AdapertPDFDescargados adapterArchivosDescargados;
    BottomSheetDialog mBottomSheetDialog, mBottomSheetDialogAfiliaciones;
    //******************************************* FIN VARIABLES **********************************************************

    //********Ultimas afiliaciones**************
    LinearLayout frameNoTenemosAfiliaciones;
    GridLayoutManager gridLayoutManager;
    RecyclerView rvAf;
    TextView tvServ, tvDescripcionServ, tvVerTodasLasAfiliaciones;
    AdapterSolicitudes adapterSol;
    LinearLayout layoutUltimasLoad;
    FrameLayout frameImagesLoad;

    //************************************************************* HOME ************************************************************************************
    private FabSpeedDial btFab;
    Dialog myDialog;

    public String  codigoPromotor="", esquema_pagoWS="", nombrePromotor="";
    ProgressDialog pDialog;
    private BottomSheetDialog bottomSheetDialog, bottomFindDialog;

    static final int PICK_CONTACT_REQUEST = 1;


    //IMAGES
    RecyclerView recyclerView;
    RecyclerView recyclerViewImages;
    ProductAdapterImages adapter;

    List<ProductImages> productList = new ArrayList<>();
    List<ProductImages> productListLocal = new ArrayList<>();;
    List<ModelContractDetails>  productListContractInfo = new ArrayList<>();
    List<ModelPaymentPerContract>  productListPaymentPerContract = new ArrayList<>();
    AdapterLocal adapterLocal;
    boolean sincronizarFotos;

    //---Actualizacion
    private boolean updatePostponed = false;
    private Timer timerUpdate;
    private Boolean downloadingApp = false;
    //-----------
    TextView tvTiempo, tvTextoNombre;
    LinearLayout frameNoInternet;


    //*******************************************************************************************************************************************************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_principal_screen);

        root_layout=(FrameLayout) findViewById(R.id.root_layout);
        //*************** start class *******************************************
        rvPDF = (RecyclerView) findViewById(R.id.rvPDF);
        background = (RelativeLayout) findViewById(R.id.background);
        pdfView = (PDFView) findViewById(R.id.pdfView);
        recyclerView =(RecyclerView) findViewById(R.id.rvImagenes);
        recyclerViewImages =(RecyclerView) findViewById(R.id.rvImagenes);
        framePDF = (FrameLayout) findViewById(R.id.framePDF);
        frameSinRegistrosDePDF = (LinearLayout) findViewById(R.id.frameSinRegistrosDePDF);
        layoutLoadPDF = (LinearLayout) findViewById(R.id.layoutLoadPDF);
        tvDescargados = (TextView) findViewById(R.id.tvDescargados);
        productListModelPDF = new ArrayList<>();
        rvPDF.setHasFixedSize(true);
        rvPDF.setLayoutManager(new LinearLayoutManager(this));
        frameNoInternet=(LinearLayout) findViewById(R.id.frameNoInternet);

        //****** ultimas afiliaciones*********************************
        tvVerTodasLasAfiliaciones=(TextView) findViewById(R.id.tvVerTodasLasAfiliaciones);
        frameNoTenemosAfiliaciones = (LinearLayout) findViewById(R.id.frameNoTenemosAfiliaciones);
        rvAf = (RecyclerView) findViewById(R.id.rvAf);
        layoutUltimasLoad = (LinearLayout) findViewById(R.id.layoutUltimasLoad);

        rvAf.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvAf.setLayoutManager(gridLayoutManager);
    //**********************************************************FIN CLASS**************

        /*CircularProgressBar circularProgressBar = findViewById(R.id.circle1);
        circularProgressBar.setProgress(40f);
        //circularProgressBar.setProgressWithAnimation(65f, Long.parseLong("10")); // =1s
        circularProgressBar.setProgressMax(100f);
        circularProgressBar.setProgressBarColor(Color.BLACK);
        circularProgressBar.setProgressBarColorStart(ContextCompat.getColor(getApplicationContext(), R.color.start_color_gradient));
        circularProgressBar.setProgressBarColorEnd(ContextCompat.getColor(getApplicationContext(), R.color.end_color_gradient));
        circularProgressBar.setProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);

        circularProgressBar.setBackgroundProgressBarColor(ContextCompat.getColor(getApplicationContext(), R.color.color_gray_progress_bar));
        //circularProgressBar.setBackgroundProgressBarColorStart(Color.WHITE);
        //circularProgressBar.setBackgroundProgressBarColorEnd(Color.RED);
        circularProgressBar.setBackgroundProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);
        circularProgressBar.setProgressBarWidth(10f); // in DP
        circularProgressBar.setBackgroundProgressBarWidth(15f); // in DP
        circularProgressBar.setRoundBorder(true);
        circularProgressBar.setStartAngle(0f);
        circularProgressBar.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);


        CircularProgressBar circularProgressBar2 = findViewById(R.id.circle2);
        circularProgressBar2.setProgress(60f);
        //circularProgressBar.setProgressWithAnimation(65f, Long.parseLong("10")); // =1s
        circularProgressBar2.setProgressMax(100f);
        circularProgressBar2.setProgressBarColor(Color.BLACK);
        circularProgressBar2.setProgressBarColorStart(ContextCompat.getColor(getApplicationContext(), R.color.start_color_gradient));
        circularProgressBar2.setProgressBarColorEnd(ContextCompat.getColor(getApplicationContext(), R.color.end_color_gradient));
        circularProgressBar2.setProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);

        circularProgressBar2.setBackgroundProgressBarColor(ContextCompat.getColor(getApplicationContext(), R.color.color_gray_progress_bar));
        //circularProgressBar.setBackgroundProgressBarColorStart(Color.WHITE);
        //circularProgressBar.setBackgroundProgressBarColorEnd(Color.RED);
        circularProgressBar2.setBackgroundProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);
        circularProgressBar2.setProgressBarWidth(7f); // in DP
        circularProgressBar2.setBackgroundProgressBarWidth(7f); // in DP
        circularProgressBar2.setRoundBorder(true);
        circularProgressBar2.setStartAngle(0f);
        circularProgressBar2.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);


        CircularProgressBar circularProgressBar3 = findViewById(R.id.circle3);
        circularProgressBar3.setProgress(80f);
        //circularProgressBar.setProgressWithAnimation(65f, Long.parseLong("10")); // =1s
        circularProgressBar3.setProgressMax(100f);
        circularProgressBar3.setProgressBarColor(Color.BLACK);
        circularProgressBar3.setProgressBarColorStart(ContextCompat.getColor(getApplicationContext(), R.color.start_color_gradient));
        circularProgressBar3.setProgressBarColorEnd(ContextCompat.getColor(getApplicationContext(), R.color.end_color_gradient));
        circularProgressBar3.setProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);

        circularProgressBar3.setBackgroundProgressBarColor(ContextCompat.getColor(getApplicationContext(), R.color.color_gray_progress_bar));
        //circularProgressBar.setBackgroundProgressBarColorStart(Color.WHITE);
        //circularProgressBar.setBackgroundProgressBarColorEnd(Color.RED);
        circularProgressBar3.setBackgroundProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);
        circularProgressBar3.setProgressBarWidth(7f); // in DP
        circularProgressBar3.setBackgroundProgressBarWidth(12f); // in DP
        circularProgressBar3.setRoundBorder(true);
        circularProgressBar3.setStartAngle(0f);
        circularProgressBar3.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);*/


        //******** HOME **********
        frameImagesLoad =(FrameLayout)  findViewById(R.id.frameImagesLoad);
        btFab=(FabSpeedDial)findViewById(R.id.btFab);
        myDialog = new Dialog(this);
        bottomSheetDialog = new BottomSheetDialog(PrincipalScreen.this);
        bottomFindDialog = new BottomSheetDialog(PrincipalScreen.this);
        tvTiempo=(TextView) findViewById(R.id.tvTiempo);
        tvTextoNombre=(TextView) findViewById(R.id.tvTextoNombre);
        startService();
        //btFab.setAnimation(ApplicationResourcesProvider.rapida);


        final Bundle extras = getIntent().getExtras();
        if(extras!=null)
        {
            codigoPromotor= extras.getString("codigo");
            tvTextoNombre.setText(extras.getString("nombre"));
            nombrePromotor= extras.getString("nombre");

            if (extras.containsKey("query")) {
                try {
                    String query = extras.getString("query");
                    Log.e("ServerQuery", "=======================> " + query);
                    if (!query.equals("") && !query.equals("null")) {
                        SugarRecord.executeQuery(query);
                        extras.remove("query");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        else {
            Log.v(TAG, "NO SE ECONTRARON REISTROS DE EXTRAS");
            tvTextoNombre.setText("Hola, Bienvenido");
        }

        try {
            Mint.setUserIdentifier(loadUser());
        } catch (Exception e) {
            e.printStackTrace();
        }

        btFab.setMenuListener(new FabSpeedDial.MenuListener() {

            @SuppressLint("RestrictedApi")
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                /**
                 * 1 -> coordinador
                 * 2-> Gerente
                 * 3 -> Asistente
                 * 4 ->Admin
                 */
                try {
                    if (Control.getTipoDePromotor() == 3) {
                        navigationMenu.getItem(3).setVisible(false);
                    }
                }catch (Throwable e){
                    Log.e(TAG, "onPrepareMenu: " + e.getMessage());
                }

                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem)
            {
                if(menuItem.getTitle().toString().equals("Nueva solicitud"))
                {
                    Intent intent = new Intent(PrincipalScreen.this, SolicitudAndScanner.class);
                    intent.putExtra("codigoPromotor", codigoPromotor);
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                    bottomSheetDialog.dismiss();
                    onPause();
                }
                else if(menuItem.getTitle().toString().equals("Sincronizar"))
                {
                    sincronizarTodo();
                    try {
                        adapterSol.notifyDataSetChanged();
                        adapterModelPDF.notifyDataSetChanged();
                    }catch (Throwable e)
                    {
                        Log.e(TAG, "Error onMenuItemSelected: " + e.getMessage());
                    }
                }
                else if(menuItem.getTitle().toString().equals("Salir"))
                {
                    Toast.makeText(PrincipalScreen.this, "Hasta luego", Toast.LENGTH_SHORT).show();
                    finishAffinity();
                }
                else if(menuItem.getTitle().toString().equals("Consultar contrato"))
                    showBottomDialogFindContract(tvDescripcionServ);
                else if(menuItem.getTitle().toString().equals("Reporte de metas"))
                {
                    Intent intent = new Intent(PrincipalScreen.this, Reportes.class);
                    intent.putExtra("codigoPromotor", codigoPromotor);
                    try {
                        if(Control.getTipoDePromotor() == 1 || Control.getTipoDePromotor() == 2)
                            intent.putExtra("isAdmin", false);
                        else if(Control.getTipoDePromotor() == 4)
                            intent.putExtra("isAdmin", true);
                        startActivity(intent);
                    }catch (Throwable e){
                        Log.e(TAG, "onPrepareMenu: " + e.getMessage());
                    }
                }else if(menuItem.getTitle().toString().equals("Capturar fotografías"))
                {
                    Intent intent = new Intent(PrincipalScreen.this, CapturaSolicitud.class);
                    intent.putExtra("codigoPromotor", codigoPromotor);
                    intent.putExtra("BarrasCorrecto", "0");
                    intent.putExtra("reCapturaDeFotografias", true);
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                    bottomSheetDialog.dismiss();
                }

                return true;
            }

            @Override
            public void onMenuClosed() {
            }
        });


        if(verificarInternet())
        {
            sincronizarFotos=true;
            loadRequestForLocalidadesAndColonia(codigoPromotor);
            frameImagesLoad.setVisibility(View.VISIBLE);
        }
        else
        {
            frameImagesLoad.setVisibility(View.GONE);
            Snackbar.make(recyclerView, "Estas trabajando en modo Offline", Snackbar.LENGTH_LONG).show();


            String querX = "SELECT * FROM PUBLICIDAD";
            List<publicidad> listaPublicidad = publicidad.findWithQuery(publicidad.class, querX);
            if (listaPublicidad.size() > 0)
            {
                for(int i=0; i<=listaPublicidad.size()-1;i++)
                {
                    try
                    {
                        ProductImages product = new ProductImages(listaPublicidad.get(i).getUrl(), listaPublicidad.get(i).getImagen());
                        productListLocal.add(product);
                        adapterLocal = new AdapterLocal(PrincipalScreen.this, productListLocal);
                        recyclerView.setAdapter(adapterLocal);
                        recyclerView.showContextMenu();

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

            }
            else
                Log.v(TAG, "No tienen datos PUBLICIDAD verificarInternet()");


        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                llenarUltimasAfiliaciones();
            }
        }, 2000);

        parametrosForPDFNominas(codigoPromotor);





        tvDescargados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDownloadsPDF(v);
            }
        });


        tvVerTodasLasAfiliaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    adapterSol.notifyDataSetChanged();
                }catch (Throwable e)
                {
                    Log.e(TAG, "Error en verTodasLasAfiliaciones.setOnClickListener(): " + e.getMessage());
                }
                showAfiliacionList(v);
            }
        });



        ExportImportDB exportImportDB = new ExportImportDB();
        try {
            if (exportImportDB.backupDBHidden()) {
                Log.i(TAG, "EXPORTADA CORRECTAMENTE");
            } else {
                Log.i(TAG, "ERROR AL EXPORTAR");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }




    }



    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        try {
            super.onConfigurationChanged(newConfig);
            if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
                Log.e(TAG,"LANDSCAPE");
            }else{

                Log.e(TAG,"PORTRAIT");
            }
        }catch (Throwable e)
        {
            Log.d("ONCONFIG", "Error en onConfigurationChange()");
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClickDownload(int position, String nombrePDF, String url) {

        if(verificarInternetFromDownloads())
        {
            nombreDelArchivo = nombrePDF + ".pdf";
            urlDesdeAdapter = url;

            if(Control.verificarSiTenemosUnArchivoConElMismoNombre(nombreDelArchivo)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalScreen.this);
                builder.setTitle("Información");
                builder.setMessage("Parece que ya tienes un archivo descargado con el mismo nombre, ¿Deseas reemplazarlo?");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        descargarPDFParaGuardarEnMemoriaInterna(url, nombreDelArchivo);
                        Control.actualizarArchivo(nombreDelArchivo, fechaSistema(), urlDesdeAdapter);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PrincipalScreen.this, "Descarga cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.show();
            }
            else
                descargarPDFParaGuardarEnMemoriaInterna(url, nombreDelArchivo);

        }
        else
            showPopup(tvDescargados, "Conectate a internet para descargar tus recibos");




    }

    @Override
    public void onClickOpenDocument(int position, String nombrePDF, String ruta) {
        openPdf(nombrePDF);
        if(mBottomSheetDialog.isShowing() && mBottomSheetDialog!=null)
            mBottomSheetDialog.dismiss();
    }


    private void descargarPDFParaFilesInternosYSerMostrados(final String fileName, final String urlParaDescarga)
    {
        new AsyncTask<Void, Integer, Boolean>()
        {


            @Override
            protected Boolean doInBackground(Void... voids) {
                return descargaParaFilesInternos();
            }

            private Boolean descargaParaFilesInternos()
            {
                try {
                    File file = getFileStreamPath(fileName);
                    if(file.exists())
                        return true;

                    try {
                        FileOutputStream fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
                        URL u = new URL(urlParaDescarga);
                        URLConnection conn = u.openConnection();
                        int contentLength = conn.getContentLength();
                        InputStream input = new BufferedInputStream(u.openStream());
                        byte data[] = new byte[contentLength];
                        long total = 0;
                        int count;
                        while((count = input.read(data)) != -1)
                        {
                            total += count;
                            publishProgress((int) (total * 100) / contentLength);
                            fileOutputStream.write(data, 0, count);
                        }
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        input.close();
                        return true;
                    }catch (final Exception e)
                    {
                        e.printStackTrace();
                        return false;
                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean)
                    openPdf(fileName);
                else
                    Toast.makeText(PrincipalScreen.this, "No podemos abrir el pdf", Toast.LENGTH_SHORT).show();
            }
        }.execute();

    }

    void openPdf(String fileName)
    {
        try {
            File file = getFileStreamPath(fileName);
            framePDF.setVisibility(View.VISIBLE);
            archivoAbierto = true;
            pdfView.fromFile(file)
                    .enableSwipe(true)
                    .swipeHorizontal(false)
                    .onError(new OnErrorListener() {
                        @Override
                        public void onError(Throwable t) {
                            Log.e(TAG, "Error en file openPdf(): " + t.toString());
                            archivoAbierto = false;
                        }
                    }).enableAntialiasing(true)
                    .onRender(new OnRenderListener() {
                        @Override
                        public void onInitiallyRendered(int nbPages) {
                            pdfView.fitToWidth(pdfView.getCurrentPage());
                        }
                    })
                    .spacing(0)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .load();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClickAddPhoto(int position, String solicitud, String codigo_activacion) {
        Intent intent  = new Intent(PrincipalScreen.this, ComprobantesFotografias.class);
        intent.putExtra("reCapturaDeFotos", true);
        intent.putExtra("numeroSolicitud", solicitud);
        intent.putExtra("codigoPromotor", codigoPromotor);
        intent.putExtra("tipoSolicitud", "A");
        intent.putExtra("codigo_add_photo", codigo_activacion);
        startActivity(intent);
        onPause();
    }

    class RetrievePDFStream extends AsyncTask<String, Void, InputStream>
    {

        @Override
        protected InputStream doInBackground(String... strings) {
            return null;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            pdfView.fromStream(inputStream);
        }
    }


    public void parametrosForPDFNominas(String codigo)
    {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("codigo", codigo);
            loadPDFForPromotor(jsonParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPDFForPromotor(JSONObject jsonParams)
    {

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_DOWNLOAD_PDF_FOR_PROMOTOR, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try
                {
                    if (response.has("pdf_nominas"))
                    {
                        JSONArray pdf_nominas_array= response.getJSONArray("pdf_nominas");
                        productListModelPDF.clear();

                        for(int i=0; i<=pdf_nominas_array.length()-1; i++)
                        {
                            JSONObject jsonNominas = pdf_nominas_array.getJSONObject(i);
                            ModelPDF modelPDF = new ModelPDF(jsonNominas.getString("nombre"), jsonNominas.getString("url"));
                            productListModelPDF.add(modelPDF);
                        }

                        adapterModelPDF= new ProductAdapterPDF (PrincipalScreen.this, productListModelPDF, PrincipalScreen.this);
                        rvPDF.setAdapter(adapterModelPDF);
                        layoutLoadPDF.setVisibility(View.GONE);
                        rvPDF.setVisibility(View.VISIBLE);
                        Log.v("PDF_NOMINAS", "Nominas descargadas correctamente");

                    }else if(response.has("error"))
                    {
                        layoutLoadPDF.setVisibility(View.GONE);
                        frameSinRegistrosDePDF.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Log.d(TAG, "Error en LoadPromotorPDF()" + e.toString());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        layoutLoadPDF.setVisibility(View.GONE);
                        frameSinRegistrosDePDF.setVisibility(View.VISIBLE);
                        Log.d(TAG, "Error en onErrorResponse()"+ error.toString());
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public void descargarPDFParaGuardarEnMemoriaInterna(String urlPDFPorPosicion, String nombrePDF)
    {

        try {
            cancelar = false;
            ProgressDialog progressDialog = new ProgressDialog(PrincipalScreen.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setTitle("Descargando");
            progressDialog.setMessage("Su documento " + nombrePDF + " estará listo en un momento...");
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCELAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar = true;
                    new descargarPDFAsyncTaskParaGuardarEnMemoriaInterna(progressDialog).cancel(true);
                    Toast.makeText(PrincipalScreen.this, "Descarga cancelada", Toast.LENGTH_SHORT).show();
                    //progressDialog.dismiss();
                }
            });

            new descargarPDFAsyncTaskParaGuardarEnMemoriaInterna(progressDialog).execute(urlPDFPorPosicion);
        }catch (Throwable e){
            Log.e(TAG, "descargarPDFParaGuardarEnMemoriaInterna: " + e.toString());
        }
    }

    private class descargarPDFAsyncTaskParaGuardarEnMemoriaInterna extends AsyncTask<String, Integer, String>
    {
        ProgressDialog progressDialog;

        private descargarPDFAsyncTaskParaGuardarEnMemoriaInterna(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urlPDF) {
            String urlParaDescargar = urlPDF[0];

            HttpURLConnection conexion = null;
            InputStream input = null;
            OutputStream output = null;

            File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
            boolean isDirectoryCreated = file.exists();

            if(!isDirectoryCreated)
                isDirectoryCreated = file.mkdirs();

            if(isDirectoryCreated){
                try {

                    URL url = new URL(urlParaDescargar);
                    conexion = (HttpURLConnection) url.openConnection();
                    conexion.connect();

                    if(conexion.getResponseCode() != HttpURLConnection.HTTP_OK)
                    {
                        return "Conexión no se realizó correctamente";
                    }

                    input = conexion.getInputStream();
                    String rutaDelFicheroGuardado = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator +"/"+nombreDelArchivo;

                    File newFile = new File(rutaDelFicheroGuardado);
                    try
                    {
                        newFile.createNewFile();
                    }
                    catch (IOException e)
                    {
                        Log.e("Error: ",  e.getMessage());
                    }

                    output = new FileOutputStream(rutaDelFicheroGuardado);

                    int tamanoFichero = conexion.getContentLength();


                    byte[] data = new byte[1024];
                    int total =0;
                    int count;

                    while((count = input.read(data)) != -1)
                    {
                        sleep(50);
                        output.write(data, 0, count);
                        total += count;
                        publishProgress((int) (total * 100 /tamanoFichero));
                    }

                }catch (MalformedURLException e)
                {
                    e.printStackTrace();
                    return "Error: " + e.getMessage();
                }catch (IOException e)
                {
                    e.printStackTrace();
                    return "Error: " + e.getMessage();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if(input != null) input.close();
                        if(output != null) output.close();
                        if(conexion != null) conexion.disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return "Se realizo la descarga correctamente";
        }

        @Override
        protected void onProgressUpdate(Integer... progreso) {
            super.onProgressUpdate(progreso);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(progreso[0]);
        }

        @Override
        protected void onPostExecute(String mensaje) {
            super.onPostExecute(mensaje);

            if(!cancelar) {
                try {
                    progressDialog.dismiss();
                } catch (Throwable e) {
                    Log.e(TAG, "onPostExecute: " + e.toString());
                }
                Toast.makeText(PrincipalScreen.this, mensaje, Toast.LENGTH_SHORT).show();

                descargarPDFParaFilesInternosYSerMostrados(nombreDelArchivo, urlDesdeAdapter);
                new RetrievePDFStream().execute(urlDesdeAdapter);
                Control.insertarArchivosGuardados(nombreDelArchivo, fechaSistema(), urlDesdeAdapter);
                adapterModelPDF.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        if(archivoAbierto) {

            framePDF.setVisibility(View.GONE);
            archivoAbierto= false;
            showDownloadsPDF(tvServ);
        }
        else
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(archivoAbierto) {

                framePDF.setVisibility(View.GONE);
                archivoAbierto= false;
                showDownloadsPDF(tvServ);
            }
            else
            {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            }

            return true;
        }
        return false;
    }




    public String fechaSistema()
    {
        String fecha="";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        fecha = dateFormat.format(date);
        return  fecha;
    }

    public void llenarUltimasAfiliaciones()
    {
        /*String query = "SELECT * FROM SOLICITUDCLIENTE ORDER BY id DESC limit 5";
        List<Solicitud_Cliente> listaSolicitudes = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, query);
        if(listaSolicitudes.size()>0) {
            adapterSol = new AdapterSolicitudes(getApplicationContext(), listaSolicitudes, PrincipalScreen.this);
        }
        else
            frameNoTenemosAfiliaciones.setVisibility(View.VISIBLE);

        rvAf.setAdapter(adapterSol);
        layoutUltimasLoad.setVisibility(View.GONE);
        rvAf.setVisibility(View.VISIBLE);*/


        String query = "SELECT * FROM SOLICITUDES_NUEVAS ORDER BY id DESC limit 5";
        List<SolicitudesNuevas> listaSolicitudes = SolicitudesNuevas.findWithQuery(SolicitudesNuevas.class, query);
        if(listaSolicitudes.size()>0) {
            adapterSol = new AdapterSolicitudes(getApplicationContext(), listaSolicitudes, PrincipalScreen.this);
        }
        else
            frameNoTenemosAfiliaciones.setVisibility(View.VISIBLE);

        rvAf.setAdapter(adapterSol);
        layoutUltimasLoad.setVisibility(View.GONE);
        rvAf.setVisibility(View.VISIBLE);
    }

    public void showDownloadsPDF(View view) {
        try {
            RecyclerView rvPDF;
            LinearLayout frameNoTenemosDescargas;
            GridLayoutManager gridLayoutManagerFromDownloads;
            mBottomSheetDialog = new BottomSheetDialog(PrincipalScreen.this);
            mBottomSheetDialog.setContentView(R.layout.bottom_dialog_downloads);
            rvPDF = (RecyclerView) mBottomSheetDialog.findViewById(R.id.rvPDF);
            frameNoTenemosDescargas = (LinearLayout) mBottomSheetDialog.findViewById(R.id.frameNoTenemosDescargas);

            rvPDF.setHasFixedSize(true);
            gridLayoutManagerFromDownloads = new GridLayoutManager(this, 1);
            rvPDF.setLayoutManager(gridLayoutManagerFromDownloads);

            String query = "SELECT * FROM ARCHIVOS_DESCARGADOS ORDER BY id DESC limit 10";
            List<ArchivosDescargados> listaArchivos = ArchivosDescargados.findWithQuery(ArchivosDescargados.class, query);
            if (listaArchivos.size() > 0) {
                adapterArchivosDescargados = new AdapertPDFDescargados(getApplicationContext(), listaArchivos, PrincipalScreen.this);
                rvPDF.setAdapter(adapterArchivosDescargados);
                rvPDF.setVisibility(View.VISIBLE);
            } else {
                rvPDF.setVisibility(View.GONE);
                frameNoTenemosDescargas.setVisibility(View.VISIBLE);
            }

            mBottomSheetDialog.show();
        } catch (Throwable e) {
            Log.e(TAG, "Error en showDownloadsPDF(): " + e.toString());
        }

    }

    public void showAfiliacionList(View view) {
        try {
            RecyclerView rvAfiliaciones;
            LinearLayout frameNoTenemosAfiliaciones;
            GridLayoutManager gridLayoutManagerFromAfiliaciones;
            AdapterSolicitudes adapterAfiliaciones;
            mBottomSheetDialogAfiliaciones = new BottomSheetDialog(PrincipalScreen.this);
            mBottomSheetDialogAfiliaciones.setContentView(R.layout.bottom_dialog_afiliaciones);

            rvAfiliaciones = (RecyclerView) mBottomSheetDialogAfiliaciones.findViewById(R.id.rvAfiliaciones);
            frameNoTenemosAfiliaciones = (LinearLayout) mBottomSheetDialogAfiliaciones.findViewById(R.id.frameNoTenemosAfiliaciones);

            rvAfiliaciones.setHasFixedSize(true);
            gridLayoutManagerFromAfiliaciones = new GridLayoutManager(this, 1);
            rvAfiliaciones.setLayoutManager(gridLayoutManagerFromAfiliaciones);

            String query = "SELECT * FROM SOLICITUDES_NUEVAS ORDER BY id DESC";
            List<SolicitudesNuevas> listaSolicitudes = SolicitudesNuevas.findWithQuery(SolicitudesNuevas.class, query);
            if (listaSolicitudes.size() > 0) {
                adapterAfiliaciones= new AdapterSolicitudes(getApplicationContext(), listaSolicitudes, PrincipalScreen.this);
                rvAfiliaciones.setAdapter(adapterAfiliaciones);
                frameNoTenemosAfiliaciones.setVisibility(View.GONE);
                rvAfiliaciones.setVisibility(View.VISIBLE);
            }
            else
            {
                rvAfiliaciones.setVisibility(View.GONE);
                frameNoTenemosAfiliaciones.setVisibility(View.VISIBLE);
            }

            mBottomSheetDialogAfiliaciones.show();
        } catch (Throwable e) {
            Log.e(TAG, "Error en showAfiliacionList(): " + e.toString());
        }

    }




    //***************************************************** HOME **********************************************************
    /*private void consultaDeTablas()
    {
        List<Cliente> developers = Cliente.listAll(Cliente.class);
        for(Cliente developer : developers){
            Log.v("CLIENTES--->", developer.getId() +
                    ", " + developer.getNombre() +
                    ", " + developer.getPaterno() +
                    ", " + developer.getMaterno()
            );
        }

        List<Archivo> developers2 = Archivo.listAll(Archivo.class);
        for(Archivo developer2 : developers2){
            Log.v("CLIENTES--->", developer2.getId() +
                    ", " + developer2.getArchivoNombre() +
                    ", " + developer2.getArchivoSolicitudClienteCodigoActivacion() +
                    ", " + developer2.getArchivoTipoArchivo()
            );
        }
    }*/

    public void loadRequestForLocalidadesAndColonia(String codigo)
    {
        JSONObject jsonParams = new JSONObject();
        try {
            int lastLocalidadColoniaID = Control.getLastLocalidadColoniaInserted();
            jsonParams.put("codigo", codigo);
            jsonParams.put("localidad_colonia_id", lastLocalidadColoniaID);
            requestDownloadCatalogosDeLocalidades(jsonParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestDownloadCatalogosDeLocalidades(JSONObject jsonParams)
    {
        pDialog=new ProgressDialog(PrincipalScreen.this);
        pDialog.setMessage("Actualizando información...");
        pDialog.setCancelable(false);
        pDialog.show();
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_GET_CATALOGOS_SOLICITUDES, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.has("error"))
                            {
                                JSONObject json = response.getJSONObject("result");

                                JSONArray arregloSolicitudes = json.getJSONArray("solicitudes");
                                if(arregloSolicitudes.length()>0) {
                                    Solicitud.deleteAll(Solicitud.class);
                                    for (int i = 0; i <= arregloSolicitudes.length() - 1; ) {
                                        JSONObject jsonTmp = arregloSolicitudes.getJSONObject(i);
                                        Control.insertarSolicitud(
                                                jsonTmp.getString("solicitud_serie"),
                                                codigoPromotor,
                                                jsonTmp.getString("tipo_solicitud"),
                                                jsonTmp.getString("letra_indice"),
                                                jsonTmp.getString("usada"));
                                        i++;
                                    }
                                }

                                JSONArray arregloDirecciones  = json.getJSONArray("direcciones");
                                if ( arregloDirecciones.length() > 0 ) {
                                    Localidad_Colonia.deleteAll(Localidad_Colonia.class);
                                    for(int i=0; i<=arregloDirecciones.length()-1;)
                                    {
                                        JSONObject jsonTmp = arregloDirecciones.getJSONObject(i);
                                        Control.insertarLocalidadColonia(
                                                jsonTmp.getString("localidad"),
                                                jsonTmp.getString("colonia"),
                                                jsonTmp.getInt("ID"));
                                        i++;
                                    }
                                }

                                JSONArray arregloEsquemasDePago  = json.getJSONArray("esquema_pago");
                                if(arregloEsquemasDePago.length()>0) {
                                    EsquemaPago.deleteAll(EsquemaPago.class);
                                    for(int i=0; i<=arregloEsquemasDePago.length()-1;)
                                    {
                                        JSONObject jsonTmp = arregloEsquemasDePago.getJSONObject(i);
                                        Control.insertarEsquemaDePago(jsonTmp.getString("esquema_pago"));
                                        i++;
                                    }
                                }


                                if(pDialog != null)
                                    pDialog.dismiss();
                            }
                            else
                            {
                                String error = response.getString("error");
                                showPopup(tvDescargados, error);
                                if(pDialog != null)
                                    pDialog.dismiss();
                            }

                            if(pDialog != null)
                                pDialog.dismiss();

                            if(sincronizarFotos)
                            {
                                loadImages();
                                if(pDialog != null)
                                    pDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(pDialog != null)
                                pDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        try {
                            if (pDialog != null)
                                pDialog.dismiss();
                        }catch (Throwable e){
                            Log.e(TAG, "onErrorResponse: " + e.getMessage());
                        }
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
    public void showPopup(View view, final String codeError) {
        final Button btNew;
        TextView tvCodeError;
        myDialog.setContentView(R.layout.login_incorrecto);
        myDialog.setCancelable(false);
        btNew = (Button) myDialog.findViewById(R.id.btNew);
        tvCodeError = (TextView) myDialog.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);
        if(codeError.equals("Lo sentimos; Tienes mas de 15 días sin afiliaciones, no es posible acceder al sistema")) {
            btNew.setText("Salir del sistema");
        }else if(codeError.equals("Lo sentimos no podemos sincronizar sin internet"))
        {
            btNew.setText("Aceptar");
        }
        else if(codeError.equals("Conectate a internet para descargar tus recibos"))
        {
            btNew.setText("Aceptar");
        }

        btNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(btNew.getText().toString().equals("Salir del sistema"))
                {
                    finishAffinity();
                }
                if(btNew.getText().toString().equals("Aceptar"))
                {
                    myDialog.dismiss();
                }
                else
                {
                    myDialog.dismiss();
                }
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }

    @SuppressLint("ClickableViewAccessibility")
    public void mostarBottom(View view)
    {
        CardView btSolicitud, btEscanner;
        View dialog = getLayoutInflater().inflate(R.layout.bottom_layout_camera, null);
        bottomSheetDialog.setContentView(dialog);
        bottomSheetDialog.show();
        btSolicitud =(CardView) dialog.findViewById(R.id.btSolcitud);
        btEscanner=(CardView) dialog.findViewById(R.id.btEscanner);

        btSolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalScreen.this, ScannerBarras.class);
                intent.putExtra("codigoPromotor", codigoPromotor);
                startActivityForResult(intent, PICK_CONTACT_REQUEST);
                bottomSheetDialog.dismiss();
                onPause();
            }
        });

        btEscanner.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    Intent intent = new Intent(PrincipalScreen.this, CapturaSolicitud.class);
                    intent.putExtra("codigoPromotor", codigoPromotor);
                    intent.putExtra("BarrasCorrecto", "0");
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                    bottomSheetDialog.dismiss();
                }
                return true;
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public void showBottomDialogFindContract(View view)
    {
        RecyclerView recyclerViewContractFindSuccess;
        final GridLayoutManager[] gridLayoutManagerFromAfiliaciones = new GridLayoutManager[1];
        EditText etContrato, etSolicitud;
        Button btBuscar;
        ProgressBar progressBarFind;
        LinearLayout layoutBusqueda;
        View dialog = getLayoutInflater().inflate(R.layout.bottom_layout_find_contract, null);
        bottomFindDialog.setContentView(dialog);
        bottomFindDialog.show();

        recyclerViewContractFindSuccess =(RecyclerView) dialog.findViewById(R.id.recyclerViewContractFindSuccess);
        layoutBusqueda = (LinearLayout) dialog.findViewById(R.id.layoutBusqueda);
        etContrato =(EditText) dialog.findViewById(R.id.etContrato);
        etSolicitud=(EditText) dialog.findViewById(R.id.etSolicitud);
        btBuscar =(Button) dialog.findViewById(R.id.btBuscar);
        progressBarFind =(ProgressBar) dialog.findViewById(R.id.progressBarFind);


        bottomFindDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                productListContractInfo.clear();
                productListPaymentPerContract.clear();
            }
        });


        btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(verificarInternetFromDownloads())
                {
                    progressBarFind.setVisibility(View.VISIBLE);
                    btBuscar.setEnabled(false);
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonParams.put("contrato", etContrato.getText().toString());
                        jsonParams.put("solicitud", etSolicitud.getText().toString());
                        jsonParams.put("promotorCodigo", codigoPromotor);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error en contrucción de parametros para request de busqueda de contrato en web: " + e.getMessage());
                        if(progressBarFind.getVisibility() == View.VISIBLE)
                            progressBarFind.setVisibility(View.INVISIBLE);
                        btBuscar.setEnabled(true);
                    }

                    JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_FIND_CONTRACT, jsonParams, new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                if (!response.has("error"))
                                {
                                    AdapterContract adapterContract;
                                    String fechaContrato="";
                                    JSONObject jsonResult = response.getJSONObject("contrato_info");

                                    if(jsonResult.has("fecha_contrato"))
                                        fechaContrato = jsonResult.getString("fecha_contrato");
                                    else
                                        fechaContrato="Sin detalles";


                                    productListContractInfo.clear();
                                    ModelContractDetails productModelContractDetails = new ModelContractDetails(
                                            ""+ jsonResult.getString("contrato"),
                                            ""+ jsonResult.getString("nombre"),
                                            ""+jsonResult.getString("solicitud"),
                                            ""+jsonResult.getString("nombre_plan"),
                                            ""+jsonResult.getString("costo"),
                                            ""+jsonResult.getString("domicilio"),
                                            ""+jsonResult.getString("saldo"),
                                            ""+jsonResult.getString("forma_pago_actual"),
                                            ""+jsonResult.getString("monto_pago_actual"),
                                            ""+ jsonResult.getString("abonado"),
                                            ""+jsonResult.getString("detalle_servicio"),
                                            ""+jsonResult.getString("estatus"),
                                            fechaContrato
                                    );
                                    productListContractInfo.add(productModelContractDetails);


                                    JSONArray arregloPagos = response.getJSONArray("pagos");
                                    if(arregloPagos.length()>0)
                                    {
                                        productListPaymentPerContract.clear();
                                        for (int i = 0; i <= arregloPagos.length() - 1; )
                                        {
                                            JSONObject jsonTmp = arregloPagos.getJSONObject(i);
                                            ModelPaymentPerContract productModelPayments = new ModelPaymentPerContract(
                                                    ""+ jsonTmp.getString("folio"),
                                                    ""+ jsonTmp.getString("fecha"),
                                                    ""+jsonTmp.getString("hora"),
                                                    ""+jsonTmp.getString("monto"),
                                                    ""+jsonTmp.getString("cobrador")
                                            );
                                            productListPaymentPerContract.add(productModelPayments);
                                            i++;
                                        }
                                    }



                                    if(progressBarFind.getVisibility() == View.VISIBLE)
                                        progressBarFind.setVisibility(View.INVISIBLE);

                                    adapterContract= new AdapterContract (PrincipalScreen.this, productListContractInfo, productListPaymentPerContract);
                                    recyclerViewContractFindSuccess.setHasFixedSize(true);
                                    gridLayoutManagerFromAfiliaciones[0] = new GridLayoutManager(getApplicationContext(), 1);
                                    recyclerViewContractFindSuccess.setLayoutManager(gridLayoutManagerFromAfiliaciones[0]);
                                    recyclerViewContractFindSuccess.setAdapter(adapterContract);
                                    recyclerViewContractFindSuccess.setVisibility(View.VISIBLE);
                                    btBuscar.setEnabled(true);


                                    layoutBusqueda.setEnabled(false);
                                    layoutBusqueda.setVisibility(View.GONE);

                                }
                                else
                                {
                                    String error = response.getString("error");
                                    showPopup(etContrato, error);
                                    Log.e(TAG, "Error en contrucción de parametros para request de busqueda de contrato en web: " + error);
                                    if(progressBarFind.getVisibility() == View.VISIBLE)
                                        progressBarFind.setVisibility(View.INVISIBLE);
                                    btBuscar.setEnabled(true);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error en contrucción de parametros para request de busqueda de contrato en web: " + e.getMessage());
                                if(progressBarFind.getVisibility() == View.VISIBLE)
                                    progressBarFind.setVisibility(View.INVISIBLE);
                                btBuscar.setEnabled(true);
                            }
                        }
                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();
                                    Log.e(TAG, "Error en contrucción de parametros para request de busqueda de contrato en web: " + error.getMessage());
                                    if(progressBarFind.getVisibility() == View.VISIBLE)
                                        progressBarFind.setVisibility(View.INVISIBLE);
                                    btBuscar.setEnabled(true);
                                }
                            }) {

                    };
                    VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                }
                else
                    showPopup(etContrato, "Para buscar el contrato necesitas estar conectado a internet");
            }
        });


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK)
                Log.e(TAG, "RESULT_OK");
            else
            {
                try
                {
                    AdapterSolicitudes adapterAfiliaciones;
                    String query = "SELECT * FROM SOLICITUDES_NUEVAS ORDER BY id DESC limit 5";
                    List<SolicitudesNuevas> listaSolicitudes = SolicitudesNuevas.findWithQuery(SolicitudesNuevas.class, query);
                    if (listaSolicitudes.size() > 0) {
                        adapterAfiliaciones= new AdapterSolicitudes(getApplicationContext(), listaSolicitudes, PrincipalScreen.this);
                        rvAf.setAdapter(adapterAfiliaciones);
                        frameNoTenemosAfiliaciones.setVisibility(View.GONE);
                        layoutUltimasLoad.setVisibility(View.GONE);
                        rvAf.setVisibility(View.VISIBLE);
                        adapterAfiliaciones.notifyDataSetChanged();
                    }
                    else
                    {
                        //layoutUltimasLoad.setVisibility(View.VISIBLE);
                        rvAf.setVisibility(View.GONE);
                        frameNoTenemosAfiliaciones.setVisibility(View.VISIBLE);
                    }
                }
                catch (Throwable e)
                {
                    e.printStackTrace();
                }
            }
        }



    }

    @Override
    public final void onDestroy()
    {
        super.onDestroy();
        stopService();
    }

    public void startService()
    {
        try {
            Intent serviceIntent = new Intent(this, ForegroundService.class);
            serviceIntent.putExtra("inputExtra", Constants.ACTION.STARTFOREGROUND_ACTION);
            ContextCompat.startForegroundService(this, serviceIntent);
        }catch (Throwable e)
        {
            e.printStackTrace();
        }

    }

    public void stopService() {
        try {
            Intent serviceIntent = new Intent(this, ForegroundService.class);
            serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);
            stopService(serviceIntent);
        }catch (Throwable e) {
            e.printStackTrace();
        }
    }


    public void creacionDeJsonParaRealizarLaCargaDeSolicitudes()
    {
        JSONArray arrarJSONGeneral = new JSONArray();
        JSONObject jsonSolicitud = new JSONObject();

        String CodigoActivacion="", SolicitudSerie="", ClienteID="";
        String query="SELECT * FROM SOLICITUDCLIENTE where sync = 0";
        List<Solicitud_Cliente> listaSolicitudesCliente = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, query);
        try
        {
            jsonSolicitud.put("codigo", codigoPromotor);

        }catch(JSONException ex)
        {
            ex.printStackTrace();
        }


        if(listaSolicitudesCliente.size()>0)
        {
            for (int i = 0; i <= listaSolicitudesCliente.size() - 1; i++) {

                JSONObject jsonSolicitudInterna = new JSONObject();
                ClienteID = listaSolicitudesCliente.get(i).getSolCtle_ClienteID();
                SolicitudSerie = listaSolicitudesCliente.get(i).getSolCtle_SolicitudSerie();
                CodigoActivacion = listaSolicitudesCliente.get(i).getSolCtle_CodigoActivacion();
                try
                {
                    jsonSolicitudInterna.put("solicitud", new JSONObject(new Gson().toJson(listaSolicitudesCliente.get(i))));
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


                JSONObject jsonDatosDentroDelCliente = new JSONObject();
                String queryClientes = "SELECT * FROM CLIENTE WHERE id='" + ClienteID + "'";
                List<Cliente> listadoClienteFiltrado = Cliente.findWithQuery(Cliente.class, queryClientes);
                if (listadoClienteFiltrado.size() > 0) {
                    try {

                        jsonDatosDentroDelCliente.put("fechanacimiento", listadoClienteFiltrado.get(0).getFechanacimiento());
                        jsonDatosDentroDelCliente.put("materno", listadoClienteFiltrado.get(0).getMaterno());
                        jsonDatosDentroDelCliente.put("nombre", listadoClienteFiltrado.get(0).getNombre());
                        jsonDatosDentroDelCliente.put("observaciones", listadoClienteFiltrado.get(0).getObservaciones());
                        jsonDatosDentroDelCliente.put("paterno", listadoClienteFiltrado.get(0).getPaterno());
                        jsonDatosDentroDelCliente.put("rfc", listadoClienteFiltrado.get(0).getRfc());
                        jsonDatosDentroDelCliente.put("sync", listadoClienteFiltrado.get(0).getSync());
                        jsonDatosDentroDelCliente.put("telefono", listadoClienteFiltrado.get(0).getTelefono());
                        jsonDatosDentroDelCliente.put("id", listadoClienteFiltrado.get(0).getId());
                        jsonDatosDentroDelCliente.put("estado_civil", listadoClienteFiltrado.get(0).getEstadoCivil());

                        jsonSolicitudInterna.put("cliente", jsonDatosDentroDelCliente);

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }


                String queryUbicacione = "SELECT * FROM UBICACION WHERE solicitudcliente='" + SolicitudSerie + "'";
                List<Ubicacion> listadoUbicaciones = Ubicacion.findWithQuery(Ubicacion.class, queryUbicacione);
                if (listadoUbicaciones.size() > 0) {
                    try {
                        jsonSolicitudInterna.put("ubicacion", new JSONObject(new Gson().toJson(listadoUbicaciones.get(0))));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }

                //----------------------------carga de domicilios para json-------------------------------------------------
                String queryDomicilios = "SELECT * FROM DOMICILIOS WHERE id_cliente_from_suagar='" + ClienteID + "'";
                List<Domicilios> domiciliosList = Domicilios.findWithQuery(Domicilios.class, queryDomicilios);
                JSONArray jsonArrayDomicilios = new JSONArray();

                if (domiciliosList.size() > 0) {

                    for (int y = 0; y <= domiciliosList.size() - 1; y++)
                    {
                        JSONObject jsonDomicilio = new JSONObject();
                        try
                        {
                            jsonDomicilio.put("tipodomicilio", domiciliosList.get(y).getTipoDomicilio());
                            jsonDomicilio.put("calle", domiciliosList.get(y).getCalle());
                            jsonDomicilio.put("colonia", domiciliosList.get(y).getColonia());
                            jsonDomicilio.put("localidad", domiciliosList.get(y).getLocalidad());
                            jsonDomicilio.put("numext", domiciliosList.get(y).getNumeroExterior());
                            jsonDomicilio.put("numint", domiciliosList.get(y).getNumeroInterior());
                            jsonDomicilio.put("entrecalles", domiciliosList.get(y).getEntreCalles());
                            jsonDomicilio.put("localiadcoloniaID", Control.getIDFromLocalidadColonia(domiciliosList.get(y).getColonia(), domiciliosList.get(y).getLocalidad()));
                            jsonDomicilio.put("codigopostal", domiciliosList.get(y).getCodigoPostal());
                            jsonDomicilio.put("estado", "AQUI VA EL ESTADO");
                            jsonDomicilio.put("descripcion", "AQUI VA LA DESCRIPCION");

                            jsonArrayDomicilios.put(jsonDomicilio);
                        }
                        catch (JSONException ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                }
                try {
                    jsonSolicitudInterna.put("domicilios", jsonArrayDomicilios);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
                //-----------------------------------------------------------------------------

                String queryArchivos = "SELECT * FROM ARCHIVO WHERE codigo='" + CodigoActivacion + "'";
                List<Archivo> listadoArchivos = Archivo.findWithQuery(Archivo.class, queryArchivos);

                JSONArray arrayJSON = new JSONArray();
                if (listadoArchivos.size() > 0) {

                    for (int y = 0; y <= listadoArchivos.size() - 1; y++)
                    {
                        JSONObject jsonArchivos = new JSONObject();
                        try
                        {
                            jsonArchivos.put("nombre", listadoArchivos.get(y).getArchivoNombre());
                            jsonArchivos.put("latitud", listadoArchivos.get(y).getLatitud());
                            jsonArchivos.put("longitud", listadoArchivos.get(y).getLongitud());
                            jsonArchivos.put("fecha_captura", listadoArchivos.get(y).getFecha());
                            jsonArchivos.put("codigo_activacion", listadoArchivos.get(y).getArchivoSolicitudClienteCodigoActivacion());
                            arrayJSON.put(jsonArchivos);
                        }
                        catch (JSONException ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                }

                try {
                    jsonSolicitudInterna.put("archivos", arrayJSON);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

                arrarJSONGeneral.put(jsonSolicitudInterna);
            }
            try {
                jsonSolicitud.put("solicitudes", arrarJSONGeneral);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            cargaDeSolicitudesAServidorWeb(jsonSolicitud);
        }
        else {
            Log.e(TAG, "No tenemos registros de consulta de solicitud cliente.");
        }
    }


    public void cargaDeSolicitudesAServidorWeb(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_CARGA_SOLICITUDES_A_WEB, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    if (!response.has("error"))
                    {
                        JSONArray jsonSuccess = response.getJSONArray("success");
                        JSONArray jsonFail = response.getJSONArray("fail");

                        if(jsonSuccess.length()>0)
                        {
                            for(int i=0; i<=jsonSuccess.length()-1; i++)
                            {
                                JSONObject object = jsonSuccess.getJSONObject(i);
                                JSONObject solicitud = object.getJSONObject("solicitud");
                                String query="UPDATE SOLICITUDCLIENTE SET sync=2 WHERE solicitudserie='" + solicitud.getString("solicitudserie") +"'";
                                Solicitud_Cliente.executeQuery(query);
                            }

                            Log.d(TAG, "Json success: SincronizarTodo()");
                        }
                        else
                            Log.d(TAG, "JSON SUCCESS no contiene datos");



                        if (jsonFail.length()>0)
                        {
                            String result="";
                            for(int y=0; y<jsonFail.length()-1;y++)
                            {
                                JSONObject object = jsonFail.getJSONObject(y);
                                //JSONObject solicitud = object.getJSONObject("solicitud");
                                 result = object.getString("RESULTADO");
                            }
                            Log.d(TAG, "JSON Fail: SincronizarTodo(): " + result);
                        }
                        else
                            Log.d(TAG, "JSON FAIL no contiene datos");


                        if(response.has("query"))
                        {
                            try {
                                String queryJSON = response.getString("query");
                                Log.e("ServerQuery", "from SincronizarTodo " + queryJSON);
                                if (!queryJSON.equals("") && !queryJSON.equals("null")) {
                                    SugarRecord.executeQuery(queryJSON);
                                    response.remove("query");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        else
                            Log.d(TAG, "JSON Query no contiene datos");
                    }
                    else
                    {
                        String error = response.getString("error");
                        Log.d(TAG, "Error: SincronizarTodo:   " + error);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Error: SincronizarTodo:   " + e.getMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.d(TAG, "Error: onErrorResponse():   " + error.getMessage());
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void loadImages()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_IMAGENES_PUBLICIDAD2, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String result)
            {
                try
                {
                    JSONObject object  = new JSONObject(result);
                    JSONArray resultImages= object.getJSONArray("result");
                    productList.clear();
                    for(int i=0; i<=resultImages.length()-1; i++)
                    {
                        JSONObject jsonTmp = resultImages.getJSONObject(i);
                        ProductImages product = new ProductImages(jsonTmp.getString("nombre"), "");
                        productList.add(product);
                    }

                    adapter= new ProductAdapterImages (PrincipalScreen.this, productList);
                    recyclerViewImages.setAdapter(adapter);
                    frameImagesLoad.setVisibility(View.GONE);
                    recyclerViewImages.setVisibility(View.VISIBLE);

                    saveBitmapsFromImagesPublicitary();

                    if(pDialog != null)
                        pDialog.dismiss();
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    if(pDialog != null)
                        pDialog.dismiss();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e(TAG, error.getMessage());
                try {
                    if (pDialog != null)
                        pDialog.dismiss();
                }catch (Throwable e){
                    Log.e(TAG, "onErrorResponse: " + e.getMessage());
                }
            }
        });
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void saveBitmapsFromImagesPublicitary()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_IMAGENES_PUBLICIDAD, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String result)
            {
                try
                {
                    JSONObject object  = new JSONObject(result);
                    JSONArray resultImages= object.getJSONArray("result");

                    if(resultImages.length()>0)
                    {
                        publicidad.deleteAll(publicidad.class);
                        for(int i=0; i<=resultImages.length()-1; i++)
                        {
                            JSONObject jsonTmp = resultImages.getJSONObject(i);
                            Control.insertarPublicidadd(
                                    jsonTmp.getString("bitmap"),
                                    jsonTmp.getString("nombre")
                            );
                        }
                    }
                    else
                        Log.v(TAG, "JSON ResultImages no contiene datos");

                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Log.v(TAG, "Error en catch: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e(TAG, "Error en onErrorResponse()" + error.getMessage());
            }
        });
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }





    public boolean verificarInternetFromDownloads()
    {
        ConnectivityManager con = (ConnectivityManager) PrincipalScreen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    public boolean verificarInternet()
    {

        ConnectivityManager con = (ConnectivityManager) PrincipalScreen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
        {
            //layoutLoadPDF.setVisibility(View.VISIBLE);
            frameNoInternet.setVisibility(View.GONE);
            rvPDF.setVisibility(View.VISIBLE);
            return true;
        }
        else
        {
            layoutLoadPDF.setVisibility(View.GONE);
            frameNoInternet.setVisibility(View.VISIBLE);
            rvPDF.setVisibility(View.GONE);
            return false;

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        /*if(!verificarInternet())
        {
            layoutLoadPDF.setVisibility(View.GONE);
            frameNoInternet.setVisibility(View.VISIBLE);
            showPopup(recyclerView, "Lo sentimos no podemos sincronizar sin internet");
        }*/

        if (!downloadingApp)
            checkForUpdate();

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    /*public void verAfiliaciones(View view)
    {
        GridLayoutManager gridLayoutManager;
        final Button btAceptar;
        RecyclerView rvAf;
        TextView tvServ, tvDescripcionServ;
        myDialog.setContentView(R.layout.bottom_camera_open);
        myDialog.setCancelable(true);
        rvAf=(RecyclerView) myDialog.findViewById(R.id.rvAf);
        tvServ=(TextView) myDialog.findViewById(R.id.tvServ);
        tvDescripcionServ=(TextView) myDialog.findViewById(R.id.tvDescripcionServ);

        tvServ.setTypeface(ApplicationResourcesProvider.mRegular);
        rvAf.setHasFixedSize(true);
        gridLayoutManager= new GridLayoutManager(this, 1);
        rvAf.setLayoutManager(gridLayoutManager);

        String query = "SELECT * FROM SOLICITUDCLIENTE ORDER BY id DESC limit 5";
        List<Solicitud_Cliente> listaSolicitudes = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, query);
        adapterSol= new AdapterSolicitudes(getApplicationContext(), listaSolicitudes, PrincipalScreen.this);
        rvAf.setAdapter(adapterSol);


        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }*/



    private void checkForUpdate() {
        JSONObject data = new JSONObject();
        try {
            data.put("codigo", Control.getPromotorCodigo());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.checkUpdate, data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (!response.has("error")) {
                        try {
                            int checkUpdate = response.getInt("actualizar");

                            Log.d("UpdateCheck", "Debe actualizar: " + (checkUpdate == 1 ? "Si" : "No"));

                            if (checkUpdate == 1 && !updatePostponed)
                                checkVersion();
                            else if (checkUpdate == 2)
                                forceUpdate();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (timerUpdate != null)
                        timerUpdate.cancel();

                    timerUpdate = new Timer();

                    timerUpdate.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            updatePostponed = false;
                        }
                    }, (1000 * 60) * 60);

                    updatePostponed = true;

                    error.printStackTrace();
                }
            });

            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(request);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void forceUpdate() {
        StringRequest request = new StringRequest(Request.Method.GET, com.example.sociales.Extras.URL.checkVersion, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                downloadingApp = true;
                pDialog = new ProgressDialog(PrincipalScreen.this);
                pDialog.setMessage("Descargando, espera porfavor...");
                pDialog.setCancelable(false);
                pDialog.show();

                String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                String fileName = "asistencia_social.apk";
                final Uri uri = Uri.parse("file://" + destination + fileName);

                //showMyCustomDialog();

                File file = new File(destination, fileName);

                if (file.exists())
                    file.delete();

                DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(com.example.sociales.Extras.URL.updateApp + response + ".apk"));
                //downloadRequest.setDescription("Descargando PABS");
                downloadRequest.setTitle("Actualización PABS");

                downloadRequest.setDestinationUri(uri);
                downloadRequest.allowScanningByMediaScanner();
                downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                if (manager != null) {
                    final long downloadId = manager.enqueue(downloadRequest);

                    BroadcastReceiver onComplete = new BroadcastReceiver() {

                        File file;

                        @Override
                        public void onReceive(Context context, Intent intent) {

                            if (file.exists()) {
                                Uri apkUri = Uri.fromFile(file);
                                Intent update = new Intent();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    apkUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                                    update.setAction(Intent.ACTION_INSTALL_PACKAGE);
                                    update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                    update.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    startActivity(update);
                                } else {
                                    update.setAction(Intent.ACTION_VIEW);
                                    update.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                    startActivity(update);
                                }

                                Log.i("ApkUpdate", manager.getMimeTypeForDownloadedFile(downloadId));

                                downloadingApp = false;
                                if(pDialog != null)
                                    pDialog.dismiss();
                                unregisterReceiver(this);
                                //finish();
                            } else {
                                //dismissMyCustomDialog();
                                downloadingApp = false;
                                if(pDialog != null)
                                    pDialog.dismiss();
                                timerUpdate = new Timer();

                                manager.remove(downloadId);

                                timerUpdate.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        updatePostponed = false;
                                    }
                                }, (1000 * 60) * 60);
                                updatePostponed = true;

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getContext(), "No se encontro el archivo de actualización", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }

                        public BroadcastReceiver setData(File file) {
                            this.file = file;
                            return this;
                        }
                    }.setData(file);

                    registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(request);
    }

    private void checkVersion() {
        StringRequest request = new StringRequest(Request.Method.GET, com.example.sociales.Extras.URL.checkVersion, new Response.Listener<String>()
        {
            @Override
            public void onResponse(final String response)
            {
                try {
                    PackageInfo info = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
                    String version = info.versionName;

                    int comparison = version.compareTo(response);

                    Log.d("UpdateComparison", "Comparison: " + comparison);

                    if (!version.equals(response) && comparison < 0)
                    {

                        AlertDialog alertDialog = new AlertDialog.Builder(PrincipalScreen.this).create();
                        alertDialog.setTitle("Actualización");
                        alertDialog.setMessage("Hay una nueva versión de la aplicación lista para descargar e instalar. \n\nVersión actual: " + version + "\n\nVersión nueva: " + response);
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Actualizar", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                                String fileName = "asistencia.apk";
                                final Uri uri = Uri.parse("file://" + destination + fileName);

                                try{
                                    if(pDialog.isShowing())
                                        pDialog.dismiss();

                                    pDialog = null;
                                }catch (Throwable e){
                                    Log.e(TAG, "onClick: " + e.toString());
                                }
                                pDialog = new ProgressDialog(PrincipalScreen.this);
                                pDialog.setMessage("Descargando, espera porfavor...");
                                pDialog.setCancelable(false);
                                pDialog.show();


                                //LogRegister.registrarActualizacion("Aceptada");

                                final File file = new File(destination, fileName);

                                if (file.exists())
                                    file.delete();


                                final DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(com.example.sociales.Extras.URL.urlUpdateApp + response + ".apk"));
                                //downloadRequest.setDescription("Descargando PABS");
                                downloadRequest.setTitle("Descargando...");

                                downloadRequest.setDestinationUri(uri);
                                downloadRequest.allowScanningByMediaScanner();
                                downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                if (manager != null) {
                                    final long downloadId = manager.enqueue(downloadRequest);

                                    BroadcastReceiver onComplete = new BroadcastReceiver()
                                    {
                                        @Override
                                        public void onReceive(Context context, Intent intent) {
                                            if (file.exists()) {
                                                Uri apkUri = Uri.fromFile(file);
                                                Intent update = new Intent();
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    apkUri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                                                    update.setAction(Intent.ACTION_INSTALL_PACKAGE);
                                                    update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                                    update.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                                    startActivity(update);
                                                } else {
                                                    update.setAction(Intent.ACTION_VIEW);
                                                    update.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                                    startActivity(update);
                                                }

                                                Log.i("ApkUpdate", manager.getMimeTypeForDownloadedFile(downloadId));

                                                unregisterReceiver(this);
                                                finish();
                                            } else {
                                                //dismissMyCustomDialog();
                                                timerUpdate = new Timer();

                                                manager.remove(downloadId);

                                                timerUpdate.schedule(new TimerTask() {
                                                    @Override
                                                    public void run() {
                                                        updatePostponed = false;
                                                    }
                                                }, (1000 * 60) * 60);
                                                updatePostponed = true;

                                                Toast.makeText(getApplicationContext(), "No se encontro el archivo", Toast.LENGTH_SHORT).show();
                                                //mainActivity.toastL("No se encontro el archivo de actualización");
                                            }
                                        }
                                    };
                                    if(pDialog != null)
                                        pDialog.dismiss();

                                    registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                                }
                            }
                        });

                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Posponer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                timerUpdate = new Timer();

                                timerUpdate.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        updatePostponed = false;
                                        Log.i("UpdatePostponed", "false");
                                    }
                                }, (1000 * 60) * 60);

                                updatePostponed = true;

                                Log.i("UpdatePostponed", "true");
                                //LogRegister.registrarActualizacion("Pospuesta");
                            }
                        });
                        alertDialog.show();

                    }
                } catch (PackageManager.NameNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }
        );

        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(request);
    }


    private void sincronizarTodo()
    {
        if(verificarInternet())
        {
            loadImages();
            sincronizarFotos=false;
            loadRequestForLocalidadesAndColonia(codigoPromotor);
            String querX = "SELECT * FROM SOLICITUDCLIENTE WHERE sync = 0";
            List<Solicitud_Cliente> lista = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, querX);
            if (lista.size() > 0)
            {
                creacionDeJsonParaRealizarLaCargaDeSolicitudes();
                Log.d(TAG, "Comienza creacion de JSON en sincronizarTodo()");
            }
            else
                Log.d(TAG, "No hay datos por sincronizar en sincronizarTodo()");

            parametrosForPDFNominas(codigoPromotor);

            if(rvPDF.getAdapter() != null)
            {
                if(rvPDF.getAdapter().getItemCount() == 0)
                    layoutLoadPDF.setVisibility(View.VISIBLE);
                else
                    layoutLoadPDF.setVisibility(View.GONE);
            }



            //*********** sincronizacion de fotos y solicitudes *********
            createJsonForPhotos();
            creacionDeJsonParaRealizarLaCargaDeNuevasSolicitudes();

            Toast.makeText(this, "Sincronización finalizada", Toast.LENGTH_SHORT).show();
        }
        else {
            layoutLoadPDF.setVisibility(View.GONE);
            frameNoInternet.setVisibility(View.VISIBLE);
            showPopup(recyclerView, "Lo sentimos no podemos sincronizar sin internet");
        }


        ExportImportDB exportImportDB = new ExportImportDB();
        try {
            if (exportImportDB.backupDBHidden()) {
                Log.i(TAG, "EXPORTADA CORRECTAMENTE");
            } else {
                Log.i(TAG, "ERROR AL EXPORTAR");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Loads user name of collector.
     * This is focused to Splunk Mint, in order to report error by user name
     *
     * @return collector's user name
     */
    private String loadUser() {
        return codigoPromotor;
    }

    //************************************************************* FIN HOME *********************************************************************

    @SuppressLint("LongLogTag")
    public void createJsonForPhotos() {

        JSONObject jsonUbicacionesGeneral = new JSONObject();

        String sintaxis = "SELECT * FROM PROMOTOR";
        List<Promotor> listaPromotor = Promotor.findWithQuery(Promotor.class, sintaxis);
        if (listaPromotor.size() > 0) {
            String codigoPromotor = listaPromotor.get(0).getPromCodigo();
            try {
                jsonUbicacionesGeneral.put("codigo", codigoPromotor);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else {
            Log.d("ERROR PROMOTOR-->", "NO hay registros");
        }

        String queryArchivos = "SELECT * FROM ARCHIVO WHERE sync ='0'";
        List<Archivo> listadoArchivos = Archivo.findWithQuery(Archivo.class, queryArchivos);
        if (listadoArchivos.size() > 0) {
            JSONArray arrayFotos = new JSONArray();
            for (int y = 0; y <= listadoArchivos.size() - 1; y++) {
                JSONObject jsonArchivos = new JSONObject();
                try {
                    jsonArchivos.put("nombre", listadoArchivos.get(y).getArchivoNombre());
                    jsonArchivos.put("foto", listadoArchivos.get(y).getBitmapArchivo());
                    jsonArchivos.put("latitud", listadoArchivos.get(y).getLatitud());
                    jsonArchivos.put("longitud", listadoArchivos.get(y).getLongitud());
                    jsonArchivos.put("fecha_captura", listadoArchivos.get(y).getFecha());
                    jsonArchivos.put("codigo_activacion", listadoArchivos.get(y).getArchivoSolicitudClienteCodigoActivacion());
                    arrayFotos.put(jsonArchivos);
                } catch (JSONException ex) {
                    Log.d("Error -->", ex.toString());
                }
            }
            try {
                jsonUbicacionesGeneral.put("archivos", arrayFotos);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            requestDeImagenesParaServer(jsonUbicacionesGeneral);
        }else
            Log.d(TAG, "createJsonForPhotos: No hay archivos por sincronizar");
    }

    public void requestDeImagenesParaServer(JSONObject jsonParams) {

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_LOAD_IMAGES_TO_SERVER, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (!response.has("error")) {
                                JSONArray jsonSuccess = response.getJSONArray("success");
                                JSONArray jsonFail = response.getJSONArray("fail");

                                if (jsonSuccess.length() > 0) {
                                    for (int i = 0; i <= jsonSuccess.length() - 1; i++) {
                                        JSONObject object = jsonSuccess.getJSONObject(i);

                                        String queryArchivos = "UPDATE ARCHIVO SET sync ='2' WHERE nombre='" + object.getString("nombre") + "'";
                                        Archivo.executeQuery(queryArchivos);

                                        Log.d(TAG, "onResponse: Response Imagenes: " + response);
                                    }
                                }
                                if (jsonFail.length() > 0) {
                                    for (int y = 0; y <= jsonFail.length() - 1; y++) {
                                        JSONObject object = jsonFail.getJSONObject(y);
                                        Log.d(TAG, "onResponse: FAIL: " + object.getString("RESULTADO"));
                                    }

                                }

                            } else
                                Log.d("Error:-->", response.getString("error"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: Error onResponse: " + e.getMessage());
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };

        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }



    public void creacionDeJsonParaRealizarLaCargaDeNuevasSolicitudes() {
        JSONArray arrayNuevasSolicitudes = new JSONArray();
        JSONObject objetoGeneralJson = new JSONObject();

        String codigoPromotorFromPreferences = "";
        try {
            if (!Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN).isEmpty() || Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN) != null)
                codigoPromotorFromPreferences = Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN);

            objetoGeneralJson.put("codigo", codigoPromotorFromPreferences);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        List<SolicitudesNuevas> solicitudesNuevasList = SolicitudesNuevas.findWithQuery(SolicitudesNuevas.class, "SELECT * FROM SOLICITUDES_NUEVAS where sync = '0'");
        if (solicitudesNuevasList.size() > 0) {

            for (int y = 0; y <= solicitudesNuevasList.size() - 1; y++) {
                JSONObject jsonDatos = new JSONObject();
                try {
                    jsonDatos.put("nombre_foto", solicitudesNuevasList.get(y).getNombresolicitud());
                    jsonDatos.put("codigoBarras", solicitudesNuevasList.get(y).getCodigobarras());
                    jsonDatos.put("fecha", solicitudesNuevasList.get(y).getFecha());
                    jsonDatos.put("latitud", solicitudesNuevasList.get(y).getLatitud());
                    jsonDatos.put("longitud", solicitudesNuevasList.get(y).getLongitud());
                    jsonDatos.put("codigoActivacion", solicitudesNuevasList.get(y).getCodigoactivacion());
                    jsonDatos.put("inversionInicial", solicitudesNuevasList.get(y).getInversion());
                    arrayNuevasSolicitudes.put(jsonDatos);
                } catch (JSONException ex) {
                    Log.d("LOCATION", ex.getMessage());
                }
            }
            try {
                objetoGeneralJson.put("solicitudes", arrayNuevasSolicitudes);

                requestSyncNewSolicitudesToWebServer(objetoGeneralJson);
            } catch (JSONException ex) {
                Log.d("LOCATION", ex.getMessage());
            }
        }

    }
    public void requestSyncNewSolicitudesToWebServer(JSONObject jsonParams) {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, com.example.sociales.Extras.URL.URL_LOAD_NEW_SOLICITUDES_TO_WEB, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (!response.has("error"))
                    {
                        JSONArray jsonSuccess = response.getJSONArray("success");
                        JSONArray jsonFail = response.getJSONArray("fail");


                        if(jsonSuccess.length()>0)
                        {
                            for(int i=0; i<=jsonSuccess.length()-1; i++) {
                                JSONObject object = jsonSuccess.getJSONObject(i);
                                String query="UPDATE SOLICITUDES_NUEVAS SET sync=2 WHERE fecha='" + object.getString("fecha") +"'";
                                SolicitudesNuevas.executeQuery(query);

                            }
                        }
                        else
                            Log.d("RESPUESTA", "JSON SUCCESS no contiene datos");

                        if (jsonFail.length()>0)
                        {

                            for(int y=0; y<jsonFail.length()-1;y++)
                            {
                                JSONObject object = jsonFail.getJSONObject(y);
                                String result = object.getString("RESULTADO");
                                Log.d("FAIL RESULT-->", result);
                            }
                        }
                        else
                            Log.d("RESPUESTA", "JSON FAIL no contiene datos");

                        if(response.has("query"))
                        {
                            try {
                                String queryJSON = response.getString("query");
                                Log.e("ServerQuery", "from ApplicationResourcesProvider " + queryJSON);
                                if (!queryJSON.equals("") && !queryJSON.equals("null")) {
                                    SugarRecord.executeQuery(queryJSON);
                                    response.remove("query");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        else
                        {
                            Log.d("RESPUESTA", "JSON Query no contiene datos");
                        }
                    }
                    else
                    {
                        String error = response.getString("error");
                        Log.d("WS", "Error: ApplicationResourcesProvider:   " + error.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }



}
