package com.example.sociales.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.animation.Animator;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.Resource;
import com.example.sociales.BuildConfig;
import com.example.sociales.DatabaseLocal.Control;

import com.example.sociales.DatabaseLocal.ExportImportDB;
import com.example.sociales.DatabaseLocal.Promotor;
import com.example.sociales.DatabaseLocal.Promotor_Status;
import com.example.sociales.DatabaseLocal.Solicitud;
import com.example.sociales.DatabaseLocal.Solicitud_Cliente;
import com.example.sociales.DatabaseLocal.Ubicacion;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.BaseActivity;
import com.example.sociales.Extras.CircularReveal;
import com.example.sociales.Extras.Preferences;
import com.example.sociales.Extras.URL;
import com.example.sociales.Extras.VolleySingleton;
import com.example.sociales.R;
import com.example.sociales.Service.Constants;
import com.example.sociales.Service.ForegroundService;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.gms.vision.Frame;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.karan.churi.PermissionManager.PermissionManager;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import pl.tajchert.nammu.PermissionListener;

public class MainActivity extends AppCompatActivity
{
    public static final  String TAG ="MAIN_ACTIVITY";
    private PermissionManager permissionManager;
    final PermissionCallback permissionCallback = new PermissionCallback()
    {
        @Override
        public void permissionGranted() {
            if (Nammu.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                getDataAndFoliosNotSavedToServerAfterErasingAppData();
        }

        @Override
        public void permissionRefused() {
        }
    };

    public ProgressBar progressBar;
    private ConstraintLayout layout;
    private TextView tvVersion, tvInforma, tvNoche, tvHola;
    private TextInputEditText etUsuario, etContrasena;
    private TextInputLayout TV1, TV2;
    private Button btIniciarSesion;
    private Task task;
    private FrameLayout cardAnim;
    public boolean habilitarBotonDeLogin = true;
    private CircularReveal circularReveal;
    private String android_id = "", deviceName = "", versionApp = "";
    Dialog myDialog;
    boolean indicadorSolicitudServer=false;

    public String Permisos = "", query="",Estatus = "",Efectividad = "",inversionInicial="", nombre="",
            Actualizar_app = "",codigo_activacion = "",solicitud_serie = "", cliente_id = "",
            nuevo_ingreso = "",esquema_pago_id = "",fecha_solicitud = "",C_Efectividad = "",
            C_DiasSinAfiliaciones = "",C_MinutosInactividad = "",fechaActual = "",tipoSolicitud="", letraIndice="", tipoPromotor="";

    public boolean isGPSEnabled = false;
    public Date date;
    URL objeto;
    int indicador=0;
    //--------------------------------------------------

    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        permissionManager = new PermissionManager(){};
        permissionManager.checkAndRequestPermissions(this);
        cardAnim=(FrameLayout) findViewById(R.id.cardAnim);
        etUsuario = (TextInputEditText) findViewById(R.id.etUsuario);

        etContrasena = (TextInputEditText) findViewById(R.id.etContrasena);
        btIniciarSesion = (Button) findViewById(R.id.btInicarSesion);
        layout = (ConstraintLayout) findViewById(R.id.layout);
        tvVersion = (TextView) findViewById(R.id.tvVersion);
        TV1 = (TextInputLayout) findViewById(R.id.T1);
        TV2 = (TextInputLayout) findViewById(R.id.T2);
        tvInforma = (TextView) findViewById(R.id.tvInforma);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvHola=(TextView)findViewById(R.id.tvHola);
        tvNoche=(TextView)findViewById(R.id.tvNoche);
        task = new Task(layout);
        android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        deviceName = android.os.Build.MODEL;
        myDialog = new Dialog(this);
        versionApp = BuildConfig.VERSION_NAME;
        objeto = new URL();
        Nammu.init(getApplicationContext());
        verificarInternet();
        tvVersion.setText("Versión de aplicación: "+ versionApp);

        //tvVersion.setTypeface(ApplicationResourcesProvider.mRegular);
        //tvInforma.setTypeface(ApplicationResourcesProvider.mRegular);
        //tvNoche.setTypeface(ApplicationResourcesProvider.mRegular);
        //tvHola.setTypeface(ApplicationResourcesProvider.mRegular);


        verificarGPS();
        cerrarTeclado();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        date = new Date();
        fechaActual = dateFormat.format(date);

        if(!Preferences.getPreferenceUsuarioLogin(MainActivity.this, Preferences.PREFERENCE_USUARIO_LOGIN).isEmpty())
            etUsuario.setText(Preferences.getPreferenceUsuarioLogin(MainActivity.this, Preferences.PREFERENCE_USUARIO_LOGIN));

        Mint.setUserIdentifier(etUsuario.getText().toString());

        btIniciarSesion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View view)
            {
                if(etUsuario.getText().toString().equals("resetDB%"))
                {
                    Control.resetDatabase(ApplicationResourcesProvider.getContext());
                    Toast.makeText(MainActivity.this, "La base de datos fue reseteada con éxito", Toast.LENGTH_SHORT).show();
                }

                if(Preferences.getLockApp(MainActivity.this, Preferences.PREFERENCE_LOCK_APP) && !checkInternetConnection()) { //Si tiene bloqueo de aplicación, que no deje continuar (cuando este offline)
                    showPopup(etUsuario, "Nombre de usuario inactivo, acceso denegado.");
                }
                else
                {
                    if(habilitarBotonDeLogin)
                    {
                        habilitarBotonDeLogin=false;
                        permissionManager.checkAndRequestPermissions(MainActivity.this);
                        if ( Control.getCurrentPromotorCode() == "" || etUsuario.getText().toString().equalsIgnoreCase( Control.getCurrentPromotorCode()))
                        {
                            if (verificarInternet())
                            {
                                verificarGPS();
                                if (isGPSEnabled) {
                                    if( checkFullFields( view ))
                                    {
                                        parametrosJSON(etUsuario.getText().toString(), etContrasena.getText().toString(), android_id, deviceName, versionApp);
                                        habilitarBotonDeLogin = false;
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Enciende tu GPS para continuar", Toast.LENGTH_SHORT).show();
                                    habilitarBotonDeLogin = true;
                                }
                                indicador = 1;
                            }  else {
                                Toast.makeText(getApplicationContext(), "Iniciando modo offline", Toast.LENGTH_SHORT).show();
                                verificarGPS();
                                if (isGPSEnabled) {
                                    if( checkFullFields( view ))
                                    {
                                        validarLoginOffline();
                                        habilitarBotonDeLogin = false;

                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Enciende tu GPS para continuar", Toast.LENGTH_SHORT).show();
                                    habilitarBotonDeLogin = true;
                                }
                                indicador = 1;
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.error_message_wrong_promotor_code, Toast.LENGTH_SHORT).show();
                            habilitarBotonDeLogin = true;
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Espera...", Toast.LENGTH_SHORT).show();
                    }
                }




            }
        });

        //btIniciarSesion.performClick();


    }

    public boolean checkFullFields( View view ){
        if (etUsuario.getText().toString().trim().isEmpty() || etUsuario.getText().toString().trim().equals("")) {
            Snackbar.make(view, "Ingresa un usuario", Snackbar.LENGTH_LONG).show();
            habilitarBotonDeLogin= true;
            return false;
        } else if (etContrasena.getText().toString().trim().isEmpty() || etContrasena.getText().toString().trim().equals("")) {
            Snackbar.make(view, "Ingresa una contraseña", Snackbar.LENGTH_LONG).show();
            habilitarBotonDeLogin= true;
            return false;
        } else {
            return true;
        }
    }


    public void verificarGPS()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Activa el GPS para continuar", Toast.LENGTH_SHORT).show();
        } else {
            final LocationManager locationManager = (LocationManager) MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        }
    }

    public boolean verificarInternet()
    {
        ConnectivityManager con = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
        {
            tvNoche.setVisibility(View.GONE);
            layout.setBackgroundColor(Color.WHITE);
            etUsuario.setTextColor(Color.DKGRAY);
            etContrasena.setTextColor(Color.DKGRAY);
            //cardAnim.setAnimation(ApplicationResourcesProvider.rapida);
            cardAnim.setVisibility(View.GONE);
            return true;
        }
        else
            {
                cardAnim.setVisibility(View.VISIBLE);
                //cardAnim.setAnimation(ApplicationResourcesProvider.rapida);
                tvNoche.setVisibility(View.VISIBLE);
                tvNoche.setTextColor(Color.WHITE);
                layout.setBackgroundColor(Color.DKGRAY);
                tvVersion.setVisibility(View.GONE);
                etUsuario.setTextColor(Color.WHITE);
                etContrasena.setTextColor(Color.WHITE);
                TV1.setBoxStrokeColor(Color.WHITE);
                TV2.setBoxStrokeColor(Color.WHITE);
                return false;
            }
    }

    public boolean checkInternetConnection()
    {
        ConnectivityManager con = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @SuppressLint("ResourceAsColor")
    private CircularReveal getInstance(final View rootView) {
        if (circularReveal != null) {
            return circularReveal;
        }

        View centerView = (ProgressBar) findViewById(R.id.progressBar);
        View reveal = (View) findViewById(R.id.circularReveal);

        if (centerView == null) {
            return null;
        }

        int centerY = reveal.getHeight();
        int centerX = reveal.getWidth() / 2;

        circularReveal = new CircularReveal(reveal, centerX, centerY);
        circularReveal.setExpandDur(400); //tiempo de animacion que cubre la activity
        circularReveal.setBackgroundColor(R.color.colorAccent);
        layout.setBackgroundColor(R.color.colorVerdeClaro);

        circularReveal.setCircularRevealListener(new CircularReveal.CircularRevealListener()
        {
            //@SuppressLint("ResourceAsColor")
            @Override
            public void onAnimationEnd(int animState)
            {
                tvInforma.setVisibility(View.GONE);
                Promotor.deleteAll(Promotor.class);
                Control.insertarPromotor(etUsuario.getText().toString(),
                        "NombrePromotor",
                        Efectividad,
                        android_id,
                        etContrasena.getText().toString(),
                        Permisos,
                        Estatus,
                        Actualizar_app,
                        tipoPromotor
                );
                Control.insertarPromotor_Status(etUsuario.getText().toString(), android_id);

                String querX = "SELECT * FROM SOLICITUDCLIENTE WHERE fechasolicitud >= datetime('now', '-15 day')";
                List<Solicitud_Cliente> lista = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, querX);
                if (lista.size() > 0)
                {
                    Solicitud.deleteAll(Solicitud.class);
                    Control.insertarSolicitud(solicitud_serie, etUsuario.getText().toString(), tipoSolicitud, letraIndice, "1");

                        if (indicadorSolicitudServer)
                        {
                            String sintax="SELECT * FROM SOLICITUDCLIENTE WHERE solicitudserie='"+ solicitud_serie + "'";
                            List<Solicitud_Cliente> encontrado = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, sintax);
                            if (encontrado.size() > 0)
                            {
                                String aux="UPDATE SOLICITUDCLIENTE SET sync =2 WHERE solicitudserie='" + solicitud_serie +"'";
                                Solicitud_Cliente.executeQuery(aux);
                            }
                            else
                                Control.insertarSolicitudCliente(codigo_activacion, solicitud_serie, cliente_id, nuevo_ingreso, esquema_pago_id, fecha_solicitud, inversionInicial,2);
                        }
                        else
                        {
                            Log.i(TAG, "Indicador de solicitud server: " + indicadorSolicitudServer);
                        }
                }
                    Intent intent = new Intent(MainActivity.this, PrincipalScreen.class);
                    intent.putExtra("codigo", etUsuario.getText().toString());
                    intent.putExtra("nombre", nombre);
                    intent.putExtra("query", query);
                    startActivity(intent);
                    finish();
            }
        });

        return circularReveal;
    }

    private class Task extends AsyncTask<Void, Void, Void> {
        private View rootView;
        private View progressBar;
        private View btnLogin;

        public Task(View rootView) {
            this.rootView = rootView;
        }

        @Override
        protected void onPreExecute() {
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
            btnLogin = (Button) findViewById(R.id.btInicarSesion);
            fadeAnimation(btnLogin, true);
            fadeAnimation(TV2, true);
            fadeAnimation(TV1, true);
            fadeAnimation(tvVersion, true);
            fadeAnimation(tvHola, true);
            fadeAnimation(cardAnim, true);
            tvVersion.setVisibility(View.GONE);
            tvHola.setVisibility(View.GONE);
            btnLogin.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);
            CircularReveal circular = getInstance(rootView);

            if (circular != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circular.expand();
                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        Nammu.permissionCompare(new PermissionListener() {
            @Override
            public void permissionsChanged(String s) {

            }

            @Override
            public void permissionsGranted(String s) {
                Log.d("Location permission", s);
            }

            @Override
            public void permissionsRemoved(String s) {
                Nammu.askForPermission(MainActivity.this, s, permissionCallback);
            }
        });

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Nammu.hasPermission(this, permissions)) {
            //ApplicationResourcesProvider.startLocationUpdates();
            getDataAndFoliosNotSavedToServerAfterErasingAppData();
        } else {
            Nammu.askForPermission(this, permissions, permissionCallback);
        }
    }


    private void fadeAnimation(final View v, boolean isFadeOut) //animacion de ocultamiento de boton
    {
        ObjectAnimator fadeOut = isFadeOut ? ObjectAnimator.ofFloat(v, "alpha", 1f, 0f) : ObjectAnimator.ofFloat(v, "alpha", 0f, 1f);
        fadeOut.setDuration(400);
        fadeOut.start();
    }

    public void  parametrosJSON(String codigo, String password, String androidID, String androidModel, String versionApp) {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("codigo", codigo);
            jsonParams.put("password", password);
            jsonParams.put("androidID", androidID);
            jsonParams.put("androidModel", androidModel);
            jsonParams.put("appVersion", versionApp);
            verificarLoginOnline(jsonParams); //manda llamar el metodo del request
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verificarLoginOnline(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_LOGIN_PROBENSO, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.has("error")) {
                                JSONObject json = response.getJSONObject("result");
                                Permisos = json.getString("Permisos");
                                nombre = json.getString("Nombre");
                                query = json.getString("query");
                                Estatus = json.getString("Estatus");
                                Efectividad = json.getString("Efectividad");
                                tipoPromotor = json.getString("tipo_promotor");
                                Actualizar_app = json.getString("Actualizar_app");

                                try
                                {
                                    JSONObject jsonAfiliacion = json.getJSONObject("Ultima_afiliacion");
                                    if(jsonAfiliacion.length()>0) {
                                        codigo_activacion = jsonAfiliacion.getString("codigo_activacion");
                                        solicitud_serie = jsonAfiliacion.getString("solicitud_serie");
                                        cliente_id = jsonAfiliacion.getString("cliente_id");
                                        nuevo_ingreso = jsonAfiliacion.getString("nuevo_ingreso");
                                        esquema_pago_id = jsonAfiliacion.getString("esquema_pago_id");
                                        fecha_solicitud = jsonAfiliacion.getString("fecha_solicitud");
                                        tipoSolicitud = jsonAfiliacion.getString("tipo_solicitud");
                                        letraIndice = jsonAfiliacion.getString("letra_indice");
                                        inversionInicial = jsonAfiliacion.getString("inversion_inicial");
                                        indicadorSolicitudServer = true;
                                    }
                                }
                                catch (JSONException ex)
                                {
                                    indicadorSolicitudServer=false;
                                    ex.printStackTrace();
                                }

                                JSONObject jsonConfiguracion = json.getJSONObject("Configuracion");
                                C_Efectividad = jsonConfiguracion.getString("C_Efectividad");
                                C_DiasSinAfiliaciones = jsonConfiguracion.getString("C_DiasSinAfiliaciones");
                                C_MinutosInactividad = jsonConfiguracion.getString("C_MinutosInactividad");


                                int permisos = Integer.parseInt(Permisos);
                                int status = Integer.parseInt(Estatus);

                                if (permisos == 1 && status == 1) {
                                    tvInforma.setVisibility(View.VISIBLE);
                                    if(task != null){
                                        task.execute();
                                        Preferences.savePreferenceUsuarioLogin(MainActivity.this, jsonParams.getString("codigo"), Preferences.PREFERENCE_USUARIO_LOGIN);
                                        habilitarBotonDeLogin=false;
                                        Preferences.saveLockApp(MainActivity.this, false, Preferences.PREFERENCE_LOCK_APP);
                                    }

                                } else if (permisos != 1 || status != 1) {
                                    String error = "Acceso bloqueado o usuario inactivo";
                                    showPopup(etUsuario, error);
                                }
                            } else {
                                String error = response.getString("error");
                                showPopup(etUsuario, error);
                                habilitarBotonDeLogin = true;
                                Preferences.saveLockApp(MainActivity.this, true, Preferences.PREFERENCE_LOCK_APP);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Ocurrio un error al iniciar sesión, por favor vuelve a intentarlo", Toast.LENGTH_LONG).show();
                        habilitarBotonDeLogin=true;
                    }
                }) {

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
    }

    public void showPopup(View view, final String codeError)
    {
        final Button btNew;
        TextView tvCodeError;
        myDialog.setContentView(R.layout.login_incorrecto);
        myDialog.setCancelable(false);
        btNew = (Button) myDialog.findViewById(R.id.btNew);
        tvCodeError = (TextView) myDialog.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);
        if(codeError.equals("Lo sentimos; Tienes mas de 15 días sin afiliaciones, no es posible acceder al sistema"))
            btNew.setText("Salir del sistema");

        btNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(btNew.getText().toString().equals("Salir del sistema"))
                {
                    finishAffinity();
                }
                else
                {
                    etContrasena.setText("");
                    btIniciarSesion.setEnabled(true);
                    btIniciarSesion.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    habilitarBotonDeLogin=true;
                    myDialog.dismiss();
                }
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }

    /*public void validarFechas(Date fecha, int dias)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, - dias);
        objeto.fecha15DiasAtras = calendar.getTime();
        //Toast.makeText(this, dateFormat.format(calendar.getTime()), Toast.LENGTH_SHORT).show(); //Fecha actual menos 15 dias ----> calendar.getTime()
    }*/

    public void validarLoginOffline()
    {
        permissionManager.checkAndRequestPermissions(MainActivity.this);
        String codigoPromotorOffline ="", contrasenaPromotorOffline="";
        String query = "SELECT * FROM PROMOTOR";
        List<Promotor> promotor = Promotor.findWithQuery(Promotor.class, query);

        for (int i = 0; i < promotor.size(); i++)
        {
            codigoPromotorOffline = promotor.get(i).getPromCodigo();
            contrasenaPromotorOffline = promotor.get(i).getProm_Contraseña();
        }


        if (etUsuario.getText().toString().trim().isEmpty() || etUsuario.getText().toString().trim().equals("")) {
            Snackbar.make(etUsuario, "Ingresa un usuario", Snackbar.LENGTH_LONG).show();
            habilitarBotonDeLogin = true;
        } else if (etContrasena.getText().toString().trim().isEmpty() || etContrasena.getText().toString().trim().equals("")) {
            Snackbar.make(etUsuario, "Ingresa una contraseña", Snackbar.LENGTH_LONG).show();
            habilitarBotonDeLogin= true;
        } else
        {
            if (etUsuario.getText().toString().equals(codigoPromotorOffline) && etContrasena.getText().toString().equals(contrasenaPromotorOffline))
            {
                habilitarBotonDeLogin= false;
                Intent intent = new Intent(MainActivity.this, PrincipalScreen.class);
                startActivity(intent);
                finish();
            }
            else if (etUsuario.getText().toString().equals(codigoPromotorOffline) && !etContrasena.getText().toString().equals(contrasenaPromotorOffline))
            {
                String errorCode= "Contraseña incorrecta verifica nuevamente";
                showPopup(etUsuario, errorCode);
                habilitarBotonDeLogin= true;
            }
            else if (!etUsuario.getText().toString().equals(codigoPromotorOffline) && etContrasena.getText().toString().equals(contrasenaPromotorOffline))
            {
                String errorCode= "Código de usuario incorrecto verifica nuevamente";
                showPopup(etUsuario, errorCode);
                habilitarBotonDeLogin= true;
            }
            else
            {
                String errorCode= "Credenciales incorrectas, no podemos inicar sesión";
                showPopup(etUsuario, errorCode);
                habilitarBotonDeLogin= true;
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //stopService();
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(serviceIntent);
    }

    public void startService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STARTFOREGROUND_ACTION);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finishAffinity();
    }

    private void cerrarTeclado()
    {
        View view  = this.getCurrentFocus();
        if(view!=null)
        {
            InputMethodManager imm =(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    /*private void animar(boolean mostrar) {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar) {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        } else {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);
        layout.setLayoutAnimation(controller);
    }*/


    private void getDataAndFoliosNotSavedToServerAfterErasingAppData()
    {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean IS_DATA_CLEARED =  appSharedPrefs.getBoolean("IS_DATA_CLEARED",true);

        if ( IS_DATA_CLEARED )
        {
            Log.v(TAG, "resultado: " + IS_DATA_CLEARED);
            ExportImportDB exportImportDB = new ExportImportDB();
            boolean successImporting = false;
            try {
                successImporting = exportImportDB.backupDBHidden();
                if ( successImporting ) {
                    Log.v(TAG, "Base de datos importada correctamente: " + successImporting);
                } else {
                    Log.v(TAG, "Hubo un error al importar la base de datos."+ successImporting);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (successImporting)
            {
                try
                {
                    Log.v(TAG,  "Proceso correcto");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Log.v(TAG,  "No se exporto la base de datos");
            }

        } else {
            Log.v(TAG,  "Datos de aplicación sin borrar: " + IS_DATA_CLEARED);
        }

        boolean IS_DATA_CLEARED2 =  appSharedPrefs.getBoolean("IS_DATA_CLEARED",true);
        if ( IS_DATA_CLEARED2 )
        {
            Log.v(TAG,  "if(IS_DATA_CLEARED)");
            SharedPreferences.Editor editor = appSharedPrefs.edit();
            editor.putBoolean("IS_DATA_CLEARED",false);
            editor.apply();
        }
        else {
            Log.v(TAG,  "Datos de aplicación sin borrar: " + IS_DATA_CLEARED);
        }

    }


}


