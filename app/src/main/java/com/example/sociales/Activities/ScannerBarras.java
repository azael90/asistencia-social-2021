package com.example.sociales.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.sociales.DatabaseLocal.FotografiasPorSolicitudDeCliente;
import com.example.sociales.DatabaseLocal.Solicitud;
import com.example.sociales.Extras.BaseActivity;
import com.example.sociales.R;
import com.example.sociales.Service.Constants;
import com.example.sociales.Service.ForegroundService;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.Result;

import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerBarras extends BaseActivity implements ZXingScannerView.ResultHandler {
    static String TAG = "SCANNER_BARRAS";
    private ZXingScannerView mScannerView;
    public String mensaje="";
    ProgressDialog pDialog;
    String codigoPromotor="";
    static final int PICK_CONTACT_REQUEST = 1;
    private boolean consultarSolicitudUsada= false;

    private AlertDialog.Builder dialogo;
    private boolean capturaRealizada= false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_scanner_barras);
        mScannerView = new ZXingScannerView(ScannerBarras.this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(ScannerBarras.this);
        mScannerView.startCamera();




        final Bundle extras = getIntent().getExtras();
        if(extras!=null)
        {
            if(extras.containsKey("preguntarPorLasUsadas")) //Esto viene desde  CapturaSolicitud
                consultarSolicitudUsada = extras.getBoolean("preguntarPorLasUsadas");
            else
                consultarSolicitudUsada = false;

            Log.v("CAPTURA", "preguntarPorUsadas: " + consultarSolicitudUsada + " ------------ > SCANNERBARRAS");
            codigoPromotor= extras.getString("codigoPromotor");
        }
        else
            Log.v(TAG, "No se recuperaron extras");

    }

    @Override
    public void handleResult(Result result) {

        try
        {
            if(result.getText().toString().length() != 12)
            {
                capturaRealizada = false;
                Toast.makeText(this, "CÓDIGO INCORRECTO", Toast.LENGTH_SHORT).show();
            }
            else
            {
                if(!capturaRealizada)
                {
                    capturaRealizada = true;
                    mensaje = result.getText().toString();
                    pDialog = new ProgressDialog(ScannerBarras.this);
                    pDialog.setMessage("Validando código de solicitud...");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {

                            if(consultarSolicitudUsada) //****************************************************************
                            {
                                String queryy = "SELECT * FROM FOTOGRAFIAS_POR_SOLICITUD_DE_CLIENTE WHERE nosolicitud= '" + mensaje + "'";
                                List<FotografiasPorSolicitudDeCliente> listFotos = FotografiasPorSolicitudDeCliente.findWithQuery(FotografiasPorSolicitudDeCliente.class, queryy);

                                if (listFotos.size() > 0) {
                                    Intent intent = new Intent(ScannerBarras.this, CapturaSolicitud.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    intent.putExtra("numeroSolicitud", listFotos.get(0).nosolicitud);
                                    intent.putExtra("codigoPromotor", codigoPromotor);
                                    intent.putExtra("BarrasCorrecto", "1");
                                    intent.putExtra("consultaDeFotografiasGuardadas", consultarSolicitudUsada);
                                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                                    Log.v(TAG, "consultaDeFotografiasGuardadas: " + consultarSolicitudUsada);
                                    finish();

                                } else {
                                    pDialog.dismiss();
                                    mScannerView.stopCamera();
                                    mScannerView.stopCameraPreview();
                                    dialogo = new AlertDialog.Builder(ScannerBarras.this);
                                    dialogo.setTitle("Información");
                                    dialogo.setMessage("Parece que esta solicitud aún no tiene ningun registro, por favor ve a la página principal y selecciona **Nueva solicitud**");
                                    dialogo.setCancelable(false);
                                    dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                        }
                                    });
                                    dialogo.show();
                                    capturaRealizada = false;
                                }



                            }//************************************************************
                            else
                            {
                                String query = "SELECT * FROM SOLICITUD WHERE solSerie= '" + mensaje + "' and usada = '0'";
                                List<Solicitud> notes = Solicitud.findWithQuery(Solicitud.class, query);

                                if (notes.size() > 0) {
                                    Intent intent = new Intent(ScannerBarras.this, CapturaSolicitud.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    intent.putExtra("numeroSolicitud", notes.get(0).sol_Serie);
                                    intent.putExtra("codigoPromotor", codigoPromotor);
                                    intent.putExtra("BarrasCorrecto", "1");
                                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                                    finish();

                                } else {
                                    pDialog.dismiss();
                                    mScannerView.stopCamera();
                                    mScannerView.stopCameraPreview();
                                    dialogo = new AlertDialog.Builder(ScannerBarras.this);
                                    dialogo.setTitle("Información");
                                    dialogo.setMessage("Número de solicitud no encontrado, o ya fue usado anteriormente");
                                    dialogo.setCancelable(false);
                                    dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                        }
                                    });
                                    dialogo.show();
                                    capturaRealizada = false;
                                }
                            }

                        }
                    }, 2000);
                }
            }
        }catch (NumberFormatException e)
        {
            Log.v(TAG, "Error en captura de código: " + e.getMessage());
        }
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public final void onDestroy()
    {
        super.onDestroy();
        stopService();
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);

        stopService(serviceIntent);
    }

    @Override
    public void onBackPressed() {
       finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
