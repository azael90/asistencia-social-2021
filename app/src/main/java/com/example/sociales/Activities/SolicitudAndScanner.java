package com.example.sociales.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.sociales.BuildConfig;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.ExportImportDB;
import com.example.sociales.DatabaseLocal.Solicitud;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.LogRegister;
import com.example.sociales.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import pl.tajchert.nammu.Nammu;

public class SolicitudAndScanner extends AppCompatActivity {

    private static final String TAG = "SolicitudAndScanner";
    String codigoPromotor="";
    public static String NOMBRE_IMAGEN_PREFIJO="";
    Dialog dialogShowActivationCode;
    CardView cardCodigo, cardCamera;
    ProgressDialog pDialog;
    String letraIndice="", codigoDeActivacion="";

    Bitmap bitmap;
    int PICK_IMAGE_REQUEST = 1;
    String imageName;
    public int indicadorCarta=0;
    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";
    private final int PHOTO_CODE = 200;
    private String mPath;
    private final String numeroSolicitud="";
    public ArrayList<String> listaImagenes = new ArrayList<>();
    public ArrayList<String> listaBitmaps =new ArrayList<>();
    public static String latitud="", longitud="";
    String codigoDeBarrasEscaneado ="";
    Dialog myErrorDialog;
    boolean photoIsCapture= false, codigoIsCapture= false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_solicitud_and_scanner);
        myErrorDialog = new Dialog(this);
        final Bundle extras = getIntent().getExtras();

        ImageView btBack =(ImageView) findViewById(R.id.btBack);
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(extras!=null)
            codigoPromotor= extras.getString("codigoPromotor");
        else
            Log.v(TAG, "No se recuperaron extras");


        CardView cardSolicitud = (CardView) findViewById(R.id.cardSolicitud);
        cardSolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptions("-SolicitudCompleta");
                NOMBRE_IMAGEN_PREFIJO = "SolicitudCompleta"+"C"+codigoPromotor;
            }
        });

        CardView cardScanner = (CardView) findViewById(R.id.cardScanner);
        cardScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codigoIsCapture = false;
                new IntentIntegrator(SolicitudAndScanner.this).initiateScan();
            }
        });




        Button bt100 =(Button) findViewById(R.id.bt100);
        bt100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
                etInversionInicial.setText("100");
            }
        });

        Button bt200 =(Button) findViewById(R.id.bt200);
        bt200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
                etInversionInicial.setText("200");
            }
        });

        Button bt300 =(Button) findViewById(R.id.bt300);
        bt300.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
                etInversionInicial.setText("300");
            }
        });

        Button bt400 =(Button) findViewById(R.id.bt400);
        bt400.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
                etInversionInicial.setText("400");
            }
        });

        Button bt500 =(Button) findViewById(R.id.bt500);
        bt500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
                etInversionInicial.setText("500");
            }
        });




        Button btFinalizar =(Button) findViewById(R.id.btFinalizar);
        btFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                    {
                        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if(isGPSEnabled)
                        {
                            if (getLocationMode() == 3)
                            {
                                TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
                                if(codigoIsCapture && photoIsCapture && !Objects.requireNonNull(etInversionInicial.getText()).toString().equals("")) {
                                    saveDataAndCheckInformation();
                                    ExportImportDB exportImportDB = new ExportImportDB();
                                    try {
                                        if (exportImportDB.backupDBHidden()) {
                                            Log.i(TAG, "EXPORTADA CORRECTAMENTE");
                                        } else {
                                            Log.i(TAG, "ERROR AL EXPORTAR");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else
                                    showErrorPopup("Por favor verifica y completa los campos correctamente.");
                            }
                            else
                            {
                                final AlertDialog.Builder dialog = getBuilder();
                                try {
                                    dialog.setCancelable(false);
                                    dialog.setTitle("Advertencia");
                                    dialog.setMessage("Necesitas activar el modo GPS ALTA PRESICIÓN para continuar.");
                                    dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                                    {
                                        if (getLocationMode() != 3)
                                            dialog.create().show();
                                    });
                                    dialog.create().show();

                                } catch (Exception e) {

                                    Toast.makeText(SolicitudAndScanner.this, "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Verifica tu GPS", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        final AlertDialog.Builder dialog = getBuilder();
                        try {
                            dialog.setCancelable(false);
                            dialog.setTitle("Advertencia");
                            dialog.setMessage("Parece que no tienes el GPS Activado");
                            dialog.create().show();

                        } catch (Exception e) {
                            Toast.makeText(SolicitudAndScanner.this, "Parece que no tienes el GPS Activado", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                }catch (NullPointerException ex)
                {
                    Log.e(TAG, "Ocurrio un error: " + ex.getMessage());
                    Toast.makeText(getApplicationContext(), "No podemos verificar el GPS, reinicia el GPS", Toast.LENGTH_SHORT).show();
                    Mint.logEvent("Error Exception " + numeroSolicitud + " - user: " + codigoPromotor, MintLogLevel.Info);
                }
            }
        });


    }


    public  void saveDataAndCheckInformation()
    {
        pDialog=new ProgressDialog(SolicitudAndScanner.this);
        pDialog.setMessage("Guardando datos, espera por favor...");
        pDialog.setCancelable(false);
        pDialog.show();


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            RandomString gen = new RandomString(7, ThreadLocalRandom.current());
            codigoDeActivacion = "M" + letraIndice + generateString(7);
            //codigoDeActivacion = "M" + letraIndice + gen.nextString();
            Log.d(TAG, "saveDataAndCheckInformation: Codigo de activacion generado es: " + codigoDeActivacion);
        }

        for(int i=0; i<=listaBitmaps.size()-1;i++) {
            listaImagenes.add(NOMBRE_IMAGEN_PREFIJO + codigoDeActivacion);
        }

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format( new Date() );
        TextInputEditText etInversionInicial = (TextInputEditText) findViewById(R.id.etInversionInicial);
        String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();

        try {
            if (arregloCoordenadas.length > 0) {
                latitud =arregloCoordenadas[0];
                longitud = arregloCoordenadas[1];
            }

        } catch (NullPointerException ex) {
            Log.e(TAG, "onClick: Error " + ex.getMessage());
            try {
                String[] arregloCoordenadasAux = ApplicationResourcesProvider.getCoordenadasFromApplication();
                if (arregloCoordenadasAux.length > 0) {
                    latitud =arregloCoordenadasAux[0];
                    longitud = arregloCoordenadasAux[1];
                }
            } catch (Exception e) {
                Log.e(TAG, "onClick: Error " + e.getMessage());
            }

        }

        for(int i=0; i<=listaImagenes.size()-1;i++) {
            Control.insertarNuevasSolicitudes(
                    ""+listaImagenes.get(i).toString(),
                    "FOTO",
                    ""+ codigoDeBarrasEscaneado,
                    "" +date,
                    "" + latitud,
                    "" + longitud,
                    "" +codigoDeActivacion,
                    etInversionInicial.getText().toString()
            );
        }


        for(int i=0; i<=listaBitmaps.size()-1;i++) {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date datee;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(datee = new Date());
            calendar.add(Calendar.SECOND, i+3);
            Date fechaSalida = calendar.getTime();

            Control.insertarArchivo(
                    "" + codigoDeActivacion,
                    "2",
                    "" + listaImagenes.get(i).toString(),
                    "" + listaBitmaps.get(i).toString(),
                    0,
                    "" + latitud,
                    "" +longitud,
                    "" + dateFormatter.format(fechaSalida));
        }

        try {
            pDialog.dismiss();
            pDialog.cancel();
            showPopup(codigoDeActivacion, codigoDeBarrasEscaneado);
        }catch (Throwable e){
            Log.e(TAG, "saveDataAndCheckInformation: " + e.toString() );
        }
    }


    private AlertDialog.Builder getBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else{
            return new AlertDialog.Builder(this);
        }
    }

    public int getLocationMode() {
        try {
            return Settings.Secure.getInt(getApplicationContext().getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void showOptions(final String seleccion) {
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitudAndScanner.this);
        builder.setTitle("Eleige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which] == "Tomar foto"){
                    photoIsCapture= false;
                    openCamera(seleccion);
                }else if(option[which] == "Elegir de galeria"){
                    photoIsCapture = false;
                    showFileChooser();
                }else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }


    private void openCamera(String prefijoSeleccion)
    {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            imageName = prefijoSeleccion + timestamp.toString() + ".jpg";
            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
            File newFile = new File(mPath);
            try
            {
                newFile.createNewFile();
            }
            catch (IOException e)
            {
                Log.v(TAG, "Error al abrir camara: " + e.getMessage());
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleciona imagen"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        //*************** ESCANNER DE CODIGO DE BARRAS **************************
        if(result != null){
            if(result.getContents() != null) {

                Log.v(TAG, "onActivityResult: " + result.getContents());
 
                if(Control.existeLaSolicitud(result.getContents())){
                    Toast.makeText(this, "El código fue escaneado correctamente", Toast.LENGTH_LONG).show();
                    letraIndice = Control.getLetraIndice(result.getContents());
                    codigoDeBarrasEscaneado = result.getContents();
                    ImageView imgCheckScanner = (ImageView) findViewById(R.id.imgCheckScanner);
                    imgCheckScanner.setVisibility(View.VISIBLE);
                    CardView cardScanner = (CardView) findViewById(R.id.cardScanner);
                    cardScanner.setEnabled(false);
                    codigoIsCapture = true;
                }
                else{
                    letraIndice="XX";
                    codigoDeBarrasEscaneado = "";
                    ImageView imgCheckScanner = (ImageView) findViewById(R.id.imgCheckScanner);
                    imgCheckScanner.setVisibility(View.GONE);
                    CardView cardScanner = (CardView) findViewById(R.id.cardScanner);
                    cardScanner.setEnabled(true);
                    showErrorPopup("El código parece que no corresponde a ningúna solicitud o ya fue usado anteriormente, verifica nuevamente.");
                    codigoIsCapture = false;
                }



            }
            else {
                Log.e(TAG, "onActivityResult: No se escaneo el codigo");
                Toast.makeText(this, "No se pudo escanear el código, intenta nuevamente", Toast.LENGTH_SHORT).show();
                codigoDeBarrasEscaneado="";
                ImageView imgCheckScanner = (ImageView) findViewById(R.id.imgCheckScanner);
                imgCheckScanner.setVisibility(View.GONE);
                imgCheckScanner.setEnabled(true);
                CardView cardScanner = (CardView) findViewById(R.id.cardScanner);
                cardScanner.setEnabled(true);
                codigoIsCapture = false;
            }
        }

        //**************************** TOMAR FOTO CON EL DISPOSITIVO *************************************
        if(resultCode == RESULT_OK)
        {
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this, new String[]{mPath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> Uri = " + uri);
                        }
                    });

                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);
                    if (mPath.contains("SolicitudCompleta"))
                    {
                        CardView cardSolicitud = (CardView) findViewById(R.id.cardSolicitud);
                        cardSolicitud.setEnabled(false);
                        ImageView imgCheckSolicitud = (ImageView) findViewById(R.id.imgCheckSolicitud);
                        imgCheckSolicitud.setVisibility(View.VISIBLE);
                        //String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                        //listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                        Log.d(TAG, "onActivityResult: ListaImagenes ----> " + listaImagenes.size());
                        Log.d(TAG, "onActivityResult: ListaBitmaps ----> " + listaBitmaps.size());
                        photoIsCapture = true;
                    }
                    else
                        photoIsCapture = false;

                    break;
            }
        }

        //********************************** ELIGIR IMAGEN DE GALERIA *********************************************
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                Uri selectedImage = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                String wholeID = DocumentsContract.getDocumentId(selectedImage);
                String id = wholeID.split(":")[1];
                String[] column = { MediaStore.Images.Media.DATA };
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[] { id }, null);

                String filesPath = "";
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filesPath = cursor.getString(columnIndex);
                }
                cursor.close();

                if (filesPath != null) {
                    if(!filesPath.equals("")){
                        CardView cardSolicitud = (CardView) findViewById(R.id.cardSolicitud);
                        cardSolicitud.setEnabled(false);
                        ImageView imgCheckSolicitud = (ImageView) findViewById(R.id.imgCheckSolicitud);
                        imgCheckSolicitud.setVisibility(View.VISIBLE);
                        //String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                        //listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                        Log.d(TAG, "onActivityResult: ListaImagenes LOAD ----> " + listaImagenes.size());
                        Log.d(TAG, "onActivityResult: ListaBitmaps LOAD ----> " + listaBitmaps.size());
                        photoIsCapture = true;
                    } else {
                        photoIsCapture = false;
                        Toast.makeText(this, "Imagen no soportada, intenta nuavamente, o selecciona Tomar foto", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    photoIsCapture = false;
                    Toast.makeText(this, "Ocurrio un problema con la fotografía que seleccionaste, intenta nuevamente", Toast.LENGTH_LONG).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
                photoIsCapture = false;
            }
        }
    }


    public String getStringImagen(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file_path", mPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPath = savedInstanceState.getString("file_path");
    }








    public void showPopup(final String codigoActivacion, String solicitud) {
        dialogShowActivationCode = new Dialog(this);
        final Button btFinalizar, btCapturarPantalla, btFinalizarCamera;
        String cadena="";
        LottieAnimationView lottieNF;
        TextView tvCodigo, tvNoSol;
        dialogShowActivationCode.setContentView(R.layout.solicitud_final);
        dialogShowActivationCode.setCancelable(false);
        btFinalizar = (Button) dialogShowActivationCode.findViewById(R.id.btNew);
        tvCodigo = (TextView) dialogShowActivationCode.findViewById(R.id.tvCodigo);
        btCapturarPantalla =(Button) dialogShowActivationCode.findViewById(R.id.btCapturarPantalla);
        lottieNF= (LottieAnimationView)dialogShowActivationCode.findViewById(R.id.lottieNF);
        cardCodigo=(CardView) dialogShowActivationCode.findViewById(R.id.cardCodigo);
        cardCamera=(CardView) dialogShowActivationCode.findViewById(R.id.cardRecapturaDeFotos);
        tvNoSol=(TextView) dialogShowActivationCode.findViewById(R.id.tvNoSol);
        lottieNF.setAnimation("success2.json");
        lottieNF.loop(true);


        tvNoSol.setText("No. Solicitud: "+ solicitud);
        cardCamera.setVisibility(View.GONE);
        cardCodigo.setVisibility(View.VISIBLE);

        for(int i=0; i<=codigoActivacion.length()-1;i++)
        {
            cadena=cadena + codigoActivacion.charAt(i);
            if(i==0 || i==4)
            {
                cadena=cadena + " ";
            }
        }
        tvCodigo.setText(cadena);

        btFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                try
                {
                    if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                    {
                        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if(isGPSEnabled)
                        {
                            if (getLocationMode() == 3)
                            {
                                Intent intent = new Intent(getApplicationContext(), PrincipalScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                Toast.makeText(getApplicationContext(), "Solicitud guardada correctamente", Toast.LENGTH_SHORT).show();
                                startActivity(intent);
                                dialogShowActivationCode.dismiss();
                                finish();

                            }
                            else
                            {
                                final AlertDialog.Builder dialog = getBuilder();
                                try {
                                    dialog.setCancelable(false);
                                    dialog.setTitle("Advertencia");
                                    dialog.setMessage("Necesitas activar el modo GPS ALTA PRESICIÓN para continuar.");
                                    dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                                    {
                                        if (getLocationMode() != 3)
                                            dialog.create().show();
                                    });
                                    dialog.create().show();

                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Verifica tu GPS", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        final AlertDialog.Builder dialog = getBuilder();
                        try {
                            dialog.setCancelable(false);
                            dialog.setTitle("Advertencia");
                            dialog.setMessage("Parece que no tienes el GPS Activado");
                            dialog.create().show();

                        } catch (Exception e) {

                            Toast.makeText(getApplicationContext(), "Parece que no tienes el GPS Activado", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                }catch (Throwable ex)
                {
                    Log.e(TAG, ex.toString());
                    Intent intent = new Intent(getApplicationContext(), PrincipalScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    Toast.makeText(getApplicationContext(), "Solicitud guardada correctamente", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                    dialogShowActivationCode.dismiss();
                    finish();
                }









            }
        });
        btCapturarPantalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
                boolean isDirectoryCreated = file.exists();

                if(!isDirectoryCreated)
                    isDirectoryCreated = file.mkdirs();

                if(isDirectoryCreated)
                {
                    Long timestamp = System.currentTimeMillis() / 1000;
                    String codigoActivacion = codigoDeActivacion + "_" + timestamp.toString() + ".jpg";
                    String mPathh = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + codigoActivacion;
                    File newFile = new File(mPathh);
                    try
                    {
                        newFile.createNewFile();
                        try {
                            lottieNF.setProgress(0.5f);
                            View v1 = dialogShowActivationCode.getWindow().getDecorView().getRootView();
                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);

                            FileOutputStream outputStream = new FileOutputStream(newFile);
                            int quality = 30;
                            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                            outputStream.flush();
                            outputStream.close();

                            Toast.makeText(SolicitudAndScanner.this, "Captura de pantalla realizada", Toast.LENGTH_LONG).show();
                            btFinalizar.setLayoutParams(new TableLayout.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, 1f));
                            btCapturarPantalla.setVisibility(View.GONE);

                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }


            }
        });

        lottieNF.playAnimation();
        dialogShowActivationCode.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogShowActivationCode.show();

    }


    public void showErrorPopup(final String codeError) {
        final Button btNew;
        TextView tvCodeError;
        myErrorDialog.setContentView(R.layout.login_incorrecto);
        myErrorDialog.setCancelable(false);
        btNew = (Button) myErrorDialog.findViewById(R.id.btNew);
        tvCodeError = (TextView) myErrorDialog.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);

        btNew.setText("Aceptar");

        btNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                myErrorDialog.dismiss();
            }
        });
        myErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myErrorDialog.show();

    }


    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public String generateString(int length) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }

        return builder.toString();
    }

}


 class RandomString {

     private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
     private static final String lower = upper.toLowerCase(Locale.ROOT);
     private static final String digits = "0123456789";
     public static final String alphanum = upper + lower + digits;
     private final Random random;
     private final char[] symbols;
     private final char[] buf;

     public RandomString(int length, Random random) {
         this(length, random, alphanum);
     }

     public String nextString() {
         for (int idx = 0; idx < buf.length; ++idx)
             buf[idx] = symbols[random.nextInt(symbols.length)];
         return new String(buf);
     }

     public RandomString(int length, Random random, String symbols) {
         if (length < 1) throw new IllegalArgumentException();
         if (symbols.length() < 2) throw new IllegalArgumentException();
         this.random = Objects.requireNonNull(random);
         this.symbols = symbols.toCharArray();
         this.buf = new char[length];
     }


     public RandomString(int length) {
         this(length, new SecureRandom());
     }

     public RandomString() {
         this(21);
     }

 }