package com.example.sociales.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.icu.text.NumberFormat;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.sociales.Adapters.AdapertPDFDescargados;
import com.example.sociales.Adapters.ProductAdapterImages;
import com.example.sociales.Adapters.ProductImages;
import com.example.sociales.DatabaseLocal.ArchivosDescargados;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.EsquemaPago;
import com.example.sociales.DatabaseLocal.FotografiasPorSolicitudDeCliente;
import com.example.sociales.DatabaseLocal.Localidad_Colonia;
import com.example.sociales.DatabaseLocal.Solicitud;
import com.example.sociales.DatabaseLocal.publicidad;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.BaseActivity;
import com.example.sociales.Extras.BuildConfig;
import com.example.sociales.Extras.URL;
import com.example.sociales.Extras.VolleySingleton;
import com.example.sociales.R;
import com.example.sociales.Service.Constants;
import com.example.sociales.Service.ForegroundService;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.Result;
import com.karan.churi.PermissionManager.PermissionManager;
//import com.josketres.rfcfacil.Rfc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import pl.tajchert.nammu.Nammu;

import static com.example.sociales.Extras.ApplicationResourcesProvider.getContext;

public class CapturaSolicitud extends BaseActivity
{
    public static String TAG="CAPTURA_SOLICITUD";
    private PermissionManager permissionManager;
    NestedScrollView nested_layout;
    //MyCountDownTimer countDownTimer;
    private Button btContinuar;
    private LinearLayout layoutDatos, layoutGeneral;
    private ImageView btBack, imgEscanear, imgCalendario, btBorrarTodos, imgCamara;
    private EditText etNumeroSolicitud, etApellidoPaterno, etApellidoMaterno, etFechaNac,
            etTelefono, etCodigoPostal, etCalleNumero, etObservaciones, etRFCGenerado, etNuevoIngreso, etEntreCalles, etInversionInicial;
    private TextView btValidar, etValidarRFC, tv1, tv2, tv3, tv4;
    private Spinner spEsquemaDePago;
    private String numeroSolicitud="";
    private TextInputLayout campoNuevoIngreso, campoEsqumaDePago;
    private TextInputEditText etNumeroExterior, etNumeroInterior, etOpcionalLocalidad, etOpcionalLocalidadCobranza;
    public TextInputEditText etNombres;
    public CheckBox cbCopiarDomicilioDeCasa;
    public TextInputEditText etCalleNumeroCobranza, etNumeroExteriorCobranza, etNumeroInteriorCobranza, etCodigoPostalCobranza, etEntreCallesCobranza;
    public AutoCompleteTextView etColoniaCobranza;
    public Spinner spLocalidadCobranza;
    private boolean homeKey= false;
    public Button btCobranza, btGuardarCasa, btGuardarCobranza;
    public TextView btCasa;
    Dialog myDialog;
    private TextInputLayout indicadorDeFecha;
    public AutoCompleteTextView etColonia;
    public Spinner spLocalidad, spEstadoCivil;

    //-----------------------
    public boolean isGPSEnabled = false;
    ProgressDialog pDialog;
    public boolean solicitudEncontrada=false;
    private AlertDialog.Builder dialogo;
    public ArrayList<String> CIUDADES = null;
    String codigoPromotor="";
    public boolean findNumero=false;
    String tipoSolicitud="", letraIndice="";
    public String coloniaSelecionada, coloniaSeleccionadaCobranza;
    //-------------------------------
    //private long startTime=600000; // 15 MINS IDLE TIME
    //private long startTime=120000; // 15 MINS IDLE TIME
    //private final long interval = 1000;

    String barrasCorrecto="";
    String opcionSeleccionadaIDSpinner="";
    ArrayList<String> esquemas;
    static final int PICK_CONTACT_REQUEST = 1;

    public int dia, mes, ano;
    Button bt100, bt200, bt300, bt400, bt500;
    final boolean[] soloHayUnaPosicionDeLocalidaDeDomicilioDeCasa = {false};
    final boolean[] soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza = {false};
    public boolean datosGuardadosDeDomicilioDeCasa = false;
    public boolean datosGuardadosDeDomicilioDeCobranza = false;
    public ConstraintLayout layoutDomicilioDeCasa, layoutDomicilioDeCobranza;
    TextView tvTiempo;
    public String [] estadosCivil={"Estado civil...","Casado(a)", "Soltero(a)", "Viudo(a)", "Union Libre", "Divorciado(a)", "Otros"};
    boolean estaBanderaVieneDesdeElCheckBox = false;
    boolean consultaDeFotosGuardadas = false;
    boolean busquedaGoogleCasa = false, busquedaGoogleCobranza = false;
    LinearLayout btBuscarConGoogle, btBuscarConGoogleCobranza;
    LottieAnimationView lottieGoogle, lottieGoogleCobranza;
    TextInputLayout textInputLocalidad, textInputLocalidadCobranza;




    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_captura_solicitud);

        permissionManager = new PermissionManager(){};
        permissionManager.checkAndRequestPermissions(this);
        nested_layout  = (NestedScrollView) findViewById(R.id.nested_layout);
        layoutGeneral =(LinearLayout) findViewById(R.id.layoutGeneral);
        tvTiempo=(TextView) findViewById(R.id.tvTiempo);
        myDialog = new Dialog(this);
        btContinuar=(Button) findViewById(R.id.btContinuar);
        btBack=(ImageView)findViewById(R.id.btBack);
        imgEscanear=(ImageView)findViewById(R.id.imgEscanear);
        etNumeroSolicitud=(TextInputEditText)findViewById(R.id.etNumeroSolicitud);
        etNombres=(TextInputEditText)findViewById(R.id.etNombres);
        spEstadoCivil =(Spinner) findViewById(R.id.spEstadoCivil);
        cbCopiarDomicilioDeCasa =(CheckBox) findViewById(R.id.cbCopiarDomicilioDeCasa);
        campoNuevoIngreso =(TextInputLayout) findViewById(R.id.campoNuevoIngreso);
        campoEsqumaDePago =(TextInputLayout) findViewById(R.id.campoEsqumaDePago);
        lottieGoogle = (LottieAnimationView) findViewById(R.id.lottieGoogle);
        lottieGoogleCobranza = (LottieAnimationView) findViewById(R.id.lottieGoogleCobranza);
        textInputLocalidad = (TextInputLayout) findViewById(R.id.textInputLocalidad);
        textInputLocalidadCobranza = (TextInputLayout) findViewById(R.id.textInputLocalidadCobranza);


        //--Domicilio de cobranza
        etColoniaCobranza = (AutoCompleteTextView) findViewById(R.id.etColoniaCobranza);
        spLocalidadCobranza =(Spinner) findViewById(R.id.spLocalidadCobranza);
        layoutDomicilioDeCobranza=(ConstraintLayout) findViewById(R.id.layoutDomicilioDeCobranza);
        etCalleNumeroCobranza =(TextInputEditText) findViewById(R.id.etCalleNumeroCobranza);
        etCodigoPostalCobranza =(TextInputEditText) findViewById(R.id.etCodigoPostalCobranza);
        etEntreCallesCobranza =(TextInputEditText) findViewById(R.id.etEntreCallesCobranza);
        etCalleNumeroCobranza =(TextInputEditText) findViewById(R.id.etCalleNumeroCobranza);
        etOpcionalLocalidad =(TextInputEditText) findViewById(R.id.etOpcionalLocalidad);
        etOpcionalLocalidadCobranza =(TextInputEditText) findViewById(R.id.etOpcionalLocalidadCobranza);

        etNumeroExteriorCobranza =(TextInputEditText) findViewById(R.id.etNumeroExteriorCobranza);
        btGuardarCobranza =(Button) findViewById(R.id.btGuardarCobranza);
        btCobranza = (Button) findViewById(R.id.btCobranza);
        etNumeroInteriorCobranza=(TextInputEditText)findViewById(R.id.etNumeroInteriorCobranza);
        // domicilio de cobranza--

        layoutDomicilioDeCasa=(ConstraintLayout) findViewById(R.id.layoutDomicilioDeCasa);
        etNumeroExterior =(TextInputEditText) findViewById(R.id.etNumeroExterior);
        etNumeroInterior =(TextInputEditText) findViewById(R.id.etNumeroInterior);
        btGuardarCasa = (Button) findViewById(R.id.btGuardarCasa);
        btCasa = (TextView) findViewById(R.id.btCasa);


        etApellidoPaterno=(EditText)findViewById(R.id.etApellidoPaterno);
        etApellidoMaterno=(EditText)findViewById(R.id.etApellidoMaterno);
        etFechaNac=(TextInputEditText)findViewById(R.id.etFechaNac);
        etTelefono=(EditText)findViewById(R.id.etTelefono);
        spLocalidad=(Spinner) findViewById(R.id.spLocalidad);
        etCodigoPostal=(TextInputEditText)findViewById(R.id.etCodigoPostal);
        etCalleNumero=(EditText)findViewById(R.id.etCalleNumero);
        etObservaciones=(EditText)findViewById(R.id.etObservaciones);
        etRFCGenerado=(TextInputEditText)findViewById(R.id.etRFCGenerado);
        etNuevoIngreso=(EditText)findViewById(R.id.etNuevoIngreso);
        etEntreCalles=(EditText)findViewById(R.id.etEntreCalles);
        etColonia=(AutoCompleteTextView) findViewById(R.id.etColonia);

        imgCamara =(ImageView) findViewById(R.id.imgCamara);
        btBuscarConGoogle =(LinearLayout) findViewById(R.id.btBuscarConGoogle);
        btBuscarConGoogleCobranza =(LinearLayout) findViewById(R.id.btBuscarConGoogleCobranza);
        btValidar=(TextView)findViewById(R.id.btValidar);
        etValidarRFC=(TextView)findViewById(R.id.etValidarRFC);
        tv1=(TextView)findViewById(R.id.tv1);
        tv2=(TextView)findViewById(R.id.tv2);
        tv3=(TextView)findViewById(R.id.tv3);
        tv4=(TextView)findViewById(R.id.tv4);
        imgCalendario=(ImageView)findViewById(R.id.imgCalendario);
        spEsquemaDePago=(Spinner)findViewById(R.id.spEsquemaDePago);
        layoutDatos=(LinearLayout)findViewById(R.id.layoutDatos);
        indicadorDeFecha=(TextInputLayout)findViewById(R.id.indicadorDeFecha);
        btBorrarTodos=(ImageView)findViewById(R.id.btBorrarTodos);

        bt100=(Button)findViewById(R.id.bt100);
        bt200=(Button)findViewById(R.id.bt200);
        bt300=(Button)findViewById(R.id.bt300);
        bt400=(Button)findViewById(R.id.bt400);
        bt500=(Button)findViewById(R.id.bt500);
        etInversionInicial=(EditText)findViewById(R.id.etInversionInicial);
        layoutDatos.setVisibility(View.GONE);

        lottieGoogle.setAnimation("act.json");
        lottieGoogle.loop(true);

        lottieGoogleCobranza.setAnimation("act.json");
        lottieGoogleCobranza.loop(true);
        startService();



        final Bundle extras = getIntent().getExtras();
        if(extras!=null)
        {
            if(extras.containsKey("consultaDeFotografiasGuardadas"))
                consultaDeFotosGuardadas = extras.getBoolean("consultaDeFotografiasGuardadas");
            else
                consultaDeFotosGuardadas = false;

            if(extras.containsKey("reCapturaDeFotografias"))
                homeKey = extras.getBoolean("reCapturaDeFotografias");
            else
                homeKey = false;

            numeroSolicitud= extras.getString("numeroSolicitud");
            codigoPromotor= extras.getString("codigoPromotor");
            barrasCorrecto = extras.getString("BarrasCorrecto");
            etNumeroSolicitud.setText(numeroSolicitud);
        }
        else
        {
            Log.v(TAG, "No se recuperaron datos de extras");
        }

        if(verificarGPS())
        {
            llenarSpinnerConEsquemasDePagoYLocalidades();
            etInversionInicial.setText("100");
            if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CANCUN)
            {
                campoNuevoIngreso.setVisibility(View.GONE);
                etNuevoIngreso.setText("-");
                campoEsqumaDePago.setVisibility(View.GONE);
            }
            ArrayAdapter<CharSequence> adaptador = new ArrayAdapter(this, android.R.layout.simple_spinner_item, esquemas);
            spEsquemaDePago.setAdapter(adaptador);
            ArrayAdapter<CharSequence> adapterEstadoCivil = new ArrayAdapter(this, android.R.layout.simple_spinner_item, estadosCivil);
            spEstadoCivil.setAdapter(adapterEstadoCivil);
            etFechaNac.setEnabled(false);
            etColonia.setThreshold(1);
            ArrayAdapter<String> adapterColonias = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, llenarCamposDeColonias());
            etColonia.setAdapter(adapterColonias);
            etColoniaCobranza.setAdapter(adapterColonias);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Enciende el GPS para continuar", Toast.LENGTH_LONG);
            Log.v(TAG, "GPS deshabilitado, se necesita activar");
        }


        if(consultaDeFotosGuardadas)
        {
            Intent intent = new Intent(CapturaSolicitud.this, ComprobantesFotografias.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("reCapturaDeFotos", consultaDeFotosGuardadas);
            intent.putExtra("codigoPromotor", codigoPromotor);
            intent.putExtra("numeroSolicitud", numeroSolicitud);
            intent.putExtra("tipoSolicitud", tipoSolicitud);
            startActivityForResult(intent, PICK_CONTACT_REQUEST);
        }



        cbCopiarDomicilioDeCasa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                if(checked)
                {
                    if(busquedaGoogleCasa)
                         busquedaGoogleCobranza = true;
                    else
                    {
                        busquedaGoogleCobranza = false;
                        etOpcionalLocalidadCobranza.setText("");
                        textInputLocalidadCobranza.setVisibility(View.GONE);
                    }

                    estaBanderaVieneDesdeElCheckBox = true;
                    etCalleNumeroCobranza.setText(etCalleNumero.getText().toString());
                    etNumeroExteriorCobranza.setText(etNumeroExterior.getText().toString());
                    etNumeroInteriorCobranza.setText(etNumeroInterior.getText().toString());
                    etCodigoPostalCobranza.setText(etCodigoPostal.getText().toString());
                    etEntreCallesCobranza.setText(etEntreCalles.getText().toString());
                    coloniaSeleccionadaCobranza = coloniaSelecionada;

                    try
                    {
                        etOpcionalLocalidadCobranza.setText(etOpcionalLocalidad.getText().toString());
                    }catch (Throwable e)
                    {
                        Log.v(TAG, "No se pudo copiar la localidad  opcional de cobranza: " + e.getMessage());
                    }

                    try
                    {
                        etColoniaCobranza.setText(busquedaGoogleCasa ? etColonia.getText().toString() : coloniaSeleccionadaCobranza);
                        etColoniaCobranza.setEnabled(false);
                        spLocalidadCobranza.setEnabled(false);
                        spLocalidadCobranza.setVisibility(View.VISIBLE);
                        ArrayList<String> localidadesList_temporal = new ArrayList<String>();
                        localidadesList_temporal.add(busquedaGoogleCasa ? etOpcionalLocalidad.getText().toString() : spLocalidad.getSelectedItem().toString());
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(CapturaSolicitud.this, android.R.layout.simple_list_item_1, localidadesList_temporal);
                        spLocalidadCobranza.setAdapter(adapterLocalidades);
                        spLocalidadCobranza.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.contorno));
                        soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza [0] = true;
                    }catch(Exception e)
                    {
                        e.printStackTrace();
                        spLocalidadCobranza.setVisibility(View.GONE);
                    }

                }
                else
                {
                    estaBanderaVieneDesdeElCheckBox = false;
                    etCalleNumeroCobranza.setText("");
                    etNumeroExteriorCobranza.setText("");
                    etNumeroInteriorCobranza.setText("");
                    etCodigoPostalCobranza.setText("");
                    etEntreCallesCobranza.setText("");
                    etColoniaCobranza.setEnabled(true);
                    spLocalidadCobranza.setEnabled(true);

                    soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza [0] = false;
                    coloniaSeleccionadaCobranza = null;
                    etColoniaCobranza.setText("");
                    spLocalidadCobranza.setSelection(0);
                    spLocalidadCobranza.setVisibility(View.GONE);

                }
            }
        });

        etColoniaCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coloniaSeleccionadaCobranza = null;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        etColonia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coloniaSelecionada = null;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        etColonia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                coloniaSelecionada = adapterView.getItemAtPosition(i).toString();

                busquedaGoogleCasa = false;
                etOpcionalLocalidad.setText("");
                textInputLocalidad.setVisibility(View.GONE);
                spLocalidad.setVisibility(View.VISIBLE);
                ArrayList<String> LOCALIDADES = new ArrayList<String>();
                LOCALIDADES.add("Selecciona una localidad");
                String query="SELECT * FROM LocalidadColonia where colonia = '" + coloniaSelecionada + "'group by localidad";
                List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
                if(listaLocalidades.size()>0)
                {
                    if(listaLocalidades.size()==1)
                    {
                        soloHayUnaPosicionDeLocalidaDeDomicilioDeCasa[0] = true;
                        LOCALIDADES.clear();
                        LOCALIDADES.add(listaLocalidades.get(0).getLocalidad());
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(CapturaSolicitud.this, android.R.layout.simple_list_item_1, LOCALIDADES);
                        spLocalidad.setAdapter(adapterLocalidades);
                    }
                    else {
                        soloHayUnaPosicionDeLocalidaDeDomicilioDeCasa[0] = false;
                        for (int t = 0; t < listaLocalidades.size(); t++) {
                            try {
                                LOCALIDADES.add(listaLocalidades.get(t).getLocalidad());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(CapturaSolicitud.this, android.R.layout.simple_list_item_1, LOCALIDADES);
                        spLocalidad.setAdapter(adapterLocalidades);
                    }

                    etColonia.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.contorno));
                    etColonia.setError(null);
                }
                else
                {
                    Log.v(TAG, "Sin registros encontrados en localidades etColonia.setOnItemClickListener()");
                }

            }
        });

        etColonia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etColonia.showDropDown();
                busquedaGoogleCasa = false;
                etColonia.setText("");
                textInputLocalidad.setVisibility(View.GONE);
            }
        });
        etColoniaCobranza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etColoniaCobranza.showDropDown();
                busquedaGoogleCobranza = false;
                etOpcionalLocalidadCobranza.setText("");
                textInputLocalidadCobranza.setVisibility(View.GONE);
            }
        });



        etColoniaCobranza.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                coloniaSeleccionadaCobranza = adapterView.getItemAtPosition(i).toString();

                busquedaGoogleCobranza = false;
                etOpcionalLocalidadCobranza.setText("");
                textInputLocalidadCobranza.setVisibility(View.GONE);
                spLocalidadCobranza.setVisibility(View.VISIBLE);
                ArrayList<String> LOCALIDADES = new ArrayList<String>();
                LOCALIDADES.add("Selecciona una localidad");
                String query="SELECT * FROM LocalidadColonia where colonia = '" + coloniaSeleccionadaCobranza + "'group by localidad"; //buscamos y agrupamos ciudades
                List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
                if(listaLocalidades.size()>0)
                {
                    if(listaLocalidades.size()==1)
                    {
                        soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza [0] = true;
                        LOCALIDADES.clear();
                        LOCALIDADES.add(listaLocalidades.get(0).getLocalidad());
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(CapturaSolicitud.this, android.R.layout.simple_list_item_1, LOCALIDADES);
                        spLocalidadCobranza.setAdapter(adapterLocalidades);
                    }
                    else {
                        soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza[0] = false;
                        for (int t = 0; t < listaLocalidades.size(); t++) {
                            try {
                                LOCALIDADES.add(listaLocalidades.get(t).getLocalidad());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(CapturaSolicitud.this, android.R.layout.simple_list_item_1, LOCALIDADES);
                        spLocalidadCobranza.setAdapter(adapterLocalidades);
                    }

                    etColoniaCobranza.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.contorno));
                    etColoniaCobranza.setError(null);
                }
                else
                    Log.v(TAG, "Sin registros en localidades dentro de etColoniaCobranza.setOnItemClickListener()");
            }
        });

        //start region--------------------------------------------------------------------------------------------
        btCasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                nested_layout.smoothScrollTo(0,700);
                //Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                //btCasa.setBackground(fondoResource);
                //datosGuardadosDeDomicilioDeCasa =false;

                if(layoutDomicilioDeCasa.getVisibility()==View.GONE)
                {
                    lottieGoogle.playAnimation();
                    layoutDomicilioDeCasa.setFocusable(true);
                    layoutDomicilioDeCasa.requestFocus();
                    TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                    layoutDomicilioDeCasa.setVisibility(View.VISIBLE);
                    if(datosGuardadosDeDomicilioDeCasa)
                    {
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCasa.setCompoundDrawables( img, null, null, null );
                    }
                    else {
                        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                        img.setBounds(0, 0, 60, 60);
                        btCasa.setCompoundDrawables(img, null, null, null);
                    }
                }
                else
                {
                    lottieGoogle.pauseAnimation();
                    TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                    layoutDomicilioDeCasa.setVisibility(View.GONE);
                    if(datosGuardadosDeDomicilioDeCasa)
                    {
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCasa.setCompoundDrawables( img, null, null, null );
                    }
                    else {
                        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_abajo);
                        img.setBounds(0, 0, 60, 60);
                        btCasa.setCompoundDrawables(img, null, null, null);
                    }
                }
            }
        });
        etCalleNumero.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCasa.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCasa =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etNumeroExterior.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCasa.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCasa =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etNumeroInterior.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCasa.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCasa =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etCodigoPostal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCasa.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCasa =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etEntreCalles.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCasa.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCasa =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etColonia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCasa.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCasa =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        spLocalidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previous = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(previous != position && previous < -1) {
                    //Log.v("Example", "Item Selected: " + parent.getItemAtPosition(position).toString());
                    Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                    btCasa.setBackground(fondoResource);
                    Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                    img.setBounds(0, 0, 60, 60);
                    btCasa.setCompoundDrawables(img, null, null, null);
                    datosGuardadosDeDomicilioDeCasa =false;
                }
                previous = position;

                if(!parent.getSelectedItem().toString().equals("Selecciona una localidad") || spLocalidad.getSelectedItemPosition() != 0)
                    spLocalidad.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.contorno));

            }

            public void onNothingSelected(AdapterView<?> parent) {}
        });
        //----------------


        btCobranza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                //btCobranza.setBackground(fondoResource);
                //datosGuardadosDeDomicilioDeCobranza =false;
                nested_layout.smoothScrollTo(0,700);

                if(layoutDomicilioDeCobranza.getVisibility()==View.GONE)
                {
                    lottieGoogleCobranza.playAnimation();
                    layoutDomicilioDeCobranza.setFocusable(true);
                    layoutDomicilioDeCobranza.requestFocus();
                    TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                    layoutDomicilioDeCobranza.setVisibility(View.VISIBLE);

                    if(datosGuardadosDeDomicilioDeCobranza)
                    {
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCobranza.setCompoundDrawables( img, null, null, null );
                    }
                    else {
                        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                        img.setBounds(0, 0, 60, 60);
                        btCobranza.setCompoundDrawables(img, null, null, null);
                    }
                }
                else
                {
                    lottieGoogleCobranza.pauseAnimation();
                    TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                    layoutDomicilioDeCobranza.setVisibility(View.GONE);
                    if(datosGuardadosDeDomicilioDeCobranza)
                    {
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCobranza.setCompoundDrawables( img, null, null, null );
                    }
                    else {
                        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_abajo);
                        img.setBounds(0, 0, 60, 60);
                        btCobranza.setCompoundDrawables(img, null, null, null);
                    }
                }
            }
        });
        etCalleNumeroCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCobranza.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCobranza =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etNumeroExteriorCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCobranza.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCobranza =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etNumeroInteriorCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCobranza.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCobranza =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etCodigoPostalCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCobranza.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCobranza =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etEntreCallesCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCobranza.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCobranza =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        etColoniaCobranza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResource);
                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                img.setBounds(0, 0, 60, 60);
                btCobranza.setCompoundDrawables(img, null, null, null);
                datosGuardadosDeDomicilioDeCobranza =false;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        spLocalidadCobranza.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previous = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(previous != position && previous < -1) {
                    Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                    btCobranza.setBackground(fondoResource);
                    Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.flecha_arriba);
                    img.setBounds(0, 0, 60, 60);
                    btCobranza.setCompoundDrawables(img, null, null, null);
                    datosGuardadosDeDomicilioDeCobranza =false;
                }
                previous = position;
            }

            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //end region---------------------------------------------------------------------------------------------

        btGuardarCasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(busquedaGoogleCasa)
                {
                    //Aceptamos todos los datos que vienen de google maps
                    // buscamos cuales campos estan en blanco paa avisarle al usuario que faltan los campos en blanco
                    if(etCalleNumero.getText().length()==0 || etCalleNumero.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el nombre de la calle", Toast.LENGTH_LONG).show();
                        etCalleNumero.setError("Completa este campo");
                    }
                    else if(etNumeroExterior.getText().length()==0 || etNumeroExterior.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el número exterior", Toast.LENGTH_LONG).show();
                        etNumeroExterior.setError("Completa este campo");
                    }
                    else if(etCodigoPostal.getText().length()==0 || etCodigoPostal.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el código postal", Toast.LENGTH_LONG).show();
                        etCodigoPostal.setError("Completa este campo");
                    }
                    else if(etEntreCalles.getText().length()==0 || etEntreCalles.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa las entre calles del domicilio", Toast.LENGTH_LONG).show();
                        etEntreCalles.setError("Completa este campo");
                    }
                    else if(etOpcionalLocalidad.getText().toString().length() == 0 || etOpcionalLocalidad.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa la localidad", Toast.LENGTH_LONG).show();
                        etOpcionalLocalidad.setError("Completa este campo");
                    }
                    else if(etColonia.getText().toString().length()<=0 || etColonia.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa una colonia", Toast.LENGTH_LONG).show();
                        etColonia.setError("Completa este campo");
                    }
                    else
                    {
                        TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                        layoutDomicilioDeCasa.setVisibility(View.GONE);
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCasa.setCompoundDrawables( img, null, null, null );
                        Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validado);
                        btCasa.setBackground(fondoResource);
                        datosGuardadosDeDomicilioDeCasa = true;
                    }
                }
                else
                {
                    if(etCalleNumero.getText().length()==0 || etCalleNumero.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el nombre de la calle", Toast.LENGTH_LONG).show();
                        etCalleNumero.setError("Completa este campo");
                    }
                    else if(etNumeroExterior.getText().length()==0 || etNumeroExterior.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el número exterior", Toast.LENGTH_LONG).show();
                        etNumeroExterior.setError("Completa este campo");
                    }
                    else if(etCodigoPostal.getText().length()==0 || etCodigoPostal.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el código postal", Toast.LENGTH_LONG).show();
                        etCodigoPostal.setError("Completa este campo");
                    }
                    else if(etEntreCalles.getText().length()==0 || etEntreCalles.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa las entre calles del domicilio", Toast.LENGTH_LONG).show();
                        etEntreCalles.setError("Completa este campo");
                    }
                    else if(spLocalidad.getSelectedItemPosition() == 0 && !soloHayUnaPosicionDeLocalidaDeDomicilioDeCasa[0])
                    {
                        Toast.makeText(CapturaSolicitud.this, "Debes seleccionar una localidad de la lista", Toast.LENGTH_LONG).show();
                        spLocalidad.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style));
                    }
                    else if(etColonia.getText().toString().length()<=0 || etColonia.getText().toString().equals("") || coloniaSelecionada == null || coloniaSelecionada == "")
                    {
                        Toast.makeText(CapturaSolicitud.this, "Debes seleccionar una colonia de la lista.", Toast.LENGTH_LONG).show();
                        etColonia.setError("Completa este campo");
                    }
                    else
                    {
                        TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                        layoutDomicilioDeCasa.setVisibility(View.GONE);
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCasa.setCompoundDrawables( img, null, null, null );
                        Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validado);
                        btCasa.setBackground(fondoResource);
                        datosGuardadosDeDomicilioDeCasa = true;
                    }
                }


            }
        });

        btGuardarCobranza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(busquedaGoogleCobranza)
                {
                    //Aceptamos todos los datos que vienen de google maps
                    // buscamos cuales campos estan en blanco paa avisarle al usuario que faltan los campos en blanco
                    if(etCalleNumeroCobranza.getText().length()==0 || etCalleNumeroCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el nombre de la calle", Toast.LENGTH_LONG).show();
                        etCalleNumeroCobranza.setError("Completa este campo");
                    }
                    else if(etNumeroExteriorCobranza.getText().length()==0 || etNumeroExteriorCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el número exterior", Toast.LENGTH_LONG).show();
                        etNumeroExteriorCobranza.setError("Completa este campo");
                    }
                    else if(etCodigoPostalCobranza.getText().length()==0 || etCodigoPostalCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el código postal", Toast.LENGTH_LONG).show();
                        etCodigoPostalCobranza.setError("Completa este campo");
                    }
                    else if(etEntreCallesCobranza.getText().length()==0 || etEntreCallesCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa las entre calles del domicilio", Toast.LENGTH_LONG).show();
                        etEntreCallesCobranza.setError("Completa este campo");
                    }
                    else if(etColoniaCobranza.getText().toString().length() == 0 || etColoniaCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa una colonia", Toast.LENGTH_LONG).show();
                        etColoniaCobranza.setError("Completa este campo");
                    }
                    else if(etOpcionalLocalidadCobranza.getText().toString().length() == 0 || etOpcionalLocalidadCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa una localidad", Toast.LENGTH_LONG).show();
                        etOpcionalLocalidadCobranza.setError("Completa este campo");
                    }
                    else
                    {
                        TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                        layoutDomicilioDeCobranza.setVisibility(View.GONE);
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCobranza.setCompoundDrawables( img, null, null, null );
                        Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validado);
                        btCobranza.setBackground(fondoResource);
                        datosGuardadosDeDomicilioDeCobranza = true;
                    }
                }
                else
                {
                    if(etCalleNumeroCobranza.getText().length()==0 || etCalleNumeroCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el nombre de la calle", Toast.LENGTH_LONG).show();
                        etCalleNumeroCobranza.setError("Completa este campo");
                    }
                    else if(etNumeroExteriorCobranza.getText().length()==0 || etNumeroExteriorCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el número exterior", Toast.LENGTH_LONG).show();
                        etNumeroExteriorCobranza.setError("Completa este campo");
                    }
                    else if(etCodigoPostalCobranza.getText().length()==0 || etCodigoPostalCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa el código postal", Toast.LENGTH_LONG).show();
                        etCodigoPostalCobranza.setError("Completa este campo");
                    }
                    else if(etEntreCallesCobranza.getText().length()==0 || etEntreCallesCobranza.getText().toString().equals(""))
                    {
                        Toast.makeText(CapturaSolicitud.this, "Ingresa las entre calles del domicilio", Toast.LENGTH_LONG).show();
                        etEntreCallesCobranza.setError("Completa este campo");
                    }
                    else if(etColoniaCobranza.getText().toString().length()<=0 || coloniaSeleccionadaCobranza == null && !estaBanderaVieneDesdeElCheckBox)
                    {
                        Toast.makeText(CapturaSolicitud.this, "Debes seleccionar una colonia de la lista.", Toast.LENGTH_LONG).show();
                        etColoniaCobranza.setError("Completa este campo");
                    }
                    else if(spLocalidadCobranza.getSelectedItemPosition() == 0 && !soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza[0] && estaBanderaVieneDesdeElCheckBox)
                    {
                        Toast.makeText(CapturaSolicitud.this, "Debes seleccionar una localidad de la lista", Toast.LENGTH_LONG).show();
                        spLocalidadCobranza.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_style));
                    }
                    else
                    {
                        TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                        layoutDomicilioDeCobranza.setVisibility(View.GONE);
                        Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.donne);
                        img.setBounds( 0, 0, 60, 60 );
                        btCobranza.setCompoundDrawables( img, null, null, null );
                        Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validado);
                        btCobranza.setBackground(fondoResource);
                        datosGuardadosDeDomicilioDeCobranza = true;
                    }
                }
            }
        });

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bt100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etInversionInicial.setText("100");
            }
        });

        bt200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etInversionInicial.setText("200");
            }
        });

        bt300.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etInversionInicial.setText("300");
            }
        });

        bt400.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etInversionInicial.setText("400");
            }
        });

        bt500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etInversionInicial.setText("500");
            }
        });

        btBuscarConGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btBuscarConGoogle.setTag("onClick");
                Places.initialize(getApplicationContext(), "AIzaSyCZ_BFON24TWbKRPh31vdZzIvPRkFyqN6I");
                List<com.google.android.libraries.places.api.model.Place.Field> fieldList = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ADDRESS,
                        com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.NAME);

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldList).build(CapturaSolicitud.this);
                startActivityForResult(intent, 100);


            }
        });

        btBuscarConGoogleCobranza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btBuscarConGoogleCobranza.setTag("onClick");
                Places.initialize(getApplicationContext(), "AIzaSyCZ_BFON24TWbKRPh31vdZzIvPRkFyqN6I");
                List<com.google.android.libraries.places.api.model.Place.Field> fieldList = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ADDRESS,
                        com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.NAME);

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldList).build(CapturaSolicitud.this);
                startActivityForResult(intent, 101);


            }
        });



        btBorrarTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                etNombres.setText("");
                etApellidoPaterno.setText("");
                etApellidoMaterno.setText("");
                etCalleNumero.setText("");
                etColonia.setText("");
                etCodigoPostal.setText("");
                etEntreCalles.setText("");
                etFechaNac.setText("");
                etTelefono.setText("");
                etObservaciones.setText("");
                etNombres.setText("");
                etInversionInicial.setText("");
                etNumeroExterior.setText("");
                etNumeroInterior.setText("");
                etOpcionalLocalidad.setText("");
                etOpcionalLocalidadCobranza.setText("");
                etNombres.setFocusable(true);
                etCalleNumeroCobranza.setText("");
                etNumeroExteriorCobranza.setText("");
                etNumeroInteriorCobranza.setText("");
                etColoniaCobranza.setText("");
                etCodigoPostalCobranza.setText("");
                etEntreCallesCobranza.setText("");

                Drawable fondoResource = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCasa.setBackground(fondoResource);
                datosGuardadosDeDomicilioDeCasa =false;

                Drawable fondoResourceCobranza = getApplicationContext().getResources().getDrawable(R.drawable.button_validar);
                btCobranza.setBackground(fondoResourceCobranza);
                datosGuardadosDeDomicilioDeCobranza =false;

                TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                layoutDomicilioDeCobranza.setVisibility(View.GONE);
                Drawable img1 = getApplicationContext().getResources().getDrawable( R.drawable.flecha_abajo);
                img1.setBounds( 0, 0, 60, 60 );
                btCobranza.setCompoundDrawables( img1, null, null, null );

                TransitionManager.beginDelayedTransition(layoutDatos, new AutoTransition());
                layoutDomicilioDeCasa.setVisibility(View.GONE);
                Drawable img = getApplicationContext().getResources().getDrawable( R.drawable.flecha_abajo);
                img.setBounds( 0, 0, 60, 60 );
                btCasa.setCompoundDrawables( img, null, null, null );

                busquedaGoogleCasa = false;
                busquedaGoogleCobranza = false;
                coloniaSelecionada=null;
                coloniaSeleccionadaCobranza=null;
                soloHayUnaPosicionDeLocalidaDeDomicilioDeCasa[0] = false;
                soloHayUnaPosicionDeLocalidaDeDomicilioDeCobranza[0] = false;
                estaBanderaVieneDesdeElCheckBox = false;
                spLocalidad.setVisibility(View.GONE);
                spLocalidadCobranza.setVisibility(View.GONE);
                cbCopiarDomicilioDeCasa.setChecked(false);
            }
        });

        btContinuar.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    //countDownTimer.cancel();
                }
                else if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    try
                    {
                        if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                        {
                            final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                            final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                            if(isGPSEnabled)
                            {
                                if (getLocationMode() == 3)
                                {
                                    if (etNumeroSolicitud.getText().toString().length() <= 0 || etNumeroSolicitud.getText().toString().equals("")) {
                                        Snackbar.make(v, "Completa los campos", Snackbar.LENGTH_SHORT).show();

                                    } else if (etNombres.getText().toString().length() <= 0 || etNombres.getText().toString().equals("")) {
                                        Snackbar.make(v, "Completa los campos", Snackbar.LENGTH_SHORT).show();

                                    } else if (etApellidoPaterno.getText().toString().length() <= 0 || etApellidoPaterno.getText().toString().equals("")) {
                                        Snackbar.make(v, "Completa los campos", Snackbar.LENGTH_SHORT).show();

                                    } else if (etApellidoMaterno.getText().toString().length() <= 0 || etApellidoMaterno.getText().toString().equals("")) {
                                        etApellidoMaterno.setText("X");

                                    } else if (etFechaNac.getText().toString().length() <= 0 || etFechaNac.getText().toString().equals("")) {
                                        Snackbar.make(v, "Completa los campos", Snackbar.LENGTH_SHORT).show();

                                    } else if (spEstadoCivil.getSelectedItemPosition() == 0) {
                                        Toast.makeText(CapturaSolicitud.this, "Selecciona el estado civil", Toast.LENGTH_LONG).show();

                                    } else if (etTelefono.getText().toString().length() <= 7 || etTelefono.getText().toString().equals("") || etTelefono.getText().toString().length() == 9 || etTelefono.getText().toString().length() > 10) {
                                        Snackbar.make(v, "El teléfono tiene que contener 10 digitos", Snackbar.LENGTH_SHORT).show();

                                    } else if (!datosGuardadosDeDomicilioDeCasa) {
                                        Snackbar.make(v, "Por favor completa los datos de domicilio de casa", Snackbar.LENGTH_LONG).show();

                                    } else if (!datosGuardadosDeDomicilioDeCobranza) {
                                        Snackbar.make(v, "Por favor completa los datos de domicilio de cobranza", Snackbar.LENGTH_LONG).show();

                                    } else if (etInversionInicial.getText().toString().length() <= 0 || etInversionInicial.getText().toString().equals("")) {
                                        Snackbar.make(v, "Completa los campos", Snackbar.LENGTH_SHORT).show();

                                    } else if (etObservaciones.getText().toString().length() <= 0 || etObservaciones.getText().toString().equals("")) {
                                        etObservaciones.setText(" ");

                                    } else {
                                        if (verificarInternet()) {
                                            if (!findNumero || numeroSolicitud == "" || imgEscanear.getVisibility() == View.GONE) {
                                                Toast.makeText(CapturaSolicitud.this, "Número de solicitud vacía o no reconocida", Toast.LENGTH_LONG).show();
                                            } else {
                                                if (spEsquemaDePago.getSelectedItemPosition() < 1 && BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA) {
                                                    Snackbar.make(v, "Selecciona un esquema de pago", Snackbar.LENGTH_LONG).show();
                                                } else {

                                                    Intent intent = new Intent(CapturaSolicitud.this, ComprobantesFotografias.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                    intent.putExtra("nombre", etNombres.getText().toString());
                                                    intent.putExtra("paterno", etApellidoPaterno.getText().toString());
                                                    intent.putExtra("materno", etApellidoMaterno.getText().toString());
                                                    intent.putExtra("calle", etCalleNumero.getText().toString());
                                                    intent.putExtra("estadoCivil", spEstadoCivil.getSelectedItem().toString());
                                                    intent.putExtra("numeroExterior", etNumeroExterior.getText().toString());
                                                    intent.putExtra("numeroInterior", etNumeroInterior.getText().toString());

                                                    intent.putExtra("colonia", busquedaGoogleCasa ? etColonia.getText().toString() :  coloniaSelecionada);
                                                    intent.putExtra("ciudad",  busquedaGoogleCasa ? etOpcionalLocalidad.getText().toString() : spLocalidad.getSelectedItem().toString());

                                                    intent.putExtra("codigoPostal", etCodigoPostal.getText().toString());
                                                    intent.putExtra("fechaNacimiento", etFechaNac.getText().toString());
                                                    intent.putExtra("telefono", etTelefono.getText().toString());
                                                    intent.putExtra("observaciones", etObservaciones.getText().toString());
                                                    intent.putExtra("nuevoIngreso", etNuevoIngreso.getText().toString());
                                                    intent.putExtra("opcionSP", opcionSeleccionadaIDSpinner);
                                                    intent.putExtra("entreCalles", etEntreCalles.getText().toString());
                                                    intent.putExtra("codigoPromotor", codigoPromotor);
                                                    intent.putExtra("numeroSolicitud", numeroSolicitud);
                                                    intent.putExtra("tipoSolicitud", tipoSolicitud);
                                                    intent.putExtra("letraIndice", letraIndice);

                                                    //datos de cobranza-----------------
                                                    intent.putExtra("calleCobranza", etCalleNumeroCobranza.getText().toString());
                                                    intent.putExtra("exteriorCobranza", etNumeroExteriorCobranza.getText().toString());
                                                    intent.putExtra("interiorCobranza", etNumeroInteriorCobranza.getText().toString());
                                                    intent.putExtra("coloniaCobranza", etColoniaCobranza.getText().toString().length()>0 ? etColoniaCobranza.getText().toString() : coloniaSeleccionadaCobranza );
                                                    intent.putExtra("localidadCobranza", etOpcionalLocalidadCobranza.getText().toString().length() > 0 ? etOpcionalLocalidadCobranza.getText().toString() : spLocalidadCobranza.getSelectedItem().toString());
                                                    intent.putExtra("postalCobranza", etCodigoPostalCobranza.getText().toString());
                                                    intent.putExtra("entreCallesCobranza", etEntreCallesCobranza.getText().toString());
                                                    //----------------------------------
                                                    intent.putExtra("inversion", etInversionInicial.getText().toString());
                                                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                                                }
                                            }

                                        } else {


                                            if (validarDireccionesOffline(spLocalidad.getSelectedItem().toString(), etColonia.getText().toString())
                                                    && validarDireccionesOffline(spLocalidadCobranza.getSelectedItem().toString(), etColoniaCobranza.getText().toString())) {
                                                if (!findNumero || numeroSolicitud == "" || imgEscanear.getVisibility() == View.GONE) {
                                                    Toast.makeText(CapturaSolicitud.this, "Número de solicitud vacía o no reconocida", Toast.LENGTH_LONG).show();
                                                } else {
                                                    if (spEsquemaDePago.getSelectedItemPosition() < 1 && BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA) {
                                                        Snackbar.make(v, "Selecciona un esquema de pago", Snackbar.LENGTH_LONG).show();
                                                    } else {

                                                        Intent intent = new Intent(CapturaSolicitud.this, ComprobantesFotografias.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                        intent.putExtra("nombre", etNombres.getText().toString());
                                                        intent.putExtra("paterno", etApellidoPaterno.getText().toString());
                                                        intent.putExtra("materno", etApellidoMaterno.getText().toString());
                                                        intent.putExtra("calle", etCalleNumero.getText().toString());
                                                        intent.putExtra("estadoCivil", spEstadoCivil.getSelectedItem().toString());
                                                        intent.putExtra("numeroExterior", etNumeroExterior.getText().toString());
                                                        intent.putExtra("numeroInterior", etNumeroInterior.getText().toString());

                                                        //intent.putExtra("colonia", coloniaSelecionada);
                                                        //intent.putExtra("ciudad", spLocalidad.getSelectedItem().toString());
                                                        intent.putExtra("colonia", busquedaGoogleCasa ? etColonia.getText().toString() :  coloniaSelecionada);
                                                        intent.putExtra("ciudad",  busquedaGoogleCasa ? etOpcionalLocalidad.getText().toString() : spLocalidad.getSelectedItem().toString());


                                                        intent.putExtra("codigoPostal", etCodigoPostal.getText().toString());
                                                        intent.putExtra("fechaNacimiento", etFechaNac.getText().toString());
                                                        intent.putExtra("telefono", etTelefono.getText().toString());
                                                        intent.putExtra("observaciones", etObservaciones.getText().toString());
                                                        intent.putExtra("nuevoIngreso", etNuevoIngreso.getText().toString());
                                                        intent.putExtra("opcionSP", opcionSeleccionadaIDSpinner);
                                                        intent.putExtra("entreCalles", etEntreCalles.getText().toString());
                                                        intent.putExtra("codigoPromotor", codigoPromotor);
                                                        intent.putExtra("numeroSolicitud", numeroSolicitud);
                                                        intent.putExtra("tipoSolicitud", tipoSolicitud);
                                                        intent.putExtra("letraIndice", letraIndice);
                                                        //datos de cobranza-----------------
                                                        intent.putExtra("calleCobranza", etCalleNumeroCobranza.getText().toString());
                                                        intent.putExtra("exteriorCobranza", etNumeroExteriorCobranza.getText().toString());
                                                        intent.putExtra("interiorCobranza", etNumeroInteriorCobranza.getText().toString());
                                                        intent.putExtra("coloniaCobranza",  etColoniaCobranza.getText().toString().length() > 0 ? etColoniaCobranza.getText().toString() : coloniaSeleccionadaCobranza );
                                                        intent.putExtra("localidadCobranza", etOpcionalLocalidadCobranza.getText().toString().length() > 0 ? etOpcionalLocalidadCobranza.getText().toString() : spLocalidadCobranza.getSelectedItem().toString());

                                                        intent.putExtra("postalCobranza", etCodigoPostalCobranza.getText().toString());
                                                        intent.putExtra("entreCallesCobranza", etEntreCallesCobranza.getText().toString());
                                                        //----------------------------------
                                                        intent.putExtra("inversion", etInversionInicial.getText().toString());
                                                        startActivityForResult(intent, PICK_CONTACT_REQUEST);
                                                    }
                                                }

                                            }
                                        }

                                    }

                                }
                                else
                                {
                                    final AlertDialog.Builder dialog = getBuilder();
                                    try {
                                        dialog.setCancelable(false);
                                        dialog.setTitle("Advertencia");
                                        dialog.setMessage("Necesitas activar el modo GPS ALTA PRESICIÓN para continuar.");
                                        dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                                        {
                                            if (getLocationMode() != 3)
                                                dialog.create().show();
                                        });
                                        dialog.create().show();

                                    } catch (Exception e) {
                                        Toast.makeText(getApplicationContext(), "Advertencia: Enciende el GPS para continuar y verifica el modo ALTA PRECISIÓN.", Toast.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            }
                            else
                                Toast.makeText(getApplicationContext(), "Verifica tu GPS", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            final AlertDialog.Builder dialog = getBuilder();
                            try {
                                dialog.setCancelable(false);
                                dialog.setTitle("Advertencia");
                                dialog.setMessage("Parece que no tienes el GPS Activado");
                                dialog.create().show();

                            } catch (Exception e) {
                                Toast.makeText(CapturaSolicitud.this, "Verifica tu GPS", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }

                    }catch (NullPointerException ex)
                    {
                        Log.v(TAG, "Location Fail: " + ex.getMessage());
                        Toast.makeText(getApplicationContext(), "No podemos verificar el GPS, reinicia el GPS", Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            }
        });

        imgCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                permissionManager.checkAndRequestPermissions(CapturaSolicitud.this);
                if(verificarGPS())
                {
                    Intent intent = new Intent(CapturaSolicitud.this, ScannerBarras.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("codigoPromotor", codigoPromotor);
                    intent.putExtra("preguntarPorLasUsadas", homeKey);
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                    finish();
                }
                else
                    Toast.makeText(CapturaSolicitud.this, "Tu GPS esta desactivado", Toast.LENGTH_SHORT).show();
            }
        });

        etNumeroSolicitud.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

                if(etNumeroSolicitud.getText().toString().length()==0)
                {
                    imgCamara.setVisibility(View.VISIBLE);
                    btValidar.setVisibility(View.GONE);
                    imgEscanear.setVisibility(View.GONE);
                }
                else {
                    if (etNumeroSolicitud.getText().toString().length() != 12) {
                        imgCamara.setVisibility(View.GONE);
                        imgEscanear.setVisibility(View.GONE);
                        btValidar.setVisibility(View.VISIBLE);
                    } else {
                        if (etNumeroSolicitud.getText().toString().length() > 0) {
                            imgCamara.setVisibility(View.GONE);
                            imgEscanear.setVisibility(View.GONE);
                            btValidar.setVisibility(View.VISIBLE);
                        } else {
                            imgCamara.setVisibility(View.VISIBLE);
                            layoutDatos.setVisibility(View.GONE);
                            imgEscanear.setVisibility(View.GONE);
                            btValidar.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });

        imgCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar= Calendar.getInstance();
                dia=calendar.get(Calendar.DAY_OF_MONTH);
                mes=calendar.get(Calendar.MONTH);
                ano=calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog= new DatePickerDialog(CapturaSolicitud.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int año, int mes, int dia)
                    {
                        etFechaNac.setVisibility(View.VISIBLE);
                        etFechaNac.setText( (dia < 10 ? "0"+dia : dia) + "-" + ((mes+1) < 10 ? "0"+(mes+1) : (mes+1)) + "-" + año );
                        etFechaNac.setEnabled(false);
                    }
                },ano,mes,dia);
                datePickerDialog.show();
            }
        });

        btValidar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view)
            {
                permissionManager.checkAndRequestPermissions(CapturaSolicitud.this);
                if(verificarGPS())
                {
                    pDialog=new ProgressDialog(CapturaSolicitud.this);
                    pDialog.setMessage("Validando código de solicitud...");
                    pDialog.setCancelable(false);
                    pDialog.show();

                    if(homeKey)
                    {
                        // hacer la validacion para saber si la solicitud tiene fotos guardadas
                        String queryy = "SELECT * FROM FOTOGRAFIAS_POR_SOLICITUD_DE_CLIENTE WHERE nosolicitud= '" + etNumeroSolicitud.getText().toString() + "'";
                        List<FotografiasPorSolicitudDeCliente> listFotos = FotografiasPorSolicitudDeCliente.findWithQuery(FotografiasPorSolicitudDeCliente.class, queryy);
                        if (listFotos.size() > 0) {
                            Intent intent = new Intent(CapturaSolicitud.this, ComprobantesFotografias.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("reCapturaDeFotos", homeKey);
                            intent.putExtra("codigoPromotor", codigoPromotor);
                            intent.putExtra("numeroSolicitud", listFotos.get(0).nosolicitud);
                            intent.putExtra("tipoSolicitud", tipoSolicitud);
                            intent.putExtra("codigo_add_photo", listFotos.get(0).getCodigoactivacion());
                            startActivityForResult(intent, PICK_CONTACT_REQUEST);
                            pDialog.dismiss();

                        } else {
                            pDialog.dismiss();
                            dialogo = new AlertDialog.Builder(CapturaSolicitud.this);
                            dialogo.setTitle("Información");
                            dialogo.setMessage("Parece que esta solicitud aún no tiene ningun registro, por favor ve a la página principal y selecciona **Nueva solicitud**");
                            dialogo.setCancelable(false);
                            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            });
                            dialogo.show();
                        }

                    }
                    else
                    {
                        String query="SELECT * FROM SOLICITUD WHERE solSerie= '" + etNumeroSolicitud.getText().toString() + "' and usada = '0'";
                        List<Solicitud> notes = Solicitud.findWithQuery(Solicitud.class, query);
                        if(notes.size()>0)
                        {
                            btBorrarTodos.performClick();
                            pDialog.dismiss();
                            numeroSolicitud = notes.get(0).sol_Serie;
                            tipoSolicitud = notes.get(0).getTipoSolicitud();
                            letraIndice =  notes.get(0).getLetraSolicitud();
                            btValidar.setVisibility(View.GONE);
                            imgEscanear.setVisibility(View.VISIBLE);
                            solicitudEncontrada=true;
                            etNumeroSolicitud.setEnabled(false);
                            findNumero=true;
                            layoutDatos.setVisibility(View.VISIBLE);
                            tv1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
                            tv2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
                            tv3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style));
                            tv4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style));
                        }
                        else
                        {
                            tv1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
                            tv2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style));
                            tv3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style));
                            tv4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style));
                            numeroSolicitud ="";
                            pDialog.dismiss();
                            dialogo=new AlertDialog.Builder(CapturaSolicitud.this);
                            dialogo.setTitle("Información");
                            dialogo.setMessage("Número de solicitud no encontrado, o ya fue usado anteriormente");
                            dialogo.setCancelable(false);
                            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            dialogo.show();
                        }
                    }
                }
                else
                {
                    Toast.makeText(CapturaSolicitud.this, "GPS desactivado", Toast.LENGTH_SHORT).show();
                }


            }
        });
        spEsquemaDePago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(BuildConfig.getTargetBranch() == BuildConfig.Branches.CANCUN)
                    opcionSeleccionadaIDSpinner = "-";
                else
                    opcionSeleccionadaIDSpinner = String.valueOf(spEsquemaDePago.getItemIdAtPosition(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(!consultaDeFotosGuardadas)
            validacionPerformClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            Uri path = data.getData();
        }

        if (requestCode == 100 && resultCode == RESULT_OK) //es casa
        {
            try {
                boolean validarposicion3 = false, validarColonia = false;
                String nombre = "", numAux = "", numero = "", calle = "", colonia = "", codigo_postal = "", codigo_postal_con_ciudad = "", ciudad = "";
                Place place = Autocomplete.getPlaceFromIntent(data);
                nombre = place.getAddress().split(",")[0];

                for (int i = nombre.length() - 1; i >= 0; i--) {
                    if (nombre.charAt(i) == ' ')
                        break;
                    else {
                        if (nombre.split(",")[0].split(" ").length >= 3) {
                            // Quiere decir que la calle se llama calle 2 y tiene numero Ejemplo: Calle 28
                            if (Character.isDigit(nombre.charAt(i))) {
                                numAux = numAux + nombre.charAt(i);
                            } else {
                                Log.v("numero", "No tiene numero la calle");
                                break;
                            }
                        } else {
                            try {
                                calle = place.getAddress().split(",")[0].split(" ")[0] + " " + place.getAddress().split(",")[0].split(" ")[1];
                            } catch (Throwable e) {
                                Log.v("numero", "No tiene posicion 1 el split de la posicion 0 de place");
                                calle = place.getAddress().split(",")[0].split(" ")[0];
                            }
                            numAux = "";
                            numero = "";
                            break;
                        }

                    }
                }

                if (numAux.length() > 0) {
                    int position = nombre.length() - numAux.length();
                    calle = nombre.substring(0, position);
                    numero = place.getAddress().split(",")[0].substring(position, nombre.length());
                } else {
                    try {
                        calle = place.getAddress().split(",")[0];
                    } catch (Throwable e) {
                        Log.v("numero", "No tiene posicion 1 el split de la posicion 0 de place");
                        calle = place.getAddress().split(",")[0].split(" ")[0];
                    }
                    numero = "";
                }


                //*********************** proceso para obtener la colonia *******************
                String posicion1 = place.getAddress().split(",")[1].trim();
                String posicion2 = "";
                String posicion3 = "";
                int contador = 0;


                for (int i = 0; i <= posicion1.length() - 1; i++) {
                    if (Character.isDigit(posicion1.charAt(i)))
                        contador += 1;
                    else
                        break;
                }


                if (contador == 5) {
                    colonia = "";
                    Log.v("numero", "No tiene colonia, se encuentra en la posicion 1");

                    //Obtener cp aqui
                    for (int i = 0; i <= posicion1.length() - 1; i++) {
                        if (posicion1.charAt(i) == ' ')
                            break;
                        else {
                            if (Character.isDigit(posicion1.charAt(i))) {
                                codigo_postal = codigo_postal + posicion1.charAt(i);
                            } else {
                                Log.v(TAG, "No tiene codigo postal la posicion 2 de split");
                                codigo_postal = "";
                                ciudad = posicion1;
                                break;
                            }
                        }
                    }
                } else {
                    if (place.getAddress().split(",").length > 3) //Av. Patria, Jalisco, Mexico tiene mas de 3 posiciones?
                        colonia = posicion1;
                    else {
                        colonia = "";
                        Log.v(TAG, "No tiene posicion de colonia, en la posicion esta: " + place.getAddress().split(",")[1]);
                    }
                }


                //****************************************************************************

                if (codigo_postal.length() <= 0) {
                    if (place.getAddress().split(",").length >= 3) //el arreglo de splits tiene la posicion 3? ---> 0, 1, 2
                    {
                        posicion2 = place.getAddress().split(",")[2].trim();
                        for (int i = 0; i <= posicion2.length() - 1; i++) {
                            if (posicion2.charAt(i) == ' ')
                                break;
                            else {
                                if (Character.isDigit(posicion2.charAt(i)))
                                    codigo_postal = codigo_postal + posicion2.charAt(i);
                                else {
                                    Log.v(TAG, "No tiene codigo postal la posicion 2 de split");
                                    codigo_postal = "";
                                    ciudad = posicion2;
                                    break;
                                }
                            }
                        }
                    } else {
                        codigo_postal = "";
                        ciudad = "";
                    }

                    if (codigo_postal.length() > 0) {
                        //posicion 2 si tiene el cp y a ciudad
                        ciudad = posicion2.substring(codigo_postal.length(), posicion2.length()).trim();

                    } else {
                        codigo_postal = "";
                        ciudad = posicion2;
                        validarposicion3 = true;
                    }


                    if (validarposicion3) {
                        if (place.getAddress().split(",").length >= 4) //el arreglo de splits tiene la posicion 3? ---> 0, 1, 2, 3
                        {
                            posicion3 = place.getAddress().split(",")[3].trim(); //Corresponde a cp y ciudad

                            for (int i = 0; i <= posicion3.length() - 1; i++) {
                                if (posicion3.charAt(i) == ' ')
                                    break;
                                else {
                                    if (Character.isDigit(posicion3.charAt(i)))
                                        codigo_postal = codigo_postal + posicion3.charAt(i);
                                    else {
                                        Log.v(TAG, "No tiene codigo postal la posicion 2 de split");
                                        codigo_postal = "";
                                        break;
                                    }
                                }
                            }
                        } else {
                            codigo_postal = "";
                            ciudad = "";
                        }

                        if (codigo_postal.length() > 0) {
                            //posicion 2 si tiene el cp y a ciudad
                            ciudad = posicion3.substring(codigo_postal.length(), posicion3.length()).trim();

                        } else {
                            codigo_postal = "";
                        }
                    } else {
                        Log.v(TAG, "No se valida lal posicion 3, ya tenemos el codigo postal y la ciudad");
                    }

                } else {
                    if (contador == 5) {
                        ciudad = posicion1.substring(codigo_postal.length(), posicion1.length()).trim();

                    } else {
                        codigo_postal = "";
                        ciudad = posicion1;
                        validarposicion3 = true;
                    }
                }

                Log.v(TAG, "Calle: " + calle + "           Numero: " + numero + "          Colonia: " + colonia + "           CP: " + codigo_postal + "           Ciudad: " + ciudad);

                etCalleNumero.setText(calle.toUpperCase());
                etNumeroExterior.setText(numero.toUpperCase());
                etColonia.setText(limpiarAcentos(colonia).toUpperCase());
                etCodigoPostal.setText(codigo_postal.toUpperCase());
                etOpcionalLocalidad.setText(limpiarAcentos(ciudad).toUpperCase());
                busquedaGoogleCasa = true;
                spLocalidad.setVisibility(View.GONE);
                textInputLocalidad.setVisibility(View.VISIBLE);
            } catch (Throwable e) {
                Log.e(TAG, e.getMessage());
            }


        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            try {
                boolean validarposicion3 = false, validarColonia = false;
                String nombre = "", numAux = "", numero = "", calle = "", colonia = "", codigo_postal = "", codigo_postal_con_ciudad = "", ciudad = "";
                Place place = Autocomplete.getPlaceFromIntent(data);
                nombre = place.getAddress().split(",")[0];

                for (int i = nombre.length() - 1; i >= 0; i--) {
                    if (nombre.charAt(i) == ' ')
                        break;
                    else {
                        if (nombre.split(",")[0].split(" ").length >= 3) //el arreglo de splits tiene la posicion 3? ---> 0, 1, 2
                        {
                            // Quiere decir que la calle se llama calle 2 y tiene numero Ejemplo: Calle 28
                            if (Character.isDigit(nombre.charAt(i))) {
                                numAux = numAux + nombre.charAt(i);
                            } else {
                                Log.v(TAG, "No tiene numero la calle");
                                break;
                            }
                        } else {
                            try {
                                calle = place.getAddress().split(",")[0].split(" ")[0] + " " + place.getAddress().split(",")[0].split(" ")[1];
                            } catch (Throwable e) {
                                Log.v(TAG, "No tiene posicion 1 el split de la posicion 0 de place");
                                calle = place.getAddress().split(",")[0].split(" ")[0];
                            }
                            numAux = "";
                            numero = "";
                            break;
                        }

                    }
                }

                if (numAux.length() > 0) {
                    int position = nombre.length() - numAux.length();
                    calle = nombre.substring(0, position);
                    numero = place.getAddress().split(",")[0].substring(position, nombre.length());
                } else {
                    //calle = place.getAddress().split(",")[0];
                    try {
                        //for(int i=0; i<=place.getAddress().split(",")[0].split(" ").length-1;i++)
                        //calle = calle  + place.getAddress().split(",")[0].split(" ")[i];
                        calle = place.getAddress().split(",")[0];

                    } catch (Throwable e) {
                        Log.v("numero", "No tiene posicion 1 el split de la posicion 0 de place");
                        calle = place.getAddress().split(",")[0].split(" ")[0];
                    }
                    //calle = place.getAddress().split(",")[0].split(" ")[0] + " " + place.getAddress().split(",")[0].split(" ")[1];
                    //calle = place.getAddress().split(",")[0];
                    numero = "";
                }

                //*********************** proceso para obtener la colonia *******************
                String posicion1 = place.getAddress().split(",")[1].trim();
                String posicion2 = "";
                String posicion3 = "";
                int contador = 0;


                for (int i = 0; i <= posicion1.length() - 1; i++) {
                    if (Character.isDigit(posicion1.charAt(i)))
                        contador += 1;
                    else
                        break;
                }


                if (contador == 5) {
                    colonia = "";
                    Log.v(TAG, "No tiene colonia, se encuentra en la posicion 1");

                    //Obtener cp aqui
                    for (int i = 0; i <= posicion1.length() - 1; i++) {
                        if (posicion1.charAt(i) == ' ')
                            break;
                        else {
                            if (Character.isDigit(posicion1.charAt(i))) {
                                codigo_postal = codigo_postal + posicion1.charAt(i);
                            } else {
                                Log.v(TAG, "No tiene codigo postal la posicion 2 de split");
                                codigo_postal = "";
                                ciudad = posicion1;
                                break;
                            }
                        }
                    }
                } else {
                    if (place.getAddress().split(",").length > 3) //Av. Patria, Jalisco, Mexico tiene mas de 3 posiciones?
                        colonia = posicion1;
                    else {
                        colonia = "";
                        Log.v(TAG, "No tiene posicion de colonia, en la posicion esta: " + place.getAddress().split(",")[1]);
                    }
                }


                //****************************************************************************

                if (codigo_postal.length() <= 0) {
                    if (place.getAddress().split(",").length >= 3) //el arreglo de splits tiene la posicion 3? ---> 0, 1, 2
                    {
                        posicion2 = place.getAddress().split(",")[2].trim();
                        for (int i = 0; i <= posicion2.length() - 1; i++) {
                            if (posicion2.charAt(i) == ' ')
                                break;
                            else {
                                if (Character.isDigit(posicion2.charAt(i)))
                                    codigo_postal = codigo_postal + posicion2.charAt(i);
                                else {
                                    Log.v(TAG, "No tiene codigo postal la posicion 2 de split");
                                    codigo_postal = "";
                                    ciudad = posicion2;//hasta aqui
                                    break;
                                }
                            }
                        }
                    } else {
                        codigo_postal = "";
                        ciudad = "";
                    }

                    if (codigo_postal.length() > 0) {
                        //posicion 2 si tiene el cp y a ciudad
                        ciudad = posicion2.substring(codigo_postal.length(), posicion2.length()).trim();

                    } else {
                        codigo_postal = "";
                        ciudad = posicion2;
                        validarposicion3 = true;
                    }


                    if (validarposicion3) {
                        if (place.getAddress().split(",").length >= 4) //el arreglo de splits tiene la posicion 3? ---> 0, 1, 2, 3
                        {
                            posicion3 = place.getAddress().split(",")[3].trim(); //Corresponde a cp y ciudad

                            for (int i = 0; i <= posicion3.length() - 1; i++) {
                                if (posicion3.charAt(i) == ' ')
                                    break;
                                else {
                                    if (Character.isDigit(posicion3.charAt(i)))
                                        codigo_postal = codigo_postal + posicion3.charAt(i);
                                    else {
                                        Log.v("numero", "No tiene codigo postal la posicion 2 de split");
                                        codigo_postal = "";
                                        break;
                                    }
                                }
                            }
                        } else {
                            codigo_postal = "";
                            ciudad = "";
                        }

                        if (codigo_postal.length() > 0) {
                            //posicion 2 si tiene el cp y a ciudad
                            ciudad = posicion3.substring(codigo_postal.length(), posicion3.length()).trim();

                        } else {
                            codigo_postal = "";
                        }
                    } else {
                        Log.v("numero", "No se valida lal posicion 3, ya tenemos el codigo postal y la ciudad");
                    }

                } else {
                    if (contador == 5) {
                        ciudad = posicion1.substring(codigo_postal.length(), posicion1.length()).trim();

                    } else {
                        codigo_postal = "";
                        ciudad = posicion1;
                        validarposicion3 = true;
                    }
                }

                Log.v("numero", "Calle: " + calle + "           Numero: " + numero + "          Colonia: " + colonia + "           CP: " + codigo_postal + "           Ciudad: " + ciudad);

                etCalleNumeroCobranza.setText(calle.toUpperCase());
                etNumeroExteriorCobranza.setText(numero.toUpperCase());
                etColoniaCobranza.setText(limpiarAcentos(colonia).toUpperCase());
                etCodigoPostalCobranza.setText(codigo_postal.toUpperCase());
                etOpcionalLocalidadCobranza.setText(limpiarAcentos(ciudad).toUpperCase());
                busquedaGoogleCobranza = true;
                spLocalidadCobranza.setVisibility(View.GONE);
                textInputLocalidadCobranza.setVisibility(View.VISIBLE);
            } catch (Throwable e) {
                Log.e(TAG, "Error en onActivityResult(): " + e.getMessage());
            }

        } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
            Status status = Autocomplete.getStatusFromIntent(data);
            Log.e(TAG, "Error en onActivityResul() en resultCode()" + status.getStatusMessage());
        } else {
            try {
                if (!btBuscarConGoogle.getTag().equals("onClick") || !btBuscarConGoogleCobranza.getTag().equals("onClick"))
                    btBorrarTodos.performClick();
                Log.v(TAG, "NO borra las imagenes");
            } catch (Throwable e) {
                Log.e(TAG, "Error en catch dentro else onActivityResult(): " + e.toString());
            }
        }


    }

    private void validacionPerformClick() {
        if(barrasCorrecto.equals("1"))
        {
            imgCamara.setVisibility(View.GONE);
            imgEscanear.setVisibility(View.GONE);
            btValidar.setVisibility(View.VISIBLE);
            btValidar.setEnabled(true);
            btValidar.performClick();
        }
        else
            barrasCorrecto="0";
    }

    private void llenarSpinnerConEsquemasDePagoYLocalidades() {
        //-----------------------ESQUEMAS-----------------------
        esquemas = new ArrayList<String>();
        esquemas.add("Selecciona un esquema de pago");
        List<EsquemaPago> lista = EsquemaPago.listAll(EsquemaPago.class);
        if(lista.size()>0)
        {
            for (int i = 0; i < lista.size(); i++)
            {
                try
                {
                    esquemas.add(lista.get(i).getEsqPagDescripcion().toUpperCase());
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
            Log.v(TAG, "No tiene items la lista de esquemas de pago de la base de datos interna");

        //----------------------------------------------------


        CIUDADES = new ArrayList<String>();
        String query="SELECT * FROM LocalidadColonia group by localidad"; //buscamos y agrupamos ciudades
        List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if(listaLocalidades.size()>0)
        {
            for (int i = 0; i < listaLocalidades.size(); i++)
            {
                try
                {
                    CIUDADES.add(listaLocalidades.get(i).getLocalidad());
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
            Log.v(TAG, "No tiene datos de localidades en la base de datos");


    }


    public boolean verificarInternet() {
        ConnectivityManager con = (ConnectivityManager) CapturaSolicitud.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }





    public static String limpiarAcentos(String cadena) {
        String limpio =null;
        if (cadena !=null) {
            String valor = cadena;
            valor = valor.toUpperCase();
            limpio = Normalizer.normalize(valor, Normalizer.Form.NFD);
            limpio = limpio.replaceAll("[^\\p{ASCII}(N\u0303)(n\u0303)(\u00A1)(\u00BF)(\u00B0)(U\u0308)(u\u0308)]", "");
            limpio = Normalizer.normalize(limpio, Normalizer.Form.NFC);
        }
        return limpio;
    }

    /*public boolean validarDireccionesOnline(String city, String colonia)
    {
        boolean findLocalidades=false;
        String query="SELECT * FROM LocalidadColonia WHERE localidad = '" + city.toUpperCase() + "' and colonia = '" + limpiarAcentos(colonia) +"'";
        List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if(listaLocalidades.size()>0)
        {
            String localidadEncontrada="", coloniaEncontrada="";
            localidadEncontrada = listaLocalidades.get(0).getLocalidad();
            coloniaEncontrada = listaLocalidades.get(0).getColonia();
            if (city.contains(localidadEncontrada) && colonia.contains(coloniaEncontrada))
            {
                findLocalidades=true;
                Toast.makeText(CapturaSolicitud.this, "LOCALIDADES CORRECTAS", Toast.LENGTH_LONG).show();
            }
            else
            {
                dialogo=new AlertDialog.Builder(CapturaSolicitud.this);
                dialogo.setTitle("Información");
                dialogo.setMessage("Creémos que las localidades no son validas");
                dialogo.setCancelable(false);
                dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Nada

                    }
                });
                dialogo.show();
            }
        }
        else
        {
            Toast.makeText(CapturaSolicitud.this, "NO HAY REGISTROS", Toast.LENGTH_LONG).show();
        }

        return findLocalidades;
    }*/

    public boolean validarDireccionesOffline(String cuidad, String colonia)
    {
        String localidadEncontrada="", coloniaEncontrada="";
        boolean findLocalidades = false;
        String query="SELECT * FROM LocalidadColonia WHERE localidad = '" + cuidad.toUpperCase() + "' and colonia = '" + limpiarAcentos(colonia) +"'";
        List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if(listaLocalidades.size()>0)
        {

            localidadEncontrada = listaLocalidades.get(0).getLocalidad();
            coloniaEncontrada = listaLocalidades.get(0).getColonia();

            if (localidadEncontrada.contains(localidadEncontrada) && coloniaEncontrada.contains(coloniaEncontrada))
            {
                findLocalidades=true;
                Log.v(TAG, "Localidades correctas al validar las localidades");
            }
            else
            {
                Log.v(TAG, "Localidades no validas");
                dialogo=new AlertDialog.Builder(CapturaSolicitud.this);
                dialogo.setTitle("Información");
                dialogo.setMessage("Creémos que las localidades no son validas");
                dialogo.setCancelable(false);
                dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                dialogo.show();
            }
        }
        else
            Log.v(TAG, "No hay registros con la busqueda de localidades que se buscaron:     Localidad: " +  cuidad.toUpperCase() + "    colonia: " + colonia);

        return findLocalidades;
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    /*private void cerrarTeclado()
    {
        View view  = this.getCurrentFocus();
        if(view!=null)
        {
            InputMethodManager imm =(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }*/

    private ArrayList<String> llenarCamposDeColonias()
    {
        ArrayList<String> COLONIAS = new ArrayList<String>();
        COLONIAS = new ArrayList<String>();
        String query2="SELECT * FROM LocalidadColonia";
        List<Localidad_Colonia> listaLocalidades2 = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query2);
        if(listaLocalidades2.size()>0)
        {
            for (int i = 0; i < listaLocalidades2.size(); i++)
            {
                try
                {
                    COLONIAS.add(listaLocalidades2.get(i).getColonia());
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
            Log.v(TAG, "Sin registros de localidades al llenar la lista de colonias");

        return COLONIAS;
    }

    /*private ArrayList<String> llenarCamposDeLocalidades()
    {
        ArrayList<String> LOCALIDADES = new ArrayList<String>();
        COLONIAS = new ArrayList<String>();
        LOCALIDADES.add("Selecciona una localidad");
        String query="SELECT * FROM LocalidadColonia where colonia = '" + coloniaSelecionada + "'group by localidad"; //buscamos y agrupamos ciudades
        List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if(listaLocalidades.size()>0)
        {
            if(listaLocalidades.size()==1)
            {
                //soloHayUnaPosicion[0] = true;
                LOCALIDADES.clear();
                LOCALIDADES.add(listaLocalidades.get(0).getLocalidad());
                ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, LOCALIDADES);
                spLocalidad.setAdapter(adapterLocalidades);
            }
            else {
                //soloHayUnaPosicion[0] = false;
                for (int t = 0; t < listaLocalidades.size(); t++) {
                    try {
                        LOCALIDADES.add(listaLocalidades.get(t).getLocalidad());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, LOCALIDADES);
                spLocalidad.setAdapter(adapterLocalidades);
            }
        }
        else
        {
            Log.i("SIN REGISTROS", "SIN REGISTROS EN LOCALIDADES");
        }
        return LOCALIDADES;
    }*/

    public boolean verificarGPS()
    {
        boolean isEnabled= false;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Activa el GPS para continuar", Toast.LENGTH_SHORT).show();
            Log.v(TAG, "GPS desactivado, se tiene que activar el GPS");
            isEnabled= false;
        } else {
            final LocationManager locationManager = (LocationManager) CapturaSolicitud.this.getSystemService(Context.LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            try {
                //ApplicationResourcesProvider.startLocationUpdates();
                isEnabled = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return isEnabled;
    }


    public int getLocationMode() {
        try {
            return Settings.Secure.getInt(getApplicationContext().getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }


    private AlertDialog.Builder getBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            return new AlertDialog.Builder(this);
    }


    public void startService()
    {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STARTFOREGROUND_ACTION);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(serviceIntent);
    }

    @Override
    public final void onDestroy()
    {
        super.onDestroy();
        stopService();
    }

}


