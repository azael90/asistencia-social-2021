package com.example.sociales.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.ContentObservable;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.Image;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sociales.BuildConfig;
import com.example.sociales.DatabaseLocal.Archivo;
import com.example.sociales.DatabaseLocal.Cliente;
import com.example.sociales.DatabaseLocal.Control;
import com.example.sociales.DatabaseLocal.ExportImportDB;
import com.example.sociales.DatabaseLocal.Solicitud;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.Extras.BaseActivity;
import com.example.sociales.Extras.LogRegister;
import com.example.sociales.R;
import com.example.sociales.Service.Constants;
import com.example.sociales.Service.ForegroundService;
import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;
import com.google.android.gms.vision.text.Line;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pl.tajchert.nammu.Nammu;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.example.sociales.Extras.ApplicationResourcesProvider.getContext;

public class ComprobantesFotografias extends BaseActivity {

    static String TAG="COMPROBANTES_FOTOGRAFIAS";
    public static String NOMBRE_IMAGEN_PREFIJO=""; //url + codigoPromotor + seleccion + randomNumeros 999999
    Dialog myDialog;
    LottieAnimationView lottieNF, lottieCamera;
    CardView cardCodigo, cardCamera;
    ProgressDialog pDialog;
    ImageView btBack, imgDomicilio, imgComprobante, imgContrato, imgIdentificacion, img1, img2, img3, img4, imgFichaDeposito, img5;
    TextView tvSolicitudActual, tv1, tv2, tv3, tv4, t1, t2, t3, t4, t5;
    CardView cardDomicilio,cardComprobante, cardIdentificacion, cardContraro, cardFichaDeposito;
    Button btFinalizar, btCancelar;
    private BottomSheetDialog bottomSheetDialog;

    String tipoSolicitud="", letraIndice="", codigoDeActivacion="", inversionInicial="", codigo_activacion_recaptura="";
    String latitud="", longitud="", date="";
    boolean reCapturoImagenes = false;

    //---------
    Bitmap bitmap;
    int PICK_IMAGE_REQUEST = 1;
    String imageName;
    String codigoPromotor="";
    String numeroInterior="", numeroExterior;
    //-------
    public int indicadorCarta=0;
    //---------
    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";
    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;

    private String mPath;
    public File newdir;
    private String nombre="", paterno="",materno="",calle="",numeroSolicitud="", colonia="",ciudad="",codigoPostal="",fechaNacimiento="",
            telefono="",observaciones="",nuevoIngreso="",opcionSP="", entreCalles="", estadoCivil="";
    public ArrayList<String> listaImagenes = new ArrayList<>();
    public ArrayList<String> listaBitmaps =new ArrayList<>();
    public ImageView lottieDomicilio, lottieComprobante, lottieIdentificacion, lottieContrato, lottieFichaDeposito;
    public String calleCobranza="", exteriorCobranza="", interiorCobranza="", coloniaCobranza="", localidadCobranza="", postalCobranza="", entreCallesCobranza="";
    boolean reCapturaDeFotos = false;
    //----------

    TextView direccion;



    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_comprobantes_fotografias);
        myDialog = new Dialog(this);


        direccion = (TextView) findViewById(R.id.txtDireccion);
        btCancelar = (Button) findViewById(R.id.btCancelar);
        btBack=(ImageView)findViewById(R.id.btBack);
        imgDomicilio=(ImageView)findViewById(R.id.imgDomicilio);
        imgComprobante=(ImageView)findViewById(R.id.imgComprobante);
        imgIdentificacion=(ImageView)findViewById(R.id.imgIdentificacion);
        imgContrato=(ImageView)findViewById(R.id.imgContrato);
        imgFichaDeposito=(ImageView)findViewById(R.id.imgFichaDeposito);
        tvSolicitudActual=(TextView)findViewById(R.id.tvSolicitudActual);
        tv1=(TextView)findViewById(R.id.tv1);
        tv2=(TextView)findViewById(R.id.tv2);
        tv3=(TextView)findViewById(R.id.tv3);
        tv4=(TextView)findViewById(R.id.tv4);
        cardDomicilio=(CardView)findViewById(R.id.cardDomicilio);
        cardComprobante=(CardView)findViewById(R.id.cardComprobante);
        cardIdentificacion=(CardView)findViewById(R.id.cardIdentificacion);
        cardContraro=(CardView)findViewById(R.id.cardContraro);
        cardFichaDeposito=(CardView)findViewById(R.id.cardFichaDeposito);
        btFinalizar=(Button)findViewById(R.id.btFinalizar);

        lottieDomicilio=(ImageView) findViewById(R.id.lottieDomicilio);
        lottieComprobante =(ImageView)findViewById(R.id.lottieComprobante);
        lottieIdentificacion =(ImageView)findViewById(R.id.lottieIdentificacion);
        lottieContrato =(ImageView)findViewById(R.id.lottieContrato);
        lottieFichaDeposito =(ImageView)findViewById(R.id.lottieFichaDeposito);

        img1=(ImageView)findViewById(R.id.img1);
        img2=(ImageView)findViewById(R.id.img2);
        img3=(ImageView)findViewById(R.id.img3);
        img4=(ImageView)findViewById(R.id.img4);
        img5=(ImageView)findViewById(R.id.img5);
        t1=(TextView)findViewById(R.id.t1);
        t2=(TextView)findViewById(R.id.t2);
        t3=(TextView)findViewById(R.id.t3);
        t4=(TextView)findViewById(R.id.t4);
        t5=(TextView)findViewById(R.id.t5);
        bottomSheetDialog = new BottomSheetDialog(ComprobantesFotografias.this);

        final Bundle extras = getIntent().getExtras();
        if(extras!=null)
        {
            if (extras.containsKey("reCapturaDeFotos"))
            {
                reCapturaDeFotos = extras.getBoolean("reCapturaDeFotos");
                numeroSolicitud = extras.getString("numeroSolicitud");
                codigoPromotor = extras.getString("codigoPromotor");
                tipoSolicitud = extras.getString("tipoSolicitud");
                codigo_activacion_recaptura = extras.getString("codigo_add_photo");
                tvSolicitudActual.setText(numeroSolicitud);
            }
            else {
                reCapturaDeFotos = false;

                nombre = extras.getString("nombre");
                paterno = extras.getString("paterno");
                materno = extras.getString("materno");
                calle = extras.getString("calle");

                colonia = extras.getString("colonia");
                ciudad = extras.getString("ciudad");

                estadoCivil = extras.getString("estadoCivil");
                codigoPostal = extras.getString("codigoPostal");
                fechaNacimiento = extras.getString("fechaNacimiento");
                telefono = extras.getString("telefono");
                observaciones = extras.getString("observaciones");
                nuevoIngreso = extras.getString("nuevoIngreso");
                opcionSP = extras.getString("opcionSP");
                entreCalles = extras.getString("entreCalles");
                numeroSolicitud = extras.getString("numeroSolicitud");
                codigoPromotor = extras.getString("codigoPromotor");
                tipoSolicitud = extras.getString("tipoSolicitud");
                letraIndice = extras.getString("letraIndice");
                inversionInicial = extras.getString("inversion");
                numeroInterior = extras.getString("numeroInterior");
                numeroExterior = extras.getString("numeroExterior");
                //------- Domiiclio cobranza -------
                calleCobranza = extras.getString("calleCobranza");
                exteriorCobranza = extras.getString("exteriorCobranza");
                interiorCobranza = extras.getString("interiorCobranza");

                coloniaCobranza = extras.getString("coloniaCobranza");
                localidadCobranza = extras.getString("localidadCobranza");

                postalCobranza = extras.getString("postalCobranza");
                entreCallesCobranza = extras.getString("entreCallesCobranza");
                tvSolicitudActual.setText(numeroSolicitud);
            }
        }
        else
        {
            Log.d(TAG, "NO SE ECONTRARON REISTROS DE EXTRAS");
        }


        tv1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
        tv2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
        tv3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
        tv4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style));


        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //-----------
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/MyPictureApp2/";
        newdir = new File(dir);
        if (!newdir.exists()) {
            newdir.mkdir();
        }

        cardDomicilio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions("-DomicilioExterior");
                NOMBRE_IMAGEN_PREFIJO = "DomicilioExterior"+"C"+codigoPromotor;
                indicadorCarta=0;
                indicadorCarta=1;
            }
        });

        cardComprobante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions("-ComprobanteDomicilio");
                NOMBRE_IMAGEN_PREFIJO = "ComprobanteDomicilio"+"C"+codigoPromotor;
                indicadorCarta=0;
                indicadorCarta=2;
            }
        });

        cardIdentificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions("-Identificacion");
                NOMBRE_IMAGEN_PREFIJO = "Identificacion"+"C"+codigoPromotor;
                indicadorCarta=0;
                indicadorCarta=3;
            }
        });

        cardContraro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions("-Contrato");
                NOMBRE_IMAGEN_PREFIJO = "Contrato"+"C"+codigoPromotor;
                indicadorCarta=0;
                indicadorCarta=4;
            }
        });

        cardFichaDeposito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions("-FichaDeposito");
                NOMBRE_IMAGEN_PREFIJO = "FichaDeposito"+"C"+codigoPromotor;
                indicadorCarta=0;
                indicadorCarta=5;
            }
        });

        btFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                try
                {
                    if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                    {
                        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if(isGPSEnabled)
                        {
                            if (getLocationMode() == 3)
                            {

                                if(reCapturaDeFotos)
                                {
                                    if(reCapturoImagenes)
                                    {
                                        for(int i=0; i<=listaBitmaps.size()-1;i++)
                                        {
                                            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            Date datee;

                                            String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();

                                            try {
                                                if (arregloCoordenadas.length > 0) {
                                                    latitud =arregloCoordenadas[0];
                                                    longitud = arregloCoordenadas[1];
                                                }

                                            } catch (NullPointerException ex) {
                                                Log.e(TAG, "onClick: Error " + ex.getMessage());
                                                try {
                                                    String[] arregloCoordenadasAux = ApplicationResourcesProvider.getCoordenadasFromApplication();
                                                    if (arregloCoordenadasAux.length > 0) {
                                                        latitud =arregloCoordenadasAux[0];
                                                        longitud = arregloCoordenadasAux[1];
                                                    }
                                                } catch (Exception e) {
                                                    Log.e(TAG, "onClick: Error " + e.getMessage());
                                                }

                                            }

                                            Calendar calendar = Calendar.getInstance();
                                            calendar.setTime(datee = new Date());
                                            calendar.add(Calendar.SECOND, i+3);
                                            Date fechaSalida = calendar.getTime();
                                            Control.insertarArchivo(
                                                    codigo_activacion_recaptura,
                                                    "2",
                                                    listaImagenes.get(i).toString(),
                                                    listaBitmaps.get(i).toString(),
                                                    0,
                                                    latitud,
                                                    longitud,
                                                    dateFormatter.format(fechaSalida)
                                            );
                                        }

                                        if(listaBitmaps.size()>0)
                                        {
                                            for(int i=0; i<=listaBitmaps.size()-1;i++)
                                            {
                                                Control.insertarFotografiasPorSolicitud(numeroSolicitud, codigo_activacion_recaptura, listaImagenes.get(i).toString());
                                            }
                                        }


                                        showPopup(codigo_activacion_recaptura, numeroSolicitud, true);

                                        ExportImportDB exportImportDB = new ExportImportDB();
                                        try {
                                            if (exportImportDB.backupDBHidden()) {
                                                Log.i(TAG, "EXPORTADA CORRECTAMENTE");
                                            } else {
                                                Log.i(TAG, "ERROR AL EXPORTAR");
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    else
                                        showError(view , "Debes agregar una fotografía para continuar, de lo contrario puedes presionar ** CANCELAR **");

                                }
                                else
                                {
                                    procesoOffline();
                                    ExportImportDB exportImportDB = new ExportImportDB();
                                    try {
                                        if (exportImportDB.backupDBHidden()) {
                                            Log.i(TAG, "EXPORTADA CORRECTAMENTE");
                                        } else {
                                            Log.i(TAG, "ERROR AL EXPORTAR");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                            else
                            {
                                final AlertDialog.Builder dialog = getBuilder();
                                try {
                                    dialog.setCancelable(false);
                                    dialog.setTitle("Advertencia");
                                    dialog.setMessage("Necesitas activar el modo GPS ALTA PRESICIÓN para continuar.");
                                    dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                                    {
                                        if (getLocationMode() != 3)
                                            dialog.create().show();
                                    });
                                    dialog.create().show();

                                } catch (Exception e) {

                                    Toast.makeText(ComprobantesFotografias.this, "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Verifica tu GPS", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        final AlertDialog.Builder dialog = getBuilder();
                        try {
                            dialog.setCancelable(false);
                            dialog.setTitle("Advertencia");
                            dialog.setMessage("Parece que no tienes el GPS Activado");
                            dialog.create().show();

                        } catch (Exception e) {
                            Toast.makeText(ComprobantesFotografias.this, "Parece que no tienes el GPS Activado", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                }catch (NullPointerException ex)
                {
                    Log.e(TAG, "Ocurrio un error: " + ex.getMessage());
                    Toast.makeText(getApplicationContext(), "No podemos verificar el GPS, reinicia el GPS", Toast.LENGTH_SHORT).show();
                    Mint.logEvent("Error Exception " + numeroSolicitud + " - user: " + codigoPromotor, MintLogLevel.Info);
                }
            }
        });

        if(reCapturaDeFotos)
        {
            btFinalizar.setText("Guardar");
            consultaDeFotosGuardadasPorSolicitud(numeroSolicitud);
            btCancelar.setVisibility(View.VISIBLE);
            tv4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
        }

        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }






    private void consultaDeFotosGuardadasPorSolicitud(String solicitudCliente)
    {
        int sizeList = Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).size();
        for(int i=0; i<= Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).size()-1; i++)
        {
            if (Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).get(i).contains("DomicilioExterior"))
            {
                img1.setVisibility(View.INVISIBLE);
                //imgDomicilio.setVisibility(View.INVISIBLE);
                t1.setTextColor(Color.DKGRAY);
                lottieDomicilio.setVisibility(View.VISIBLE);
                lottieDomicilio.setAlpha(0.6f);
                //lottieDomicilio.playAnimation();
                cardDomicilio.setEnabled(false);
            }
            else if(Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).get(i).contains("ComprobanteDomicilio"))
            {
                img2.setVisibility(View.INVISIBLE);
                t2.setTextColor(Color.DKGRAY);
                //imgComprobante.setVisibility(View.INVISIBLE);
                lottieComprobante.setVisibility(View.VISIBLE);
                lottieComprobante.setAlpha(0.6f);
                //lottieComprobante.playAnimation();
                cardComprobante.setEnabled(false);
            }
            else if (Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).get(i).contains("Identificacion"))
            {
                img3.setVisibility(View.INVISIBLE);
                t3.setTextColor(Color.DKGRAY);
                //imgIdentificacion.setVisibility(View.INVISIBLE);
                lottieIdentificacion.setVisibility(View.VISIBLE);
                lottieIdentificacion.setAlpha(0.6f);
                //lottieIdentificacion.playAnimation();
                cardIdentificacion.setEnabled(false);
            }
            else if (Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).get(i).contains("Contrato"))
            {
                img4.setVisibility(View.INVISIBLE);
                t4.setTextColor(Color.DKGRAY);
                //imgContrato.setVisibility(View.INVISIBLE);

                lottieContrato.setVisibility(View.VISIBLE);
                lottieContrato.setAlpha(0.6f);
                //lottieContrato.playAnimation();
                cardContraro.setEnabled(false);
            }
            else if (Control.getNombreFotografiasPorSolicitudRegistrada(solicitudCliente).get(i).contains("FichaDeposito"))
            {
                img5.setVisibility(View.INVISIBLE);
                t5.setTextColor(Color.DKGRAY);
                //imgFichaDeposito.setVisibility(View.INVISIBLE);
                lottieFichaDeposito.setVisibility(View.VISIBLE);
                lottieFichaDeposito.setAlpha(0.6f);
                //lottieFichaDeposito.playAnimation();
                cardFichaDeposito.setEnabled(false);
            }
        }

    }

    private void showOptions(final String seleccion) {
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(ComprobantesFotografias.this);
        builder.setTitle("Eleige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which] == "Tomar foto"){
                    openCamera(seleccion);
                }else if(option[which] == "Elegir de galeria"){
                    showFileChooser();
                }else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private void openCamera(String prefijoSeleccion)
    {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            imageName = prefijoSeleccion + timestamp.toString() + ".jpg";
            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
            File newFile = new File(mPath);
            try
            {
                newFile.createNewFile();
            }
            catch (IOException e)
            {
                Log.v(TAG, "Error al abrir camara: " + e.getMessage());
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file_path", mPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPath = savedInstanceState.getString("file_path");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this, new String[]{mPath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> Uri = " + uri);
                        }
                    });

                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);
                    if (mPath.contains("DomicilioExterior"))
                    {
                        img1.setVisibility(View.INVISIBLE);
                        //imgDomicilio.setVisibility(View.INVISIBLE);
                        t1.setTextColor(Color.DKGRAY);
                        lottieDomicilio.setVisibility(View.VISIBLE);
                        lottieDomicilio.setAlpha(0.5f);
                        //lottieDomicilio.playAnimation();
                        String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                        listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                    }
                    else if(mPath.contains("ComprobanteDomicilio"))
                    {
                        img2.setVisibility(View.INVISIBLE);
                        //imgComprobante.setImageBitmap(bitmap);
                        t2.setTextColor(Color.DKGRAY);
                        //imgComprobante.setVisibility(View.INVISIBLE);
                        lottieComprobante.setVisibility(View.VISIBLE);
                        lottieComprobante.setAlpha(0.5f);
                        //lottieComprobante.playAnimation();
                        String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                        listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                    }
                    else if (mPath.contains("Identificacion"))
                    {
                        //imgIdentificacion.setImageBitmap(bitmap);
                        img3.setVisibility(View.INVISIBLE);
                        t3.setTextColor(Color.DKGRAY);
                        //imgIdentificacion.setVisibility(View.INVISIBLE);
                        lottieIdentificacion.setVisibility(View.VISIBLE);
                        lottieIdentificacion.setAlpha(0.5f);
                        //lottieIdentificacion.playAnimation();
                        String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                        listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                    }
                    else if (mPath.contains("Contrato"))
                    {
                        img4.setVisibility(View.INVISIBLE);
                        //imgContrato.setImageBitmap(bitmap);
                        t4.setTextColor(Color.DKGRAY);
                        //imgContrato.setVisibility(View.INVISIBLE);

                        lottieContrato.setVisibility(View.VISIBLE);
                        lottieContrato.setAlpha(0.5f);
                        //lottieContrato.playAnimation();
                        String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                        listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                    }
                    else if (mPath.contains("FichaDeposito"))
                    {
                        img5.setVisibility(View.INVISIBLE);
                        //imgContrato.setImageBitmap(bitmap);
                        t5.setTextColor(Color.DKGRAY);
                        //imgFichaDeposito.setVisibility(View.INVISIBLE);
                        lottieFichaDeposito.setVisibility(View.VISIBLE);
                        lottieFichaDeposito.setAlpha(0.5f);
                        //lottieFichaDeposito.playAnimation();
                        String valor = String.valueOf((int)(Math.random() * 999999) * (-1) / 2);
                        listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                        listaBitmaps.add(getStringImagen(bitmap));
                    }

                    if(listaBitmaps.size()>0 && listaImagenes.size()>0)
                        reCapturoImagenes = true;

                    break;

                case SELECT_PICTURE:
                    Uri path = data.getData();
                    try
                    {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                        imgDomicilio.setImageURI(path);
                    }catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    break;
            }
        }

        //----------------Elegir imagen-------------------------------
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            try {
                Uri selectedImage = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                String wholeID = DocumentsContract.getDocumentId(selectedImage);
                String id = wholeID.split(":")[1];
                String[] column = { MediaStore.Images.Media.DATA };
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[] { id }, null);

                String filesPath = "";
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filesPath = cursor.getString(columnIndex);
                }
                cursor.close();

                //Validar filesPath
                if (filesPath.contains("DomicilioExterior") || indicadorCarta==1)
                {
                    img1.setVisibility(View.INVISIBLE);
                    t1.setTextColor(Color.DKGRAY);
                    //imgDomicilio.setVisibility(View.INVISIBLE);
                    lottieDomicilio.setVisibility(View.VISIBLE);
                    lottieDomicilio.setAlpha(0.5f);
                    //lottieDomicilio.playAnimation();
                    String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                    listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                    listaBitmaps.add(getStringImagen(bitmap));
                }
                else if(filesPath.contains("ComprobanteDomicilio") || indicadorCarta ==2)
                {
                    //imgComprobante.setImageBitmap(bitmap);
                    t2.setTextColor(Color.DKGRAY);
                    img2.setVisibility(View.INVISIBLE);
                    //imgComprobante.setVisibility(View.INVISIBLE);

                    lottieComprobante.setVisibility(View.VISIBLE);
                    lottieComprobante.setAlpha(0.5f);
                    // lottieComprobante.playAnimation();
                    String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                    listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                    listaBitmaps.add(getStringImagen(bitmap));
                }
                else if (filesPath.contains("Identificacion") || indicadorCarta ==3)
                {
                    //imgIdentificacion.setImageBitmap(bitmap);
                    t3.setTextColor(Color.DKGRAY);
                    img3.setVisibility(View.INVISIBLE);
                    //imgIdentificacion.setVisibility(View.INVISIBLE);

                    lottieIdentificacion.setVisibility(View.VISIBLE);
                    lottieIdentificacion.setAlpha(0.5f);
                    //lottieIdentificacion.playAnimation();
                    String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                    listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                    listaBitmaps.add(getStringImagen(bitmap));
                }
                else if(filesPath.contains("Contrato") || indicadorCarta ==4)
                {
                    //imgContrato.setImageBitmap(bitmap);
                    img4.setVisibility(View.INVISIBLE);
                    t4.setTextColor(Color.DKGRAY);
                    //imgContrato.setVisibility(View.INVISIBLE);
                    lottieContrato.setVisibility(View.VISIBLE);
                    lottieContrato.setAlpha(0.5f);
                    //lottieContrato.playAnimation();
                    String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                    listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                    listaBitmaps.add(getStringImagen(bitmap));
                }
                else if(filesPath.contains("FichaDeposito") || indicadorCarta ==5)
                {
                    //imgContrato.setImageBitmap(bitmap);
                    img5.setVisibility(View.INVISIBLE);
                    t5.setTextColor(Color.DKGRAY);
                    //imgFichaDeposito.setVisibility(View.INVISIBLE);
                    lottieFichaDeposito.setVisibility(View.VISIBLE);
                    lottieFichaDeposito.setAlpha(0.5f);
                    //lottieFichaDeposito.playAnimation();
                    String valor = String.valueOf((int)(Math.random() * 999999) * (1+1) / 2);
                    listaImagenes.add(NOMBRE_IMAGEN_PREFIJO+valor);
                    listaBitmaps.add(getStringImagen(bitmap));
                }

                if(listaBitmaps.size()>0 && listaImagenes.size()>0)
                    reCapturoImagenes = true;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(ComprobantesFotografias.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                //mOptionButton.setEnabled(true);
            }
        }else{
            showExplanation();
        }

        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ComprobantesFotografias.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de la app necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleciona imagen"), PICK_IMAGE_REQUEST);
    }


    public String getStringImagen(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }



    public boolean verificarInternet()
    {
        ConnectivityManager con = (ConnectivityManager) ComprobantesFotografias.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public  void  procesoOffline()
    {
        pDialog=new ProgressDialog(ComprobantesFotografias.this);
        pDialog.setMessage("Guardando datos, espera por favor...");
        pDialog.setCancelable(false);
        pDialog.show();

        codigoDeActivacion = "M"+letraIndice+generarRandom();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format( new Date() );
        //inserta el cliente
        Control.insertarCliente(
                ""+nombre,
                ""+paterno,
                ""+materno,
                ""+fechaNacimiento,
                ""+telefono,
                ""+"XAXX010101000",
                ""+observaciones,
                ""+ estadoCivil
        );

        Control.insertarDomicilios(
                ""+ Control.getLastIDCLIENT(),
                ""+calle,
                ""+numeroExterior,
                ""+numeroInterior,
                ""+colonia,
                ""+ciudad,
                ""+codigoPostal,
                ""+entreCalles,
                "1"
        );

        Control.insertarDomicilios(
                ""+ Control.getLastIDCLIENT(),
                ""+calleCobranza,
                ""+exteriorCobranza,
                ""+interiorCobranza,
                ""+coloniaCobranza,
                ""+localidadCobranza,
                ""+postalCobranza,
                ""+entreCallesCobranza,
                "2"
        );

        //inserta la solicitud
        Control.insertarSolicitudCliente(
                ""+codigoDeActivacion,
                ""+numeroSolicitud,
                ""+String.valueOf(Control.getLastIDCLIENT()),
                ""+nuevoIngreso,
                ""+opcionSP,
                ""+date,
                ""+inversionInicial,
                0
        );

        String query="UPDATE SOLICITUD SET usada ='1' WHERE solSerie='" + numeroSolicitud+"'";
        Solicitud.executeQuery(query);

        for(int i=0; i<=listaBitmaps.size()-1;i++)
        {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date datee;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(datee = new Date());
            calendar.add(Calendar.SECOND, i+3);
            Date fechaSalida = calendar.getTime(); //Y ya tienes la fecha sumada.

            String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();

            try {
                if (arregloCoordenadas.length > 0) {
                    latitud =arregloCoordenadas[0];
                    longitud = arregloCoordenadas[1];
                }

            } catch (NullPointerException ex) {
                Log.e(TAG, "onClick: Error " + ex.getMessage());
                try {
                    String[] arregloCoordenadasAux = ApplicationResourcesProvider.getCoordenadasFromApplication();
                    if (arregloCoordenadasAux.length > 0) {
                        latitud =arregloCoordenadasAux[0];
                        longitud = arregloCoordenadasAux[1];
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onClick: Error " + e.getMessage());
                }

            }

            Control.insertarArchivo(codigoDeActivacion, "2",
                    listaImagenes.get(i).toString(),
                    listaBitmaps.get(i).toString(),
                    0,
                    latitud,
                    longitud,
                    dateFormatter.format(fechaSalida));
        }

        if(listaBitmaps.size()>0)
        {
            for(int i=0; i<=listaBitmaps.size()-1;i++)
            {
                Control.insertarFotografiasPorSolicitud(numeroSolicitud, codigoDeActivacion, listaImagenes.get(i).toString());
            }
        }
        else
            Control.insertarFotografiasPorSolicitud(numeroSolicitud, codigoDeActivacion, "");

        pDialog.dismiss();
        pDialog.cancel();

        LogRegister.solicitudRegistrationBackup(
                ""+numeroSolicitud,
                ""+codigoDeActivacion,
                ""+nombre + " "+ paterno + " " + materno,
                ""+estadoCivil,
                ""+fechaNacimiento,
                ""+telefono,
                ""+observaciones,
                ""+nuevoIngreso,
                ""+opcionSP,
                ""+calle,
                ""+numeroExterior,
                ""+numeroInterior,
                ""+colonia,
                ""+ciudad,
                ""+codigoPostal,
                ""+entreCalles,
                ""+calleCobranza,
                ""+exteriorCobranza,
                ""+interiorCobranza,
                ""+coloniaCobranza,
                ""+localidadCobranza,
                ""+postalCobranza,
                ""+entreCallesCobranza
        );

        showPopup(codigoDeActivacion, numeroSolicitud, false);
        btFinalizar.setText(codigoDeActivacion);

    }

    public String generarRandom()
    {
        String random="";
        String cadena="";
        for(int i=0; i<=6;i++)
        {
            random = String.valueOf((int) (9* Math.random()));

                for(int y=0; y<=random.length()-1;y++)
                {
                    if (random.charAt(y) == '-') {
                        random = random + String.valueOf(Integer.parseInt(random) * (-Integer.parseInt(random)));
                    } else {
                        if(cadena.length()>=7)
                        {
                            break;
                        }
                        else
                            {
                                cadena = cadena + random;
                                break;
                            }
                    }
                }
        }
        return cadena;
    }

    /*public long consultaIDCliente(String primerNombre, String paternoN, String maternoN) //nomobre+paterno+materno
    {
        long idCliente=0;
        //cliente_Nombre, cliente_ApellidoPaterno, getCliente_ApellidoMaterno,
        String query="SELECT * FROM CLIENTE WHERE nombre = '"+ primerNombre+ "' and paterno = '"+ paternoN +"' and materno ='"+ maternoN+"'";
        List<Cliente> notes = Cliente.findWithQuery(Cliente.class, query);
        if(notes.size()>0)
        {
            idCliente= notes.get(0).getId();
        }
        return idCliente;
    }*/

    public void showPopup(final String codigoActivacion, final String noSol, boolean reCapturaDeFotos) {
        final Button btNew, btCapturarPantalla, btFinalizarCamera;
        String cadena="";
        TextView tvCodigo, tvNoSol;
        myDialog.setContentView(R.layout.solicitud_final);
        myDialog.setCancelable(false);
        btNew = (Button) myDialog.findViewById(R.id.btNew);
        btFinalizarCamera = (Button) myDialog.findViewById(R.id.btFinalizarCamera);
        tvCodigo = (TextView) myDialog.findViewById(R.id.tvCodigo);
        btCapturarPantalla =(Button) myDialog.findViewById(R.id.btCapturarPantalla);
        lottieNF= (LottieAnimationView)myDialog.findViewById(R.id.lottieNF);
        lottieCamera= (LottieAnimationView)myDialog.findViewById(R.id.lottieCamera);
        cardCodigo=(CardView) myDialog.findViewById(R.id.cardCodigo);
        cardCamera=(CardView) myDialog.findViewById(R.id.cardRecapturaDeFotos);
        tvNoSol=(TextView) myDialog.findViewById(R.id.tvNoSol);
        tv4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.dot_style_green));
        lottieNF.setAnimation("success2.json");
        lottieNF.loop(true);
        lottieCamera.setAnimation("camera.json");
        lottieCamera.loop(false);

        if(reCapturaDeFotos)
        {
            lottieCamera.playAnimation();
            cardCodigo.setVisibility(View.GONE);
            cardCamera.setVisibility(View.VISIBLE);
        }
        else
        {
            lottieNF.playAnimation();
            cardCamera.setVisibility(View.GONE);
            cardCodigo.setVisibility(View.VISIBLE);
        }


        btFinalizarCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                    {
                        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if(isGPSEnabled)
                        {
                            if (getLocationMode() == 3)
                            {
                                Intent intent = new Intent(getApplicationContext(), PrincipalScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.putExtra("fromComprobantes", true);

                                String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();

                                try {
                                    if (arregloCoordenadas.length > 0) {
                                        latitud =arregloCoordenadas[0];
                                        longitud = arregloCoordenadas[1];
                                    }

                                } catch (NullPointerException ex) {
                                    Log.e(TAG, "onClick: Error " + ex.getMessage());
                                    try {
                                        String[] arregloCoordenadasAux = ApplicationResourcesProvider.getCoordenadasFromApplication();
                                        if (arregloCoordenadasAux.length > 0) {
                                            latitud =arregloCoordenadasAux[0];
                                            longitud = arregloCoordenadasAux[1];
                                        }
                                    } catch (Exception e) {
                                        Log.e(TAG, "onClick: Error " + e.getMessage());
                                    }

                                }

                                saveSolicitud(
                                        noSol,
                                        latitud,
                                        longitud
                                );

                                Toast.makeText(ComprobantesFotografias.this, "Solicitud guardada correctamente", Toast.LENGTH_SHORT).show();
                                startActivityForResult(intent, 500);
                                myDialog.dismiss();
                                finish();
                            }
                            else
                            {
                                final AlertDialog.Builder dialog = getBuilder();
                                try {
                                    dialog.setCancelable(false);
                                    dialog.setTitle("Advertencia");
                                    dialog.setMessage("Necesitas activar el modo GPS ALTA PRESICIÓN para continuar.");
                                    dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                                    {
                                        if (getLocationMode() != 3)
                                            dialog.create().show();
                                    });
                                    dialog.create().show();

                                } catch (Exception e) {
                                    Toast.makeText(ComprobantesFotografias.this, "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Verifica tu GPS", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        final AlertDialog.Builder dialog = getBuilder();
                        try {
                            dialog.setCancelable(false);
                            dialog.setTitle("Advertencia");
                            dialog.setMessage("Parece que no tienes el GPS Activado");
                            dialog.create().show();

                        } catch (Exception e) {
                            Toast.makeText(ComprobantesFotografias.this, "Parece que no tienes el GPS Activado", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                }catch (Throwable ex)
                {
                    Log.e(TAG, ex.getMessage());
                    Intent intent = new Intent(getApplicationContext(), PrincipalScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("fromComprobantes", true);
                    saveSolicitud(noSol, "20.682452", "-103.381555");
                    Toast.makeText(ComprobantesFotografias.this, "Solicitud guardada correctamente", Toast.LENGTH_SHORT).show();
                    startActivityForResult(intent, 500);
                    myDialog.dismiss();
                    finish();
                }
            }
        });


        tvNoSol.setText("No. Solicitud: "+ noSol);


        for(int i=0; i<=codigoActivacion.length()-1;i++)
        {
            cadena=cadena + codigoActivacion.charAt(i);
            if(i==3 || i==7)
            {
                cadena=cadena + " ";
            }
        }

        tvCodigo.setText(cadena);
        btNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                try
                {
                    if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                    {
                        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if(isGPSEnabled)
                        {
                            if (getLocationMode() == 3)
                            {
                                Intent intent = new Intent(getApplicationContext(), PrincipalScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                String[] arregloCoordenadas = ApplicationResourcesProvider.getCoordenadasFromApplication();

                                try {
                                    if (arregloCoordenadas.length > 0) {
                                        latitud =arregloCoordenadas[0];
                                        longitud = arregloCoordenadas[1];
                                    }

                                } catch (NullPointerException ex) {
                                    Log.e(TAG, "onClick: Error " + ex.getMessage());
                                    try {
                                        String[] arregloCoordenadasAux = ApplicationResourcesProvider.getCoordenadasFromApplication();
                                        if (arregloCoordenadasAux.length > 0) {
                                            latitud =arregloCoordenadasAux[0];
                                            longitud = arregloCoordenadasAux[1];
                                        }
                                    } catch (Exception e) {
                                        Log.e(TAG, "onClick: Error " + e.getMessage());
                                    }

                                }

                                saveSolicitud(
                                        noSol,
                                        latitud,
                                        longitud
                                );

                                Toast.makeText(ComprobantesFotografias.this, "Solicitud guardada correctamente", Toast.LENGTH_SHORT).show();
                                startActivity(intent);
                                myDialog.dismiss();
                                finish();

                            }
                            else
                            {
                                final AlertDialog.Builder dialog = getBuilder();
                                try {
                                    dialog.setCancelable(false);
                                    dialog.setTitle("Advertencia");
                                    dialog.setMessage("Necesitas activar el modo GPS ALTA PRESICIÓN para continuar.");
                                    dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                                    {
                                        if (getLocationMode() != 3)
                                            dialog.create().show();
                                    });
                                    dialog.create().show();

                                } catch (Exception e) {
                                    Toast.makeText(ComprobantesFotografias.this, "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Verifica tu GPS", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        final AlertDialog.Builder dialog = getBuilder();
                        try {
                            dialog.setCancelable(false);
                            dialog.setTitle("Advertencia");
                            dialog.setMessage("Parece que no tienes el GPS Activado");
                            dialog.create().show();

                        } catch (Exception e) {

                            Toast.makeText(ComprobantesFotografias.this, "Parece que no tienes el GPS Activado", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                }catch (Throwable ex)
                {
                    Log.e(TAG, ex.toString());
                    Intent intent = new Intent(getApplicationContext(), PrincipalScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    saveSolicitud(noSol, "20.682452", "-103.381555");
                    Toast.makeText(ComprobantesFotografias.this, "Solicitud guardada correctamente", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                    myDialog.dismiss();
                    finish();
                }









            }
        });

        btCapturarPantalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
                boolean isDirectoryCreated = file.exists();

                if(!isDirectoryCreated)
                    isDirectoryCreated = file.mkdirs();

                if(isDirectoryCreated)
                {
                    Long timestamp = System.currentTimeMillis() / 1000;
                    String codigoActivacion = codigoDeActivacion + "_" + timestamp.toString() + ".jpg";
                    String mPathh = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + codigoActivacion;
                    File newFile = new File(mPathh);
                    try
                    {
                        newFile.createNewFile();
                        try {
                            lottieNF.setProgress(0.5f);
                            View v1 = myDialog.getWindow().getDecorView().getRootView();
                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);

                            FileOutputStream outputStream = new FileOutputStream(newFile);
                            int quality = 30;
                            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                            outputStream.flush();
                            outputStream.close();

                            Toast.makeText(ComprobantesFotografias.this, "Captura de pantalla realizada", Toast.LENGTH_SHORT).show();
                            btNew.setLayoutParams(new TableLayout.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, 1f));

                            btCapturarPantalla.setVisibility(View.GONE);

                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }


            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }




    @Override
    public final void onDestroy()
    {
        super.onDestroy();
        stopService();
    }




    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public int getLocationMode() {
        try {
            return Settings.Secure.getInt(getApplicationContext().getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private AlertDialog.Builder getBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else{
            return new AlertDialog.Builder(this);
        }
    }



    public void saveSolicitud(String noSolicitud, String latitude, String longitude)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = dateFormat.format(new Date());
        Control.insertarUbicaciones(noSolicitud, Control.getPromotorCodigo(), latitude, longitude, date, 0);
        Log.i(TAG, "Fecha de ubicación para save solicitud: " + date);
    }

    public void startService()
    {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STARTFOREGROUND_ACTION);
        ContextCompat.startForegroundService(this, serviceIntent);

    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(serviceIntent);
    }

    public void showError(View view, final String codeError) {
        final Button btNew;
        TextView tvCodeError;
        myDialog.setContentView(R.layout.login_incorrecto);
        myDialog.setCancelable(false);
        btNew = (Button) myDialog.findViewById(R.id.btNew);
        tvCodeError = (TextView) myDialog.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);
        if(codeError.equals("Debes agregar una fotografía para continuar, de lo contrario puedes presionar ** CANCELAR **")) {
            btNew.setText("Aceptar");
        }

        btNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(btNew.getText().toString().equals("Salir del sistema"))
                {
                    finishAffinity();
                }
                if(btNew.getText().toString().equals("Aceptar"))
                {
                    myDialog.dismiss();
                }
                else
                {
                    myDialog.dismiss();
                }
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }



}