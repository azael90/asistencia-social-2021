package com.example.sociales.Activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.splunk.mint.Mint;

/**
 * Created by Azael Jimenez on 7/24/20.
 */
public class MintActivity extends AppCompatActivity {

    /**********************************************************************************************
     *
     *                                         Splunk MINT SDK for Android
     *
     **********************************************************************************************/
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (com.example.sociales.Extras.BuildConfig.isIsForSplunkMintMonitoring()){
            Mint.disableNetworkMonitoring();
            Mint.initAndStartSession(this.getApplication(), "4f67c77a");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Mint.startSession(this);
        Mint.setFlushOnlyOverWiFi(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        Mint.closeSession(this);
    }

}
