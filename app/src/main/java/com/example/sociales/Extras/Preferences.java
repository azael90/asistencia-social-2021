package com.example.sociales.Extras;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences
{
    public static final String STRING_PREFERENCES="pabs.sociales";
    public static final String PREFERENCE_USUARIO_LOGIN="usuario.login";
    public static final String PREFERENCE_LOCK_APP = "lock.app";
    public static final String PREFERENCE_SERVICIO_FOREGROUND="servicio.foreground";

    public static void savePreferenceUsuarioLogin(Context c, String usuario, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(STRING_PREFERENCES, c.MODE_PRIVATE);
        preferences.edit().putString(key, usuario).apply();
    }

    public static String getPreferenceUsuarioLogin(Context c, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(STRING_PREFERENCES, c.MODE_PRIVATE);
        return preferences.getString(key, ""  );
    }

    public static void savePreferenceActivityStartedForeground(Context c, boolean isActivityStarted, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(STRING_PREFERENCES, c.MODE_PRIVATE);
        preferences.edit().putBoolean(key, isActivityStarted).apply();
    }

    public static boolean getPreferenceActivityStartedForeground(Context c, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(STRING_PREFERENCES, c.MODE_PRIVATE);
        return preferences.getBoolean(key, false  );
    }

    public static void saveLockApp(Context c, boolean isLockApp, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(STRING_PREFERENCES, c.MODE_PRIVATE);
        preferences.edit().putBoolean(key, isLockApp).apply();
    }

    public static boolean getLockApp(Context c, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(STRING_PREFERENCES, c.MODE_PRIVATE);
        return preferences.getBoolean(key, false  );
    }
}
