package com.example.sociales.Extras;

import android.app.Activity;
import android.graphics.Typeface;

import java.util.Date;

public class URL extends BaseActivity
{
    public static final String baseURL = "http://35.167.149.196";
    public Date fecha15DiasAtras;
    public static final String URL_LOGIN="http://35.167.149.196/ecobrotest/consultaClientesAsistenciaSocial.php";
    //http://35.167.149.196/asistencia_social_GDL/controlusuarios/loginAsistenteSocial
    public static final String URL_LOGIN_PROBENSO= getBaseURL() +"/controlusuarios/loginAsistenteSocial";
    public static final String URL_GET_CATALOGOS_SOLICITUDES= getBaseURL() +"/controlsolicitudes/getCatalogos";
    public static final String URL_CARGA_SOLICITUDES_A_WEB= getBaseURL() + "/controlsolicitudes/saveSolicitud";
    public static final String URL_IMAGENES_PUBLICIDAD= getBaseURL() + "/controlarchivos/getAdvertisingImages";
    public static final String URL_IMAGENES_PUBLICIDAD2= getBaseURL() + "/controlarchivos/getAdvertisingImagesURL";
    public static final String URL_SAVE_LOCATIONS_10_MINUTES= getBaseURL() + "/controlubicaciones/saveLocation";
    public static final String URL_LOAD_NEW_SOLICITUDES_TO_WEB= getBaseURL() + "/controlsolicitudes/saveSolicitudWithoutAllData";
    public static final String URL_LOAD_IMAGES_TO_SERVER=  getBaseURL() + "/controlarchivos/uploadImages";
    public static final String URL_DOWNLOAD_PDF_FOR_PROMOTOR=  getBaseURL() + "/controlarchivos/getComisionesFiles";
    public static final String URL_FIND_CONTRACT = getBaseURL() + "/controlecobro/getContractInfo";
    public static final String URL_FIND_AFILIACIONES_GERENTE = getBaseURL() + "/controlsolicitudes/getContractsQuantityByGerente";
    public static final String URL_FIND_AFILIACIONES_COORDINADOR = getBaseURL() + "/controlsolicitudes/getContractsQuantityByCoordinador";

    public static final String checkUpdate =  getBaseURL() + "/controlusuarios/getUpdateStatus/";
    public static final String checkVersion = getBaseURL() + "/application/update/version.txt";
    public static final String updateApp = getBaseURL() + "/application/update/";
    public static final String urlUpdateApp =  getBaseURL() + "/application/update/";


    private static String getBaseURL()
    {
        switch (BuildConfig.getTargetBranch())
        {
            case GUADALAJARA:
                return baseURL + "/asistencia_social_GDL";
            case CANCUN:
                return baseURL + "/asistencia_social";
            default:
                //no default behavior
        }
        return baseURL + "/asistencia_social_gdl";
    }


}
