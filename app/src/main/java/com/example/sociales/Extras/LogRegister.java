package com.example.sociales.Extras;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

public class LogRegister
{
    /**
     * This method will make a register of any payment made.
     * Will work as a backup for payments sent to server.
     *
     * @author Jordan Alvarez
     * @version 03/06/2015
     *
     * @editor Azael Jimenez
     * @version 17/03/2020
     */
    public static void solicitudRegistrationBackup(String solicitud, String codigoActivacion, String nombreCompleto, String estadoCivil, String fechaNacimiento, String telefono, String observaciones,
                                                 String nuevoIngreso, String esquemaPago, String calleCasa, String exteriorCasa, String interiorCasa, String coloniaCasa, String localidadCasa,
                                                 String codigoPostalCasa, String entreCallesCasa, String calleCobranza, String exteriorCobranza, String interiorCobranza, String coloniaCobranza,
                                                 String localidadCobranza, String codigoPostalCobranza, String entreCallesCobranza)
    {
        try {
            String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            Calendar cal = Calendar.getInstance();
            String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
            String data = "";
            String path = Environment.getExternalStorageDirectory()+"/.Android_Configuration/Intern_files/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            //format amount when is less than 10 pesos
            if (month.length() == 1){
                month = "0"+month;
            }

            File file = new File(path
                    , "solicitudes_backup "
                    + cal.get(Calendar.DAY_OF_MONTH) + "-" + month
                    + "-" + cal.get(Calendar.YEAR) + ".txt");

            if (!file.exists()){StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                //data+=formatter.format("%15s;%20s;%10s;%12s;%20s;%10s;%10s;%10s;\n", "Solicitud", "Nombre", "Edo. Civil", "Fecha Nac.", "Teléfono", "Observaciones", "NuevoIngreso", "EsquemaPago\n").toString();
                //data+=formatter.format("%9s;%24s;%9s;%30s;%13s;%11s;%17s;%24s;%14s;%15s;%10s;%10s;%17s;%20s;%18s;%18s;%16s;%15s;%15s;%18s;%23s;%30s;%30s;%10s;\n", "Solicitud", "codigoActivacion", "Nombre", "Edo. Civil", "Fecha Nac.", "Teléfono", "Observaciones", "NuevoIngreso", "EsquemaPago"
                data+=formatter.format("%20s;%20s;20s;%20s;%20s;%20s;%20s;%20s;%22s;%22s;%220s;%20s;%22s;%20s;%22s;%22s;%22s;%22s;%22s;%22s;%22s;%20s;%20s;%20s;\n", "Solicitud", "codigoActivacion", "Nombre", "Edo. Civil", "Fecha Nac.", "Teléfono", "Observaciones", "NuevoIngreso", "EsquemaPago"
                        , "CalleCasa", "#extCasa", "#intCasa", "coloniaCasa", "localidadCasa", "codigoPostalCasa", "entreCallesCasa", "CalleCobranza", "#extCobranza", "#intCobranza", "coloniaCobranza"
                        , "localidadCobranza", "codigoPostalCobranza", "entreCallesCobranza", "fechaLog\n").toString();
            }

            {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                //data+=formatter.format("%12s;%14s;%36s;%10s;%13s;%11s;%28s;%13s;%13s;%15s;%10s;%10s;%17s;%10s;%10s;%10s;%10s;%10s;%10s;%10s;%10s;%10s;%10s;%10s;\n", solicitud, codigoActivacion, nombreCompleto, estadoCivil, fechaNacimiento, telefono, observaciones, nuevoIngreso, esquemaPago,
                data+=formatter.format("%20s;%22s;%22s;%20s;%22s;%22s;%22s;%22s;%22s;%22s;%20s;%20s;%22s;%20s;%20s;%20s;%20s;%20s;%20s;%20s;%20s;%20s;%20s;%20s;\n", solicitud, codigoActivacion, nombreCompleto, estadoCivil, fechaNacimiento, telefono, observaciones, nuevoIngreso, esquemaPago,
                        calleCasa, exteriorCasa, interiorCasa, coloniaCasa, localidadCasa, codigoPostalCasa, entreCallesCasa, calleCobranza, exteriorCobranza, interiorCobranza, coloniaCobranza,
                        localidadCobranza, codigoPostalCobranza, entreCallesCobranza, fecha).toString();
            }

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
            outputStreamWriter.write(data);
            outputStreamWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Probenso", "File write failed: " + e.toString());
        }
    }

}
