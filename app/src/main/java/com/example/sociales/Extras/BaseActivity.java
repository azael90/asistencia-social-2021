package com.example.sociales.Extras;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import android.os.CountDownTimer;

import com.example.sociales.Activities.MainActivity;
import com.example.sociales.Activities.MintActivity;

import java.util.Locale;

@SuppressLint("Registered")
public abstract class BaseActivity extends MintActivity
{
    private long startTime=600000;
    MyCountDownTimer countDownTimer = null;
    boolean isActivityStarted= false;
    @Override
    public void onResume() {
        super.onResume();

        if(!isActivityStarted)
        {
            Log.v("TIEMPO", "==============================> Actividad iniciada: "+ isActivityStarted);
            countDownTimer = new MyCountDownTimer(startTime, 1000);
            countDownTimer.start();
            isActivityStarted = true;
            Log.v("TIEMPO", "==============================> Actividad iniciada: "+ isActivityStarted);
        }
        Log.v("TIEMPO", "--------> onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("TIEMPO", "--------> onPause");
        if(countDownTimer!=null)
            countDownTimer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(countDownTimer!=null)
        {
            countDownTimer.cancel();
            countDownTimer=null;
        }
        Log.v("TIEMPO", "--------> onDestroy");
    }

    @Override
    public void onUserInteraction()
    {
        super.onUserInteraction();
            countDownTimer.cancel();
            countDownTimer.start();
        Log.v("TIEMPO", "----------> onUserInteraction");
    }


    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
        @Override
        public void onTick(long millisUntilFinished)
        {
            startTime= millisUntilFinished;
            int minutes =(int) ((startTime)/1000) / 60;
            int seconds =(int) (startTime/1000) % 60;
            String timeLeftFormatted= String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
            Log.v("TIEMPO", "Tiempo restante: "+timeLeftFormatted);
        }
    }

    /*public void startService()
    {

        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STARTFOREGROUND_ACTION);
        ContextCompat.startForegroundService(this, serviceIntent);
        //Preferences.savePreferenceActivityStartedForeground(this, true, Preferences.PREFERENCE_SERVICIO_FOREGROUND);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(serviceIntent);
    }*/
}


