package com.example.sociales.Extras;

/**
 * This class defines flags that depending on their
 * values, the app behaves differently
 *
 * @autor Created by adria_000 on 24/02/2015.
 * @author Modified by Jordan Alvarez
 * @author Modified by Azael Jimenez on 11/03/2020
 * @version 3.0
 */
public final class BuildConfig
{
    public static Branches targetBranch = Branches.GUADALAJARA;
    public static boolean DEBUG = false;
    private static boolean isForSplunkMintMonitoring = true;

    @SuppressWarnings("SpellCheckingInspection")
    public static enum Branches{GUADALAJARA, CANCUN}

    private BuildConfig()
    {

    }
    public static Branches getTargetBranch(){
        return targetBranch;
    }
    public static boolean isDEBUG() {
        return DEBUG;
    }
    public static void setDEBUG(boolean DEBUG) {
        BuildConfig.DEBUG = DEBUG;
    }

    public static boolean isIsForSplunkMintMonitoring() {
        return isForSplunkMintMonitoring;
    }

}
