package com.example.sociales.Extras;

    import android.Manifest;
    import android.annotation.SuppressLint;
    import android.app.AlertDialog;
    import android.app.PendingIntent;
    import android.content.Context;
    import android.content.DialogInterface;
    import android.content.Intent;
    import android.content.pm.PackageManager;
    import android.graphics.Typeface;
    import android.location.Location;
    import android.os.CountDownTimer;
    import android.os.Handler;
    import android.util.Log;
    import android.view.View;
    import android.view.animation.Animation;
    import android.view.animation.AnimationUtils;
    import android.widget.Toast;

    import androidx.core.app.ActivityCompat;
    import androidx.core.content.ContextCompat;

    import com.android.volley.DefaultRetryPolicy;
    import com.android.volley.Request;
    import com.android.volley.Response;
    import com.android.volley.VolleyError;
    import com.android.volley.toolbox.JsonObjectRequest;
    import com.bumptech.glide.util.Util;
    import com.example.sociales.Activities.MainActivity;
    import com.example.sociales.DatabaseLocal.Archivo;
    import com.example.sociales.DatabaseLocal.ArchivosDescargados;
    import com.example.sociales.DatabaseLocal.Cliente;
    import com.example.sociales.DatabaseLocal.Control;
    import com.example.sociales.DatabaseLocal.Domicilios;
    import com.example.sociales.DatabaseLocal.Promotor;
    import com.example.sociales.DatabaseLocal.Promotor_Status;
    import com.example.sociales.DatabaseLocal.Solicitud;
    import com.example.sociales.DatabaseLocal.Solicitud_Cliente;
    import com.example.sociales.DatabaseLocal.SolicitudesNuevas;
    import com.example.sociales.DatabaseLocal.Ubicacion;
    import com.example.sociales.R;
    import com.example.sociales.Service.Constants;
    import com.example.sociales.Service.ForegroundService;
    import com.google.android.gms.location.FusedLocationProviderClient;
    import com.google.android.gms.location.LocationRequest;
    import com.google.android.gms.location.LocationServices;
    import com.google.android.gms.tasks.OnSuccessListener;
    import com.google.android.gms.tasks.Task;
    import com.google.gson.Gson;
    import com.orm.SugarRecord;

    import org.json.JSONArray;
    import org.json.JSONException;
    import org.json.JSONObject;

    import java.text.SimpleDateFormat;
    import java.util.Calendar;
    import java.util.Date;
    import java.util.List;
    import java.util.Locale;
    import java.util.Random;
    import java.util.Timer;
    import java.util.TimerTask;

/**
* Created by Jordan Alvarez on 09/08/2019.
* This class will keep a reference of some important application resources
* as application context and LocationProvider
* outside or inside an Activity
*
* Instantiate this class and call getContext() or getLocationProvider()
* to get application context wherever you need it
*/
    public class ApplicationResourcesProvider extends com.orm.SugarApp {

    private static final String TAG = "ApplicationResourceProvider";
    /**
 * Keeps a reference of the application context
 * outside or inside an Activity
     *
     *
     * 1 -> coordinador
     * 2-> Gerente
     * 3 -> Asistente
     * 4 ->Admin
     *
     *
 */
    private static Context sContext;
    private Timer timerSendPaymentsEachTenMinutes;
    int seconds = 0;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    public static double latitud = 0, longitud = 0;
    private static long  error_code_location = 0;
    private static Date date;
    private static String activarBackground = "0", no_cobrador = "", timeLocation = "0", gps_provider = "";
    static Location beforeLoc;
    static float distance = 0f;

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        comenzar();
        timerCallLocations();
        updateLocation();
        timerForInsertLocations();
        timerForSyncLocationsToWeb();
    }
    public static Context getContext() {
        return sContext;
    }

    public void comenzar() {
        timerSendPaymentsEachTenMinutes = new Timer();
        timerSendPaymentsEachTenMinutes.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                if (seconds == 0) {
                    timerSendPaymentsEachTenMinutes.cancel();
                    seconds = 300;
                    creacionDeJsonParaRealizarLaCargaDeSolicitudes();
                    createJsonForPhotos();
                    creacionDeJsonParaRealizarLaCargaDeNuevasSolicitudes();
                    comenzar();
                }
                seconds -= 1;
            }
        }, 0, 1000);
    }
    public void timerForSyncLocationsToWeb() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                timerForSyncLocationsToWeb();
                createJsonForLocations();
            }
        }, 120000);
    }


    public static void createJsonForLocations() {

        JSONObject jsonUbicacionesGeneral = new JSONObject();
        String sintaxis = "SELECT * FROM PROMOTOR";
        List<Promotor> listaPromotor = Promotor.findWithQuery(Promotor.class, sintaxis);
        if (listaPromotor.size() > 0) {
            String codigoPromotor = listaPromotor.get(0).getPromCodigo();
            try {
                jsonUbicacionesGeneral.put("codigo", codigoPromotor);

            } catch (JSONException ex) {
                Log.d("LOCATION", ex.getMessage());
            }
        } else {
            Log.d("LOCATION", "NO hay registros");
        }

        String query = "SELECT * FROM UBICACION where sync = 0";
        List<Ubicacion> listaUbicaciones = Ubicacion.findWithQuery(Ubicacion.class, query);
        if (listaUbicaciones.size() > 0) {
            JSONArray arrayUbicaciones = new JSONArray();
            for (int y = 0; y <= listaUbicaciones.size() - 1; y++) {
                JSONObject jsonDatos = new JSONObject();

                try {
                    jsonDatos.put("latitud", listaUbicaciones.get(y).getUbic_Latitud());
                    jsonDatos.put("longitud", listaUbicaciones.get(y).getUbic_Longitud());
                    jsonDatos.put("codigo", listaUbicaciones.get(y).getUbic_PromotorCodigo());
                    jsonDatos.put("fecha", listaUbicaciones.get(y).getUbic_FechaUbicacion());
                    arrayUbicaciones.put(jsonDatos);
                } catch (JSONException ex) {
                    Log.d("LOCATION", ex.getMessage());
                }
            }

            try {
                jsonUbicacionesGeneral.put("ubicaciones", arrayUbicaciones);
            } catch (JSONException ex) {
                Log.d("LOCATION", ex.getMessage());
            }
            requestLocatiosnsToWebServer(jsonUbicacionesGeneral);
        } else {
            Log.d("LOCATION", "No tenemos ubicaciones son sync 0: " + jsonUbicacionesGeneral.toString());
        }
    }
    public static void requestLocatiosnsToWebServer(JSONObject jsonParams) {

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_SAVE_LOCATIONS_10_MINUTES, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (!response.has("error")) {
                                JSONArray jsonSuccess = response.getJSONArray("success");
                                JSONArray jsonFail = response.getJSONArray("fail");
                                if (jsonSuccess.length() > 0) {
                                    for (int i = 0; i <= jsonSuccess.length() - 1; i++) {
                                        JSONObject object = jsonSuccess.getJSONObject(i);
                                        String query = "DELETE FROM UBICACION WHERE fechaubicacion='" + object.getString("fecha") + "'";
                                        Ubicacion.executeQuery(query);
                                    }

                                }

                                if (jsonFail.length() > 0) {
                                    for (int y = 0; y <= jsonFail.length() - 1; y++) {
                                        JSONObject object = jsonFail.getJSONObject(y);
                                        String result = object.getString("RESULTADO");
                                        Log.d(TAG, "onResponse: "+ result);
                                    }
                                } else {
                                    String entroAqui = "";
                                }
                            } else {
                                String error = response.getString("error");
                                Log.d("LOCATION", error);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    @SuppressLint("LongLogTag")
    public static void createJsonForPhotos() {

        JSONObject jsonUbicacionesGeneral = new JSONObject();

        String sintaxis = "SELECT * FROM PROMOTOR";
        List<Promotor> listaPromotor = Promotor.findWithQuery(Promotor.class, sintaxis);
        if (listaPromotor.size() > 0) {
            String codigoPromotor = listaPromotor.get(0).getPromCodigo();
            try {
                jsonUbicacionesGeneral.put("codigo", codigoPromotor);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else {
            Log.d("ERROR PROMOTOR-->", "NO hay registros");
        }

        String queryArchivos = "SELECT * FROM ARCHIVO WHERE sync ='0'";
        List<Archivo> listadoArchivos = Archivo.findWithQuery(Archivo.class, queryArchivos);
        if (listadoArchivos.size() > 0) {
            JSONArray arrayFotos = new JSONArray();
            for (int y = 0; y <= listadoArchivos.size() - 1; y++) {
                JSONObject jsonArchivos = new JSONObject();
                try {
                    jsonArchivos.put("nombre", listadoArchivos.get(y).getArchivoNombre());
                    jsonArchivos.put("foto", listadoArchivos.get(y).getBitmapArchivo());
                    jsonArchivos.put("latitud", listadoArchivos.get(y).getLatitud());
                    jsonArchivos.put("longitud", listadoArchivos.get(y).getLongitud());
                    jsonArchivos.put("fecha_captura", listadoArchivos.get(y).getFecha());
                    jsonArchivos.put("codigo_activacion", listadoArchivos.get(y).getArchivoSolicitudClienteCodigoActivacion());
                    arrayFotos.put(jsonArchivos);
                } catch (JSONException ex) {
                    Log.d("Error -->", ex.toString());
                }
            }
            try {
                jsonUbicacionesGeneral.put("archivos", arrayFotos);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            requestDeImagenesParaServer(jsonUbicacionesGeneral);
        }else
            Log.d(TAG, "createJsonForPhotos: No hay archivos por sincronizar");
    }


    public static void requestDeImagenesParaServer(JSONObject jsonParams) {

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_LOAD_IMAGES_TO_SERVER, jsonParams,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (!response.has("error")) {
                                JSONArray jsonSuccess = response.getJSONArray("success");
                                JSONArray jsonFail = response.getJSONArray("fail");

                                if (jsonSuccess.length() > 0) {
                                    for (int i = 0; i <= jsonSuccess.length() - 1; i++) {
                                        JSONObject object = jsonSuccess.getJSONObject(i);

                                        String queryArchivos = "UPDATE ARCHIVO SET sync ='2' WHERE nombre='" + object.getString("nombre") + "'";
                                        Archivo.executeQuery(queryArchivos);

                                        Log.d(TAG, "onResponse: Response Imagenes: " + response);
                                    }
                                }
                                if (jsonFail.length() > 0) {
                                    for (int y = 0; y <= jsonFail.length() - 1; y++) {
                                        JSONObject object = jsonFail.getJSONObject(y);
                                        Log.d(TAG, "onResponse: FAIL: " + object.getString("RESULTADO"));
                                    }

                                }

                            } else
                                Log.d("Error:-->", response.getString("error"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: Error onResponse: " + e.getMessage());
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public void creacionDeJsonParaRealizarLaCargaDeSolicitudes() {
        JSONArray arrarJSONGeneral = new JSONArray();
        String codigoPromotorFromPreferences="";
        JSONObject jsonSolicitud = new JSONObject();

        String CodigoActivacion="", SolicitudSerie="", ClienteID="";
        String query="SELECT * FROM SOLICITUDCLIENTE where sync = 0";
        List<Solicitud_Cliente> listaSolicitudesCliente = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, query);
        try
        {
            if(!Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN).isEmpty() || Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN) != null)
                codigoPromotorFromPreferences= Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN);

            jsonSolicitud.put("codigo", codigoPromotorFromPreferences);

        }catch(JSONException ex)
        {
            ex.printStackTrace();
        }


        if(listaSolicitudesCliente.size()>0)
        {
            for (int i = 0; i <= listaSolicitudesCliente.size() - 1; i++) {

                JSONObject jsonSolicitudInterna = new JSONObject();
                ClienteID = listaSolicitudesCliente.get(i).getSolCtle_ClienteID();
                SolicitudSerie = listaSolicitudesCliente.get(i).getSolCtle_SolicitudSerie();
                CodigoActivacion = listaSolicitudesCliente.get(i).getSolCtle_CodigoActivacion();

                try
                {
                    jsonSolicitudInterna.put("solicitud", new JSONObject(new Gson().toJson(listaSolicitudesCliente.get(i))));

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


                JSONObject jsonDatosDentroDelCliente = new JSONObject();
                String queryClientes = "SELECT * FROM CLIENTE WHERE id='" + ClienteID + "'";
                List<Cliente> listadoClienteFiltrado = Cliente.findWithQuery(Cliente.class, queryClientes);
                if (listadoClienteFiltrado.size() > 0) {
                    try {

                        jsonDatosDentroDelCliente.put("fechanacimiento", listadoClienteFiltrado.get(0).getFechanacimiento());
                        jsonDatosDentroDelCliente.put("materno", listadoClienteFiltrado.get(0).getMaterno());
                        jsonDatosDentroDelCliente.put("nombre", listadoClienteFiltrado.get(0).getNombre());
                        jsonDatosDentroDelCliente.put("observaciones", listadoClienteFiltrado.get(0).getObservaciones());
                        jsonDatosDentroDelCliente.put("paterno", listadoClienteFiltrado.get(0).getPaterno());
                        jsonDatosDentroDelCliente.put("rfc", listadoClienteFiltrado.get(0).getRfc());
                        jsonDatosDentroDelCliente.put("sync", listadoClienteFiltrado.get(0).getSync());
                        jsonDatosDentroDelCliente.put("telefono", listadoClienteFiltrado.get(0).getTelefono());
                        jsonDatosDentroDelCliente.put("id", listadoClienteFiltrado.get(0).getId());
                        jsonDatosDentroDelCliente.put("estado_civil", listadoClienteFiltrado.get(0).getEstadoCivil());

                        jsonSolicitudInterna.put("cliente", jsonDatosDentroDelCliente);

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }


                String queryUbicacione = "SELECT * FROM UBICACION WHERE solicitudcliente='" + SolicitudSerie + "'";
                List<Ubicacion> listadoUbicaciones = Ubicacion.findWithQuery(Ubicacion.class, queryUbicacione);
                if (listadoUbicaciones.size() > 0) {
                    try {
                        jsonSolicitudInterna.put("ubicacion", new JSONObject(new Gson().toJson(listadoUbicaciones.get(0))));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }

                //----------------------------carga de domicilios para json-------------------------------------------------
                String queryDomicilios = "SELECT * FROM DOMICILIOS WHERE id_cliente_from_suagar='" + ClienteID + "'";
                List<Domicilios> domiciliosList = Domicilios.findWithQuery(Domicilios.class, queryDomicilios);
                JSONArray jsonArrayDomicilios = new JSONArray();

                if (domiciliosList.size() > 0) {

                    for (int y = 0; y <= domiciliosList.size() - 1; y++)
                    {
                        JSONObject jsonDomicilio = new JSONObject();
                        try
                        {
                            jsonDomicilio.put("tipodomicilio", domiciliosList.get(y).getTipoDomicilio());
                            jsonDomicilio.put("calle", domiciliosList.get(y).getCalle());
                            jsonDomicilio.put("colonia", domiciliosList.get(y).getColonia());
                            jsonDomicilio.put("localidad", domiciliosList.get(y).getLocalidad());
                            jsonDomicilio.put("numext", domiciliosList.get(y).getNumeroExterior());
                            jsonDomicilio.put("numint", domiciliosList.get(y).getNumeroInterior());
                            jsonDomicilio.put("entrecalles", domiciliosList.get(y).getEntreCalles());
                            jsonDomicilio.put("localiadcoloniaID", Control.getIDFromLocalidadColonia(domiciliosList.get(y).getColonia(), domiciliosList.get(y).getLocalidad()));
                            jsonDomicilio.put("codigopostal", domiciliosList.get(y).getCodigoPostal());
                            jsonDomicilio.put("estado", "AQUI VA EL ESTADO");
                            jsonDomicilio.put("descripcion", "AQUI VA LA DESCRIPCION");

                            jsonArrayDomicilios.put(jsonDomicilio);
                        }
                        catch (JSONException ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                }
                try {
                    jsonSolicitudInterna.put("domicilios", jsonArrayDomicilios);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
                //-----------------------------------------------------------------------------

                String queryArchivos = "SELECT * FROM ARCHIVO WHERE codigo='" + CodigoActivacion + "'";
                List<Archivo> listadoArchivos = Archivo.findWithQuery(Archivo.class, queryArchivos);

                JSONArray arrayJSON = new JSONArray();
                if (listadoArchivos.size() > 0) {

                    for (int y = 0; y <= listadoArchivos.size() - 1; y++)
                    {
                        JSONObject jsonArchivos = new JSONObject();
                        try
                        {
                            jsonArchivos.put("nombre", listadoArchivos.get(y).getArchivoNombre());
                            jsonArchivos.put("latitud", listadoArchivos.get(y).getLatitud());
                            jsonArchivos.put("longitud", listadoArchivos.get(y).getLongitud());
                            jsonArchivos.put("fecha_captura", listadoArchivos.get(y).getFecha());
                            jsonArchivos.put("codigo_activacion", listadoArchivos.get(y).getArchivoSolicitudClienteCodigoActivacion());
                            arrayJSON.put(jsonArchivos);
                        }
                        catch (JSONException ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                }

                try {
                    jsonSolicitudInterna.put("archivos", arrayJSON);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

                arrarJSONGeneral.put(jsonSolicitudInterna);
            }
            try {
                jsonSolicitud.put("solicitudes", arrarJSONGeneral);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            requestSyncSolicitudestoWebServer(jsonSolicitud);
            Log.d("WS", "Carga de solicitudes a Servidor web: ApplicationResourcesProvider");
        }
        else {
           Log.e("WS", "=======================> Sin datos por actualizar: ApplicationResourcesProvider");
        }
    }
    public void requestSyncSolicitudestoWebServer(JSONObject jsonParams) {
        Log.e("WS", "Parametros: " + jsonParams.toString());
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_CARGA_SOLICITUDES_A_WEB, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String serie="";

                    if (!response.has("error"))
                    {
                        JSONArray jsonSuccess = response.getJSONArray("success");
                        JSONArray jsonFail = response.getJSONArray("fail");


                        if(jsonSuccess.length()>0)
                        {
                            for(int i=0; i<=jsonSuccess.length()-1; i++)
                            {
                                JSONObject object = jsonSuccess.getJSONObject(i);
                                JSONObject solicitud = object.getJSONObject("solicitud");
                                serie = solicitud.getString("solicitudserie");

                                String query="UPDATE SOLICITUDCLIENTE SET sync=2 WHERE solicitudserie='" + serie +"'";
                                Solicitud_Cliente.executeQuery(query);
                                Log.d("RESPUESTA", "aqui");
                            }
                            Log.d("WS", "Json success: ApplicationResourcesProvider:   " + jsonSuccess.toString());
                        }
                        else
                            Log.d("RESPUESTA", "JSON SUCCESS no contiene datos");

                        if (jsonFail.length()>0)
                        {

                            for(int y=0; y<jsonFail.length()-1;y++)
                            {
                                JSONObject object = jsonFail.getJSONObject(y);
                                JSONObject solicitud = object.getJSONObject("solicitud");
                                serie = solicitud.getString("solicitudserie");
                                String result = object.getString("RESULTADO");
                                Log.d("FAIL RESULT-->", result);
                            }
                            Log.d("WS", "JSON Fail: ApplicationResourcesProvider:   " + jsonFail.toString());
                        }
                        else
                            Log.d("RESPUESTA", "JSON FAIL no contiene datos");

                        if(response.has("query"))
                        {
                            try {
                                String queryJSON = response.getString("query");
                                Log.e("ServerQuery", "from ApplicationResourcesProvider " + queryJSON);
                                if (!queryJSON.equals("") && !queryJSON.equals("null")) {
                                    SugarRecord.executeQuery(queryJSON);
                                    response.remove("query");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        else
                        {
                            Log.d("RESPUESTA", "JSON Query no contiene datos");
                        }
                    }
                    else
                    {
                        String error = response.getString("error");
                        Log.d("WS", "Error: ApplicationResourcesProvider:   " + error.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }



    public void creacionDeJsonParaRealizarLaCargaDeNuevasSolicitudes() {
        JSONArray arrayNuevasSolicitudes = new JSONArray();
        JSONObject objetoGeneralJson = new JSONObject();

        String codigoPromotorFromPreferences = "";
        try {
            if (!Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN).isEmpty() || Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN) != null)
                codigoPromotorFromPreferences = Preferences.getPreferenceUsuarioLogin(getApplicationContext(), Preferences.PREFERENCE_USUARIO_LOGIN);

            objetoGeneralJson.put("codigo", codigoPromotorFromPreferences);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        List<SolicitudesNuevas> solicitudesNuevasList = SolicitudesNuevas.findWithQuery(SolicitudesNuevas.class, "SELECT * FROM SOLICITUDES_NUEVAS where sync = '0'");
        if (solicitudesNuevasList.size() > 0) {

            for (int y = 0; y <= solicitudesNuevasList.size() - 1; y++) {
                JSONObject jsonDatos = new JSONObject();
                try {
                    jsonDatos.put("nombre_foto", solicitudesNuevasList.get(y).getNombresolicitud());
                    jsonDatos.put("codigoBarras", solicitudesNuevasList.get(y).getCodigobarras());
                    jsonDatos.put("fecha", solicitudesNuevasList.get(y).getFecha());
                    jsonDatos.put("latitud", solicitudesNuevasList.get(y).getLatitud());
                    jsonDatos.put("longitud", solicitudesNuevasList.get(y).getLongitud());
                    jsonDatos.put("codigoActivacion", solicitudesNuevasList.get(y).getCodigoactivacion());
                    jsonDatos.put("inversionInicial", solicitudesNuevasList.get(y).getInversion());
                    arrayNuevasSolicitudes.put(jsonDatos);
                } catch (JSONException ex) {
                    Log.d("LOCATION", ex.getMessage());
                }
            }
            try {
                objetoGeneralJson.put("solicitudes", arrayNuevasSolicitudes);

                requestSyncNewSolicitudesToWebServer(objetoGeneralJson);
            } catch (JSONException ex) {
                Log.d("LOCATION", ex.getMessage());
            }
        }

    }
    public void requestSyncNewSolicitudesToWebServer(JSONObject jsonParams) {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL.URL_LOAD_NEW_SOLICITUDES_TO_WEB, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (!response.has("error"))
                    {
                        JSONArray jsonSuccess = response.getJSONArray("success");
                        JSONArray jsonFail = response.getJSONArray("fail");


                        if(jsonSuccess.length()>0)
                        {
                            for(int i=0; i<=jsonSuccess.length()-1; i++) {
                                JSONObject object = jsonSuccess.getJSONObject(i);
                                String query="UPDATE SOLICITUDES_NUEVAS SET sync=2 WHERE fecha='" + object.getString("fecha") +"'";
                                SolicitudesNuevas.executeQuery(query);

                            }
                        }
                        else
                            Log.d("RESPUESTA", "JSON SUCCESS no contiene datos");

                        if (jsonFail.length()>0)
                        {

                            for(int y=0; y<jsonFail.length()-1;y++)
                            {
                                JSONObject object = jsonFail.getJSONObject(y);
                                String result = object.getString("RESULTADO");
                                Log.d("FAIL RESULT-->", result);
                            }
                        }
                        else
                            Log.d("RESPUESTA", "JSON FAIL no contiene datos");

                        if(response.has("query"))
                        {
                            try {
                                String queryJSON = response.getString("query");
                                Log.e("ServerQuery", "from ApplicationResourcesProvider " + queryJSON);
                                if (!queryJSON.equals("") && !queryJSON.equals("null")) {
                                    SugarRecord.executeQuery(queryJSON);
                                    response.remove("query");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        else
                        {
                            Log.d("RESPUESTA", "JSON Query no contiene datos");
                        }
                    }
                    else
                    {
                        String error = response.getString("error");
                        Log.d("WS", "Error: ApplicationResourcesProvider:   " + error.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(900000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }






/*


 */

    public void timerForInsertLocations() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @SuppressLint("LongLogTag")
            public void run() {
                timerForInsertLocations();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    Control.insertarUbicaciones(
                            null,
                            Control.getPromotorCodigo(),
                            ""+ latitud,
                            ""+ longitud,
                            "" +  dateFormat.format( new Date()),
                                    0);

                    Log.d(TAG, "Ubicación insertada: " + latitud + ", " + longitud);
            }
        }, 17000);
    }






    /**************************************************
     * Nueva modificacion
     */

    static boolean limiteDeTiempoParaEvaluarUbicacionDeCobro(Date fechaSistema, long fechaUbicacion) {
        boolean correcto = false;
        long diferencia = (Math.abs(fechaSistema.getTime() - fechaUbicacion)) / 1000;
        long limit = (60 * 1000) / 1000L;
        if (diferencia <= limit) {
            correcto = true;
        }
        return correcto;
    }


    void timerCallLocations()
    {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @SuppressLint("LongLogTag")
            public void run() {
                timerCallLocations();
                try {
                    updateLocation();
                }catch (Throwable e)
                {
                    Log.d(TAG, e.getMessage());
                }
            }
        }, 60000);
    }

    private void updateLocation() {
        buildLocationRequest();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, getPendingIntent());
        getCurrentLocation();

    }

    public void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();
                }
            }
        });

    }

    private PendingIntent getPendingIntent(){
        Intent intent = new Intent(this, MyLocationService.class);
        intent.setAction(MyLocationService.ACTION_PROCESS_UPDATE);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void buildLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(12000);
        locationRequest.setFastestInterval(9000);
        locationRequest.setSmallestDisplacement(10f);
    }


    @SuppressLint("LongLogTag")
    public static void updateCoordenadasLocation(Location loc, boolean error_location_form_service)
    {
        try {
            if (error_location_form_service) {
                error_code_location = 600;
                if (limiteDeTiempoParaEvaluarUbicacionDeCobro(date = new Date(), Long.parseLong(timeLocation)))
                    error_code_location = 700;
            } else {
                if (latitud == loc.getLatitude() && longitud == loc.getLongitude()) {
                    if (limiteDeTiempoParaEvaluarUbicacionDeCobro(date = new Date(), loc.getTime()))
                        error_code_location = 200;
                    else
                        error_code_location = 400;
                } else {
                    if (limiteDeTiempoParaEvaluarUbicacionDeCobro(date = new Date(), loc.getTime()))
                        error_code_location = 50;
                    else
                        error_code_location = 300;

                    latitud = loc.getLatitude();
                    longitud = loc.getLongitude();
                    timeLocation = String.valueOf(loc.getTime());

                    gps_provider = loc.getProvider();
                    if (beforeLoc != null)
                        distance = loc.distanceTo(beforeLoc);
                    else
                        distance = 0f;

                    beforeLoc = loc;
                }
                Log.d(TAG, "Ubicación obtenida: " + "  latitud: " + String.valueOf(loc.getLatitude()) + "  longitud: " + String.valueOf(loc.getLongitude()));
            }
        } catch (Throwable e) {
            error_code_location = 100;
            Log.d(TAG, "No se obtuvo ubicacion");
        }
    }


    public static String[] getCoordenadasFromApplication() {
        String[] coordenadas = new String[9];
        try
        {
            coordenadas[0]= String.valueOf(latitud);
            coordenadas[1]= String.valueOf(longitud);
            coordenadas[2]= String.valueOf(timeLocation);
            coordenadas[3]= String.valueOf(error_code_location);
            coordenadas[4]= gps_provider;
            coordenadas[5]= String.valueOf(distance);

        }catch (Throwable e)
        {
            Log.e("TA", "catch getCoordenadasFromApplication()" + e.getMessage().toString());
        }
        return coordenadas;
    }




}