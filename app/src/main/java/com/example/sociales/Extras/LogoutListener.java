package com.example.sociales.Extras;

public interface LogoutListener
{
    void onSessionLogout();
}
