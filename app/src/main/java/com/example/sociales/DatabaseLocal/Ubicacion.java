package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Ubicacion extends SugarRecord
{
    public String  solicitudcliente, promotorcodigo, latitud, longitud, fechaubicacion;
    public int sync;

    public Ubicacion() {
    }

    public Ubicacion( String ubic_SolicitudClienteID, String ubic_PromotorCodigo,
                      String ubic_Latitud, String ubic_Longitud, String ubic_FechaUbicacion, int sync)
    {

        this.solicitudcliente = ubic_SolicitudClienteID;
        this.promotorcodigo = ubic_PromotorCodigo;
        this.latitud = ubic_Latitud;
        this.longitud = ubic_Longitud;
        this.fechaubicacion = ubic_FechaUbicacion;
        //this.ubic_FechaRegistro = ubic_FechaRegistro;
        this.sync = sync;
    }


    public String getUbic_SolicitudClienteID() {
        return solicitudcliente;
    }

    public void setUbic_SolicitudClienteID(String ubic_SolicitudClienteID) {
        this.solicitudcliente = ubic_SolicitudClienteID;
    }

    public String getUbic_PromotorCodigo() {
        return promotorcodigo;
    }

    public void setUbic_PromotorID(String ubic_PromotorCodigo) {
        this.promotorcodigo = ubic_PromotorCodigo;
    }

    public String getUbic_Latitud() {
        return latitud;
    }

    public void setUbic_Latitud(String ubic_Latitud) {
        this.latitud = ubic_Latitud;
    }

    public String getUbic_Longitud() {
        return longitud;
    }

    public void setUbic_Longitud(String ubic_Longitud) {
        this.longitud = ubic_Longitud;
    }

    public String getUbic_FechaUbicacion() {
        return fechaubicacion;
    }

    public void setUbic_FechaUbicacion(String ubic_FechaUbicacion) {
        this.fechaubicacion = ubic_FechaUbicacion;
    }





    /*public String getUbic_FechaRegistro() {
        return ubic_FechaRegistro;
    }

    public void setUbic_FechaRegistro(String ubic_FechaRegistro) {
        this.ubic_FechaRegistro = ubic_FechaRegistro;
    }*/

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
