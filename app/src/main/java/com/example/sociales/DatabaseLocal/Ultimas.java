package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Ultimas extends SugarRecord
{
    String codigoactivacion="", solicitud="";

    public Ultimas() {
    }

    public Ultimas(String codigoactivacion, String solicitud) {
        this.codigoactivacion = codigoactivacion;
        this.solicitud = solicitud;
    }

    public String getCodigoactivacion() {
        return codigoactivacion;
    }

    public void setCodigoactivacion(String codigoactivacion) {
        this.codigoactivacion = codigoactivacion;
    }

    public String getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(String solicitud) {
        this.solicitud = solicitud;
    }
}
