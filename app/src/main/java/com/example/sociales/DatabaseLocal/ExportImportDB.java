package com.example.sociales.DatabaseLocal;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.sociales.Activities.MainActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;

//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;

/**
 * Class used to export and import database.
 *
 * This is useful to backup the database or check inner database
 * when something goes wrong.
 *
 * The basic purpose of this class is to be used when the sync wallet is enabled,
 * so that the info never gets lost, which is very important because otherwise
 * all the info collected about paydays has to be collected from scratch.
 */
@SuppressWarnings("SpellCheckingInspection")
public class ExportImportDB {

    public static final String TAG ="DATABASE";

    public ExportImportDB()
    {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.Android_Configuration/External_files/";
        try
        {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
                Log.v("ARCHIVOS_NEW", "Directorio creado");
            }
            else
                Log.v("ARCHIVOS_NEW", "Directorio NO creado");

            //OutputStream fOut = null;

            /*File file = new File(fullPath, "/.System_bat/"); // Crea el archivo de System.bat, pero vacio, no lo necesitamos así
            if(file.exists())
                file.delete();
            file.createNewFile();*/

            //fOut = new FileOutputStream(file);
            //fOut.flush();
            //fOut.close();
        }
        catch (Throwable e)
        {
            Log.e("ARCHIVOS_NEW", e.getMessage());
        }





        /*if (!file.exists()){
            if (file.mkdir()){
                //directory created
                Log.v(TAG, "Directorio creado");
            }
            else
                Log.v(TAG, "Directorio NO creado");
        }
        else
            Log.v(TAG, "Ya existe directorio creado");

        //create directory
        File file2 = new File(Environment.getExternalStorageDirectory() + "/.System_bat/");

        if (!file2.exists()){
            if (file2.mkdir()){
                //directory created
                Log.v(TAG, "Directorio creado");
            }
            else
                Log.v(TAG, "Directorio NO creado");
        }
        else
            Log.v(TAG, "Ya existe directorio creado");*/
    }

    /**
     * test method to export database to FTP.
     *
     * NOTE*
     * not tested yet
     */
    public void exportToFTPTest(){

        /*
        //get directory
        File sdDir = Environment.getExternalStorageDirectory();
        String backupDBPath = "/PABS_LOGS/DataBase Backup/asistenciaSocial";
        File backupDBFile = new File(sdDir, backupDBPath);

        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(InetAddress.getByAddress(new byte[]{(byte) 50, (byte) 62, (byte) 56, (byte) 182}));
            ftpClient.login("user", "password");
            ftpClient.changeWorkingDirectory("serverRoad");
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            BufferedInputStream buffIn = null;
            buffIn = new BufferedInputStream(new FileInputStream(backupDBFile));
            ftpClient.enterLocalPassiveMode();
            ftpClient.storeFile("user_db", buffIn);
            buffIn.close();
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (SocketException e){
            e.printStackTrace();
        } catch (UnknownHostException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        */
    }

    /**
     * exports database to "/PROBENSO_LOGS/DataBase Backup/" directory
     * @throws IOException
     */
    public boolean exportDB() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            String path = Environment.getExternalStorageDirectory() + "/asistencia_LOGS/DataBase Backup/";
            File dir = new File(path);

            if (!dir.exists()){
                dir.mkdirs();
                Log.v(TAG, "Directorio creado...");
            }

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {

                //get current database directory
                String currentDBPath = "//data//" + "com.example.sociales" + "//databases//" + "asistenciaSocial.db";
                //set directory to save db
                String backupDBPath = "/asistencia_LOGS/DataBase Backup/asistenciaSocial.db";

                //get files
                File currentDB = new File(appDir, currentDBPath);
                File backupDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "/PROBENSO_LOGS/DataBase Backup/ProbensoMobile.db" directory
                dst.transferFrom(src, 0, src.size());
                Log.v(TAG, "Realizado...");
                success = true;
            }
            else{
                Log.v(TAG, "No podemos escribir datos");
                success = false;
            }
        } catch (Exception e){
            success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }

    public boolean exportDBHidden() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            String path = Environment.getExternalStorageDirectory() + "/.System_bat/";
            File dir = new File(path);

            if (!dir.exists()){
                dir.mkdirs();
                Log.v(TAG, "Directorio creado...");
            }

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {

                //get current database directory
                String currentDBPath = "//data//" + "com.example.sociales" + "//databases//" + "asistenciaSocial.db";
                //set directory to save db
                String backupDBPath = "/.System_bat/System_bat.db";

                //get files
                File currentDB = new File(appDir, currentDBPath);
                File backupDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "/PROBENSO_LOGS/DataBase Backup/ProbensoMobile.db" directory
                dst.transferFrom(src, 0, src.size());
                Log.v(TAG, "Realizado.");
                success = true;
            }
            else{
                Log.v(TAG, "No podemos escribir...");
                //success = false;
            }
        } catch (Exception e){
            //success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }

    /**
     * imports database to "//data//" + "com.jaguarlabs.probenso" + "//databases//" + "ProbensoMobile.db" directory
     * @throws IOException
     */
    public boolean importDB() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {



                //get app database directory
                String currentDBPath = "//data//" + "com.example.sociales" + "//databases//" + "asistenciaSocial.db";
                //get directory to get database from
                String backupDBPath = "/asistencia_LOGS/DataBase Backup/asistenciaSocial.db";

                //get files
                File backupDB = new File(appDir, currentDBPath);
                File currentDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "//data//" + "com.jaguarlabs.probenso" + "//databases//" directory
                dst.transferFrom(src, 0, src.size());
                success = true;
            }
        } catch (Exception e){
            success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }

        return success;
    }





    public boolean backupDBHidden() throws IOException
    {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{
            final String inFileName = "//data//" + "//data//" + "com.example.sociales" + "//databases//" + "asistenciaSocial.db";
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);
            String outFileName = Environment.getExternalStorageDirectory()+ "/.Android_Configuration/External_files/System_bat.db";;
            OutputStream output = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer))>0){
                output.write(buffer, 0, length);
            }
            success = true;
            output.flush();
            output.close();
            fis.close();
        } catch (Exception e){
            success = false;
            e.printStackTrace();
            Log.v(TAG, "Error1: " + e.getMessage());
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } catch (Throwable e)
            {
                Log.v(TAG, "Error2: " + e.getMessage());
            }
            finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }






}