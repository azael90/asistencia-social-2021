package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Solicitud_Cliente extends SugarRecord
{
    public String codigoactivacion="", solicitudserie="", clienteid="", nuevoingreso="", esquemapagoid="", fechasolicitud="", inversioninicial="";
    public int sync=0;

    public Solicitud_Cliente() {
    }

    public Solicitud_Cliente(String solCtle_CodigoActivacion, String solCtle_SolicitudSerie,
                             String solCtle_ClienteID, String solCtle_NuevoIngreso, String solCtle_EsquemaPagoID,
                             String solCtle_FechaSolicitud, String inversioninicial, int sync)
    {

        this.codigoactivacion = solCtle_CodigoActivacion;
        this.solicitudserie = solCtle_SolicitudSerie;
        this.clienteid = solCtle_ClienteID;
        this.nuevoingreso = solCtle_NuevoIngreso;
        this.esquemapagoid = solCtle_EsquemaPagoID;
        this.fechasolicitud = solCtle_FechaSolicitud;
        this.inversioninicial = inversioninicial;
        this.sync = sync;
    }

    public String getSolCtle_CodigoActivacion() {
        return codigoactivacion;
    }

    public void setSolCtle_CodigoActivacion(String solCtle_CodigoActivacion) {
        this.codigoactivacion = solCtle_CodigoActivacion;
    }

    public String getSolCtle_SolicitudSerie() {
        return solicitudserie;
    }

    public void setSolCtle_SolicitudSerie(String solCtle_SolicitudSerie) {
        this.solicitudserie = solCtle_SolicitudSerie;
    }

    public String getSolCtle_ClienteID() {
        return clienteid;
    }

    public void setSolCtle_ClienteID(String solCtle_ClienteID) {
        this.clienteid = solCtle_ClienteID;
    }

    public String getSolCtle_NuevoIngreso() {
        return nuevoingreso;
    }

    public void setSolCtle_NuevoIngreso(String solCtle_NuevoIngreso) {
        this.nuevoingreso = solCtle_NuevoIngreso;
    }

    public String getSolCtle_EsquemaPagoID() {
        return esquemapagoid;
    }

    public void setSolCtle_EsquemaPagoID(String solCtle_EsquemaPagoID) {
        this.esquemapagoid = solCtle_EsquemaPagoID;
    }

    public String getSolCtle_FechaSolicitud() {
        return fechasolicitud;
    }

    public void setSolCtle_FechaSolicitud(String solCtle_FechaSolicitud) {
        this.fechasolicitud = solCtle_FechaSolicitud;
    }


    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }


    public String getInversioninicial() {
        return inversioninicial;
    }

    public void setInversioninicial(String inversioninicial) {
        this.inversioninicial = inversioninicial;
    }
}
