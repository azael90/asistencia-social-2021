package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Archivo extends SugarRecord
{
    String codigo="", tipo="", nombre="", bitmap="", latitud="", longitud="", fecha="";
    int sync;

    public Archivo() {
    }

    public Archivo(String archivoSolicitudClienteCodigoActivacion, String tipo, String nombre, String bitmap, int sync, String latitud, String longitud, String fecha) {
        this.codigo = archivoSolicitudClienteCodigoActivacion;
        this.tipo = tipo;
        this.nombre = nombre;
        this.bitmap= bitmap;
        this.sync = sync;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha = fecha;
    }


    public String getArchivoSolicitudClienteCodigoActivacion() {
        return codigo;
    }

    public void setArchivoSolicitudClienteCodigoActivacion(String archivoSolicitudClienteCodigoActivacion) {
        this.codigo = archivoSolicitudClienteCodigoActivacion;
    }

    public String getArchivoTipoArchivo() {
        return tipo;
    }

    public void setArchivoTipoArchivo(String archivoTipoArchivo) {
        this.tipo = archivoTipoArchivo;
    }

    public String getArchivoNombre() {
        return nombre;
    }

    public void setArchivoNombre(String archivoNombre) {
        this.nombre = archivoNombre;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getBitmapArchivo() {
        return bitmap;
    }

    public void setBitmapArchivo(String bitmapArchivo) {
        this.bitmap = bitmapArchivo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
