package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class FotografiasPorSolicitudDeCliente  extends SugarRecord
{
    public String nosolicitud="", codigoactivacion="", nombrefoto="";


    public FotografiasPorSolicitudDeCliente() {
    }


    public FotografiasPorSolicitudDeCliente(String nosolicitud, String codigoactivacion, String nombrefoto) {
        this.nosolicitud = nosolicitud;
        this.codigoactivacion = codigoactivacion;
        this.nombrefoto = nombrefoto;
    }

    public String getNosolicitud() {
        return nosolicitud;
    }

    public void setNosolicitud(String nosolicitud) {
        this.nosolicitud = nosolicitud;
    }

    public String getCodigoactivacion() {
        return codigoactivacion;
    }

    public void setCodigoactivacion(String codigoactivacion) {
        this.codigoactivacion = codigoactivacion;
    }

    public String getNombrefoto() {
        return nombrefoto;
    }

    public void setNombrefoto(String nombrefoto) {
        this.nombrefoto = nombrefoto;
    }
}
