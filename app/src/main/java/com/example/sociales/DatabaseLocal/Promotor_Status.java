package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Promotor_Status extends SugarRecord
{
    public String  promEst_PromotorCodigo, promEst_AndroidID;
    public int sync=0;

    public Promotor_Status() {
    }

    public Promotor_Status( String promEst_PromotorCodigo, String promEst_AndroidID, int sync)
    {

        this.promEst_PromotorCodigo = promEst_PromotorCodigo;
        this.promEst_AndroidID = promEst_AndroidID;
        this.sync = sync;
    }


    public String getPromEst_PromotorCodigo() {
        return promEst_PromotorCodigo;
    }

    public void setPromEst_PromotorCodigo(String promEst_PromotorCodigo) {
        this.promEst_PromotorCodigo = promEst_PromotorCodigo;
    }

    public String getPromEst_AndroidID() {
        return promEst_AndroidID;
    }

    public void setPromEst_AndroidID(String promEst_AndroidID) {
        this.promEst_AndroidID = promEst_AndroidID;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
