package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Cliente extends SugarRecord
{
    public String  nombre, paterno, materno, fechanacimiento, telefono, rfc, observaciones, estadoCivil;
    public int sync=0;

    public Cliente() {
    }

    public Cliente(String nombre, String paterno, String materno, String fechanacimiento, String telefono, String rfc, String observaciones, int sync, String estadoCivil) {
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.fechanacimiento = fechanacimiento;
        this.telefono = telefono;
        this.rfc = rfc;
        this.observaciones = observaciones;
        this.sync = sync;
        this.estadoCivil = estadoCivil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
}
