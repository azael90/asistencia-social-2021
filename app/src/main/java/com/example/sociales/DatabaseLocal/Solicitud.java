package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Solicitud extends SugarRecord
{
    public String sol_Serie, sol_PromotorCodigo, tipoSolicitud, letraSolicitud, usada;
    public int sync=0;

    public Solicitud() {
    }

    public Solicitud(String sol_Serie, String sol_PromotorCodigo, String tipoSolicitud, String letraSolicitud, String usada, int sync) {
        this.sol_Serie = sol_Serie;
        this.sol_PromotorCodigo = sol_PromotorCodigo;
        this.tipoSolicitud= tipoSolicitud;
        this.letraSolicitud= letraSolicitud;
        this.usada=usada;
        this.sync = sync;
    }

    public String getSol_Serie() {
        return sol_Serie;
    }

    public void setSol_Serie(String sol_Serie) {
        this.sol_Serie = sol_Serie;
    }

    public String getSol_PromotorCodigo() {
        return sol_PromotorCodigo;
    }

    public void setSol_PromotorCodigo(String sol_PromotorCodigo) {
        this.sol_PromotorCodigo = sol_PromotorCodigo;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }


    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public String getLetraSolicitud() {
        return letraSolicitud;
    }

    public void setLetraSolicitud(String letraSolicitud) {
        this.letraSolicitud = letraSolicitud;
    }

    public String getUsada() {
        return usada;
    }

    public void setUsada(String usada) {
        this.usada = usada;
    }
}
