package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class EsquemaPago extends SugarRecord
{
    public String EsqPagDescripcion;
    public int sync=0;

    public EsquemaPago() {
    }

    public EsquemaPago(String esqPagDescripcion, int sync)
    {
        this.EsqPagDescripcion = esqPagDescripcion;
        this.sync=sync;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getEsqPagDescripcion() {
        return EsqPagDescripcion;
    }

    public void setEsqPagDescripcion(String esqPagDescripcion) {
        EsqPagDescripcion = esqPagDescripcion;
    }
}
