package com.example.sociales.DatabaseLocal;

import android.content.Context;
import android.util.Log;

import com.example.sociales.Activities.SolicitudAndScanner;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import java.util.ArrayList;
import java.util.List;

public class Control
{


    private static final String TAG = "CONTROL";

    public static void insertarPromotor(String promCodigo, String promNombre, String promEfectividad,
                                        String promEst_AndroidID, String promEst_Contraseña,
                                        String promEst_Permisos, String promEst_Estatus, String promEst_ActualizarApp, String tipoPromotor) {
        Promotor promotor = new Promotor(promCodigo,
                promNombre,
                promEfectividad,
                promEst_AndroidID,
                promEst_Contraseña,
                promEst_Permisos,
                promEst_Estatus,
                promEst_ActualizarApp,
                0,
                tipoPromotor
        );
                promotor.save();
    }

    public static void insertarPromotor_Status(String promEst_PromotorCodigo, String promEst_AndroidID) {
        /*for (int i = 0; i <1; i++) {
            Promotor_Status promotorStatus = new Promotor_Status(promEst_PromotorCodigo, promEst_AndroidID,  i);
            promotorStatus.save();
        }*/
        Promotor_Status promotorStatus = new Promotor_Status(promEst_PromotorCodigo, promEst_AndroidID,  0);
                promotorStatus.save();
    }

    public static void insertarOficinas(String nombre) {
        Oficinas oficina = new Oficinas(nombre);
        oficina.save();
    }


    public static void insertarNuevasSolicitudes(String nombresolicitud, String fotoSolicitud, String codigoDeBarras, String fecha, String latitud, String longitud, String codigoactivacion, String inversion) {
        SolicitudesNuevas solicitudNueva = new SolicitudesNuevas(nombresolicitud, fotoSolicitud, codigoDeBarras, fecha, latitud, longitud, "0", codigoactivacion, inversion);
        solicitudNueva.save();

        String query = "UPDATE SOLICITUD set usada ='1' WHERE solSerie= '" + codigoDeBarras + "'";
        Solicitud.executeQuery(query);
    }



    public static void insertarSolicitud(String sol_Serie, String sol_PromotorCodigo, String tipoSolicitud, String letraIndice, String usada) {
        /*for (int i = 0; i <1; i++) {

            Solicitud solicitud = new Solicitud(sol_Serie, sol_PromotorCodigo, i);
            solicitud.save();
        }*/
        Solicitud solicitud = new Solicitud(sol_Serie, sol_PromotorCodigo, tipoSolicitud, letraIndice, usada, 4);
        solicitud.save();
    }

    public static void insertarSolicitudCliente(String solCtle_CodigoActivacion, String solCtle_SolicitudSerie,
                                                String solCtle_ClienteID, String solCtle_NuevoIngreso, String solCtle_EsquemaPagoID,
                                                String solCtle_FechaSolicitud, String inversionInicial, int sync) {

        Solicitud_Cliente solicitudCliente = new Solicitud_Cliente(solCtle_CodigoActivacion, solCtle_SolicitudSerie, solCtle_ClienteID,
                               solCtle_NuevoIngreso, solCtle_EsquemaPagoID, solCtle_FechaSolicitud, inversionInicial, sync);
                solicitudCliente.save();
    }


    /*
    public static void insertarUltimos(String codigo, String solicitud)
    {
        Ultimas ultimas = new Ultimas(codigo, solicitud);
        ultimas.save();
    }
    */

    public static void insertarUbicaciones(String solicitud, String ubic_PromotorCodigo, String ubic_Latitud, String ubic_Longitud, String ubic_FechaUbicacion, int sync)
    {
        try {
            Ubicacion ubicacion = new Ubicacion(solicitud, ubic_PromotorCodigo, ubic_Latitud, ubic_Longitud, ubic_FechaUbicacion, sync);
            ubicacion.save();
        }catch (Throwable e)
        {
            Log.e("Error_insert", e.getMessage());
        }
    }





    public static void insertarLocalidadColonia(String locColLocalidad, String locColColonia, int locCol_id_web)
    {
        /*for (int i = 0; i <1; i++) {
            Localidad_Colonia localidad = new Localidad_Colonia(locColLocalidad, locColColonia, i);
            localidad.save();
        }*/
        Localidad_Colonia localidad = new Localidad_Colonia(locColLocalidad, locColColonia, locCol_id_web, 0);
        localidad.save();
    }


    public static void insertarCliente(String nombre, String paterno, String materno,
                                       String cliente_FechaNac, String cliente_Telefono, String cliente_RFC, String cliente_Observaciones, String estadoCivil)
    {
        Cliente cliente = new Cliente(
                ""+nombre,
                ""+paterno,
                ""+materno,
                ""+cliente_FechaNac,
                ""+cliente_Telefono,
                ""+cliente_RFC,
                ""+cliente_Observaciones,
                0,
                ""+estadoCivil);
        cliente.save();
    }

    public static void insertarDomicilios(String idClienteFromSuagar, String calle, String numeroExterior,
                                          String numeroInterior, String colonia, String localidad,
                                          String codigoPostal, String entreCalles, String tipoDomicilio)
    {
        Domicilios domicilios = new Domicilios(
                ""+idClienteFromSuagar,
                ""+calle,
                ""+numeroExterior,
                ""+numeroInterior,
                ""+colonia,
                ""+localidad,
                ""+codigoPostal,
                ""+entreCalles,
                ""+tipoDomicilio,
                0);
        domicilios.save();
    }



    public static void insertarEsquemaDePago(String ubic_PromotorCodigo)
    {
        /*for (int i = 0; i <1; i++)
        {
            EsquemaPago esquema = new EsquemaPago(ubic_PromotorCodigo, i);
            esquema.save();
        }*/
        EsquemaPago esquema = new EsquemaPago(ubic_PromotorCodigo, 0);
                esquema.save();
    }

    public static void insertarArchivo(String  archivoSolicitudClienteCodigoActivacion, String archivoTipoArchivo,
                                       String archivoNombre, String bitmapArchivo, int sync, String latitud, String longitud, String fecha)
    {

        Archivo archivo = new Archivo(archivoSolicitudClienteCodigoActivacion, archivoTipoArchivo, archivoNombre, bitmapArchivo, sync, latitud, longitud, fecha);
        archivo.save();

    }


    public static void insertarFotografiasPorSolicitud(String  solicitud, String codigoActivacion, String nombreFoto)
    {

        FotografiasPorSolicitudDeCliente fotosporSol = new FotografiasPorSolicitudDeCliente(solicitud, codigoActivacion, nombreFoto);
        fotosporSol.save();

    }

    public static int getLastLocalidadColoniaInserted()
    {
        //List<Localidad_Colonia> localidadColonia = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, "SELECT TOP 1 * FROM LOCALIDAD_COLONIA ORDER BY locaColIDWEB ");
        List<Localidad_Colonia> localidadColonia = Localidad_Colonia.find(Localidad_Colonia.class, null, null, null, "idweb DESC", "1");
        if ( localidadColonia.size() > 0 ) {
            return localidadColonia.get(0).getIdweb();
        }
        return 0;
    }


    public static String getPromotorCodigo() {
            List<Promotor> promotor = Promotor.listAll(Promotor.class);
            if ( promotor.size() > 0 ){
                    return promotor.get(0).getPromCodigo();
                }
            return "Sin Codigo";
        }


    public static long getLastIDCLIENT()
{
    List<Cliente> listaCliente = Cliente.find(Cliente.class, null, null, null, "id DESC", "1");
    if ( listaCliente.size() > 0 ) {
        return listaCliente.get(0).getId();
    }
    return 0;
}

    public static String getLastSolicitudCliente()
    {
        List<Solicitud_Cliente> listaCliente = Solicitud_Cliente.find(Solicitud_Cliente.class, null, null, null, "id DESC", "1");
        if ( listaCliente.size() > 0 ) {
            return listaCliente.get(0).getSolCtle_CodigoActivacion();
        }
        return "-";
    }

    public static String getLastFechaSolicitud()
    {
        List<Solicitud_Cliente> listaCliente = Solicitud_Cliente.find(Solicitud_Cliente.class, null, null, null, "id DESC", "1");
        if ( listaCliente.size() > 0 ) {
            return listaCliente.get(0).getSolCtle_FechaSolicitud();
        }
        return "Sin Fecha";
    }

    public static void insertarPublicidadd(String imagen, String url)
    {
        /*for (int i = 0; i <1; i++)
        {
            EsquemaPago esquema = new EsquemaPago(ubic_PromotorCodigo, i);
            esquema.save();
        }*/
        publicidad ima = new publicidad(imagen, url);
        ima.save();
    }

    public static String getCurrentPromotorCode() {
        String query = "SELECT * FROM PROMOTOR";
        List<Promotor> promotorInfo = Promotor.findWithQuery(Promotor.class, query);

        if( promotorInfo.size() > 0 ){
            return promotorInfo.get(0).getPromCodigo();
        }
        return "";
    }







    public static String ultimaSoliciudFecha()
    {
        List<Solicitud_Cliente> listaCliente = Solicitud_Cliente.find(Solicitud_Cliente.class, null, null, null, "id DESC", "1");
        if ( listaCliente.size() > 0 ) {
            return listaCliente.get(0).getSolCtle_FechaSolicitud();
        }
        return "-";
    }


    public static String getIDFromLocalidadColonia(String colonia, String localidad)
    {
        String query="SELECT * FROM LOCALIDADCOLONIA WHERE localidad='" + localidad + "' and colonia= '" + colonia + "'";
        List<Localidad_Colonia> localidad_coloniaList = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if ( localidad_coloniaList.size() > 0 )
        {
            return String.valueOf(localidad_coloniaList.get(0).getIdweb());
        }
        return "-";
    }

    public static ArrayList<String> getNombreFotografiasPorSolicitudRegistrada(String solicitudCliente)
    {
        ArrayList<String> listaDeFotosPorSolicitud = new ArrayList<String>();
        String query="SELECT * FROM FOTOGRAFIAS_POR_SOLICITUD_DE_CLIENTE WHERE nosolicitud='" + solicitudCliente + "'";
        List<FotografiasPorSolicitudDeCliente> fotosList = FotografiasPorSolicitudDeCliente.findWithQuery(FotografiasPorSolicitudDeCliente.class, query);
        if(fotosList.size()>0)
        {
            for (int i = 0; i < fotosList.size(); i++) {
                try {
                    listaDeFotosPorSolicitud.add(fotosList.get(i).getNombrefoto());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            Log.i("FOTOGRAFIA", "SIN REGISTROS getNombreFotografiasPorSolicitudRegistrada");
        }

        return listaDeFotosPorSolicitud;
    }





    public static void insertarArchivosGuardados(String nombreArchivo, String fecha , String url)
    {
        ArchivosDescargados archivosDescargados = new ArchivosDescargados(nombreArchivo, fecha, url);
        archivosDescargados.save();
    }








    public static boolean verificarSiTenemosUnArchivoConElMismoNombre(String nombre)
    {
        boolean tenemosUnArchivoConElMismoNombre= false;
        String query = "SELECT * FROM ARCHIVOS_DESCARGADOS WHERE nombre_archivo ='" + nombre + "'";
        List<ArchivosDescargados> listaArchivos = ArchivosDescargados.findWithQuery(ArchivosDescargados.class, query);
        if (listaArchivos.size() > 0)
            tenemosUnArchivoConElMismoNombre=true;

        return tenemosUnArchivoConElMismoNombre;
    }

    public static void actualizarArchivo(String nombreArchivo, String fecha , String url)
    {
        String queryBorrado="DELETE FROM ARCHIVOS_DESCARGADOS WHERE nombre_archivo='" + nombreArchivo +"'";
        ArchivosDescargados.executeQuery(queryBorrado);
        //insertarArchivosGuardados(nombreArchivo, fecha, url);
        String queryUpdate="UPDATE ARCHIVOS_DESCARGADOS SET nombre_archivo ='"+nombreArchivo+"', fecha ='"+fecha+"', url='"+url+"' WHERE nombre_archivo='" + nombreArchivo +"'";
        ArchivosDescargados.executeQuery(queryUpdate);
    }
    //buscarSiLaColoniaCoincideConUnaColoniaDeLaBaseDeDatos

    public static boolean buscarSiLaColoniaCoincideConUnaColoniaDeLaBaseDeDatos(String colonia)
    {
        boolean tenemosColonia= false;
        String query="SELECT * FROM LOCALIDADCOLONIA WHERE colonia= '" + colonia + "'";
        List<Localidad_Colonia> listaDeColonias = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if (listaDeColonias.size() > 0)
            tenemosColonia=true;

        return tenemosColonia;
    }

    public static boolean buscarSiLaLocalidadCoincideConUnaLocalidadDeLaBaseDeDatos(String localidad)
    {
        boolean tenemosColonia= false;
        String query="SELECT * FROM LOCALIDADCOLONIA WHERE localidad= '" + localidad + "'";
        List<Localidad_Colonia> listaDeLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if (listaDeLocalidades.size() > 0)
            tenemosColonia=true;

        return tenemosColonia;
    }

    public static void resetDatabase( Context context ) {
        SugarContext.terminate();
        SchemaGenerator schemaGenerator = new SchemaGenerator( context );
        schemaGenerator.deleteTables(new SugarDb( context ).getDB());
        SugarContext.init( context );
        schemaGenerator.createDatabase(new SugarDb( context ).getDB());
    }

    public static int getTipoDePromotor()
    {
        String query="SELECT * FROM PROMOTOR";
        List<Promotor> promotorList = Promotor.findWithQuery(Promotor.class, query);
        if (promotorList.size() > 0) {
            try {
                return Integer.parseInt(promotorList.get(0).getTipoPromotor());
            }
            catch (Throwable e){
                Log.e(TAG, "getTipoDePromotor: " + e.getMessage());
            }
        }
        return 0;
    }


    public static String getLetraIndice(String codigoDeBarras){
        String query = "SELECT * FROM SOLICITUD WHERE solSerie= '" + codigoDeBarras + "' and usada = '0'";
        List<Solicitud> listaSolicitudes = Solicitud.findWithQuery(Solicitud.class, query);
        return listaSolicitudes.size()>0 ? listaSolicitudes.get(0).getLetraSolicitud() : "XXX";
    }


    public static boolean existeLaSolicitud(String codigoDeBarras){
        String query = "SELECT * FROM SOLICITUD WHERE solSerie= '" + codigoDeBarras + "' and usada = '0'";
        List<Solicitud> listaSolicitudes = Solicitud.findWithQuery(Solicitud.class, query);
        return listaSolicitudes.size()>0;
    }

}
