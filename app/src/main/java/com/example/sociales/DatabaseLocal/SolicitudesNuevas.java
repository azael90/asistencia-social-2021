package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class SolicitudesNuevas extends SugarRecord {
    String nombresolicitud="", fotosolicitud="", codigobarras="", fecha="", latitud="", longitud="", sync="", codigoactivacion="", inversion="";

    public SolicitudesNuevas() {
    }

    public SolicitudesNuevas(String nombresolicitud, String fotosolicitud, String codigobarras,
                             String fecha, String latitud, String longitud, String sync, String codigoactivacion,
                             String inversion) {
        this.nombresolicitud = nombresolicitud;
        this.fotosolicitud = fotosolicitud;
        this.codigobarras = codigobarras;
        this.fecha = fecha;
        this.latitud = latitud;
        this.longitud = longitud;
        this.sync = sync;
        this.codigoactivacion = codigoactivacion;
        this.inversion = inversion;
    }

    public String getNombresolicitud() {
        return nombresolicitud;
    }

    public void setNombresolicitud(String nombresolicitud) {
        this.nombresolicitud = nombresolicitud;
    }

    public String getFotosolicitud() {
        return fotosolicitud;
    }

    public void setFotosolicitud(String fotosolicitud) {
        this.fotosolicitud = fotosolicitud;
    }

    public String getCodigobarras() {
        return codigobarras;
    }

    public void setCodigobarras(String codigobarras) {
        this.codigobarras = codigobarras;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getCodigoactivacion() {
        return codigoactivacion;
    }

    public void setCodigoactivacion(String codigoactivacion) {
        this.codigoactivacion = codigoactivacion;
    }

    public String getInversion() {
        return inversion;
    }

    public void setInversion(String inversion) {
        this.inversion = inversion;
    }
}
