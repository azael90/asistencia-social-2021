package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Promotor extends SugarRecord
{
    public String promCodigo, promNombre, promEfectividad, prom_AndroidID, promContraseña, promPermisos, promEstatus, promActualizarApp, tipoPromotor="";
    public int sync=0;

    public Promotor() {
    }

    public Promotor(String promCodigo, String promNombre, String promEfectividad,
                    String prom_AndroidID, String prom_Contraseña,
                    String prom_Permisos, String prom_Estatus, String prom_ActualizarApp, int sync, String tipoPromotor)
    {
        this.promCodigo = promCodigo;
        this.promNombre = promNombre;
        this.promEfectividad = promEfectividad;
        this.prom_AndroidID = prom_AndroidID;
        this.promContraseña = prom_Contraseña;
        this.promPermisos = prom_Permisos;
        this.promEstatus = prom_Estatus;
        this.promActualizarApp = prom_ActualizarApp;
        this.sync = sync;
        this.tipoPromotor = tipoPromotor;
    }

    public String getPromCodigo() {
        return promCodigo;
    }

    public void setPromCodigo(String promCodigo) {
        this.promCodigo = promCodigo;
    }

    public String getPromNombre() {
        return promNombre;
    }

    public void setPromNombre(String promNombre) {
        this.promNombre = promNombre;
    }

    public String getPromEfectividad() {
        return promEfectividad;
    }

    public void setPromEfectividad(String promEfectividad) {
        this.promEfectividad = promEfectividad;
    }

    public String getPromEst_AndroidID() {
        return prom_AndroidID;
    }

    public void setProm_AndroidID(String prom_AndroidID) {
        this.prom_AndroidID = prom_AndroidID;
    }

    public String getProm_Contraseña() {
        return promContraseña;
    }

    public void setProm_Contraseña(String prom_Contraseña) {
        this.promContraseña = prom_Contraseña;
    }

    public String getProm_Permisos() {
        return promPermisos;
    }

    public void setProm_Permisos(String prom_Permisos) {
        this.promPermisos = prom_Permisos;
    }

    public String getProm_Estatus() {
        return promEstatus;
    }

    public void setProm_Estatus(String prom_Estatus)
    {
        this.promEstatus = prom_Estatus;
    }

    public String getProm_ActualizarApp() {
        return promActualizarApp;
    }

    public void setProm_ActualizarApp(String prom_ActualizarApp) {
        this.promActualizarApp = prom_ActualizarApp;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getTipoPromotor() {
        return tipoPromotor;
    }

    public void setTipoPromotor(String tipoPromotor) {
        this.tipoPromotor = tipoPromotor;
    }
}
