package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class publicidad extends SugarRecord
{
    String url="";
    String imagen="";

    public publicidad() {
    }

    public publicidad(String imagen, String url) {
        this.imagen = imagen;
        this.url=url;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
