package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Domicilios extends SugarRecord
{
    public String idClienteFromSuagar="", calle="", numeroExterior="", numeroInterior="", colonia="", localidad="", codigoPostal="", entreCalles="", tipoDomicilio="";
    public int sync=0;

    public Domicilios() {
    }

    public Domicilios(String idClienteFromSuagar, String calle, String numeroExterior, String numeroInterior, String colonia,
                      String localidad, String codigoPostal, String entreCalles, String tipoDomicilio, int sync) {
        this.idClienteFromSuagar = idClienteFromSuagar;
        this.calle = calle;
        this.numeroExterior = numeroExterior;
        this.numeroInterior = numeroInterior;
        this.colonia = colonia;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.entreCalles = entreCalles;
        this.tipoDomicilio = tipoDomicilio;
        this.sync = sync;
    }

    public String getIdClienteFromSuagar() {
        return idClienteFromSuagar;
    }

    public void setIdClienteFromSuagar(String idClienteFromSuagar) {
        this.idClienteFromSuagar = idClienteFromSuagar;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getEntreCalles() {
        return entreCalles;
    }

    public void setEntreCalles(String entreCalles) {
        this.entreCalles = entreCalles;
    }

    public String getTipoDomicilio() {
        return tipoDomicilio;
    }

    public void setTipoDomicilio(String tipoDomicilio) {
        this.tipoDomicilio = tipoDomicilio;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }


}
