package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class ArchivosDescargados extends SugarRecord {
    String nombreArchivo="", fecha="", url="";

    public ArchivosDescargados() {
    }

    public ArchivosDescargados(String nombreArchivo, String fecha, String url) {
        this.nombreArchivo = nombreArchivo;
        this.fecha = fecha;
        this.url = url;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
