package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Oficinas extends SugarRecord {
    String nombre = "";

    public Oficinas() {
    }

    public Oficinas(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
