package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Configuracion extends SugarRecord
{
    public String conf_Efectividad, conf_DiasSinAfilicaciones, conf_MinutosInactvidad;
    public int sync=0;

    public Configuracion() {
    }

    public Configuracion( String conf_Efectividad, String conf_DiasSinAfilicaciones, String conf_MinutosInactvidad, int sync) {

        this.conf_Efectividad = conf_Efectividad;
        this.conf_DiasSinAfilicaciones = conf_DiasSinAfilicaciones;
        this.conf_MinutosInactvidad = conf_MinutosInactvidad;
        this.sync = sync;
    }



    public String getConf_Efectividad() {
        return conf_Efectividad;
    }

    public void setConf_Efectividad(String conf_Efectividad) {
        this.conf_Efectividad = conf_Efectividad;
    }

    public String getConf_DiasSinAfilicaciones() {
        return conf_DiasSinAfilicaciones;
    }

    public void setConf_DiasSinAfilicaciones(String conf_DiasSinAfilicaciones) {
        this.conf_DiasSinAfilicaciones = conf_DiasSinAfilicaciones;
    }

    public String getConf_MinutosInactvidad() {
        return conf_MinutosInactvidad;
    }

    public void setConf_MinutosInactvidad(String conf_MinutosInactvidad) {
        this.conf_MinutosInactvidad = conf_MinutosInactvidad;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
