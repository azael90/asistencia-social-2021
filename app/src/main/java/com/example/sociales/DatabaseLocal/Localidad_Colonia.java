package com.example.sociales.DatabaseLocal;

import com.orm.SugarRecord;

public class Localidad_Colonia extends SugarRecord
{
    private String  localidad, colonia;
    public int sync=0, idweb;

    public Localidad_Colonia() {
    }

    public Localidad_Colonia( String locColLocalidad, String locColColonia, int locCol_id_web, int sync) {
        this.localidad = locColLocalidad;
        this.colonia = locColColonia;
        this.idweb = locCol_id_web;
        this.sync = sync;
    }



    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String locColLocalidad) {
        this.localidad = locColLocalidad;
    }




    public String getColonia() {
        return colonia;
    }

    public void setColonia(String locColColonia)
    {
        this.colonia = locColColonia;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }



    public int getIdweb()
    {
        return idweb;
    }

    public void setIdweb(int idweb)
    {
        this.idweb = idweb;
    }

}
