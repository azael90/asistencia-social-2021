package com.example.sociales.Service;
        
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.provider.SyncStateContract;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.sociales.Activities.MainActivity;
import com.example.sociales.Extras.ApplicationResourcesProvider;
import com.example.sociales.R;


/**
* Created by Jordan Alvarez on 09/08/2019.
*/
public class ForegroundService extends Service
{
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private static final String TAG = "ForegroundServiceChannel";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        try
        {
            String input = intent.getStringExtra("inputExtra");
            if (input.equals(Constants.ACTION.STARTFOREGROUND_ACTION))
            {
                createNotificationChannel();

                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("Afiliaciones electrónicas")
                        .setContentText("Asistencia social")
                        .setSmallIcon(R.drawable.ic_person)
                        .build();

                startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);

            } else if (input.equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
                stopForeground(true);
                stopSelf();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @SuppressLint("LongLogTag")
    private void createNotificationChannel() {
        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel serviceChannel = new NotificationChannel(
                        CHANNEL_ID,
                        "Foreground Service Channel",
                        NotificationManager.IMPORTANCE_DEFAULT
                );

                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(serviceChannel);
            }
        }catch (Throwable e)
        {
            Log.e(TAG, "createNotificationChannel: " + e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}