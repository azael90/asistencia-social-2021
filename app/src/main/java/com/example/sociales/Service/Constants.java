package com.example.sociales.Service;
        
    /**
    * Created by Jordan Alvarez on 09/08/2019.
    */
    public class Constants {


        public interface ACTION {
            public static String MAIN_ACTION = "com.example.sociales.foregroundservice.action.main";
            public static String INIT_ACTION = "com.example.socialess.foregroundservice.action.init";
            public static String PREV_ACTION = "com.example.sociales.foregroundservice.action.prev";
            public static String PLAY_ACTION = "com.example.sociales.foregroundservice.action.play";
            public static String NEXT_ACTION = "com.example.sociales.foregroundservice.action.next";
            public static String STARTFOREGROUND_ACTION = "com.example.sociales.foregroundservice.action.startforeground";
            public static String STOPFOREGROUND_ACTION = "com.example.sociales.foregroundservice.action.stopforeground";
        }

        public interface NOTIFICATION_ID {
            public static int FOREGROUND_SERVICE = 101;
        }
    }